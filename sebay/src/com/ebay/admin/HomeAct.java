package com.ebay.admin;
import com.ebay.model.*;
import com.ebay.services.*;

import java.util.*;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;


public class HomeAct  extends ActionSupport{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public int category_id,subcategory_id;
	public ArrayList list1,list2,day;
	public String name,rslt;
	Map<String,Object> session;
	public ArrayList getList1() {
		return list1;
	}
	public void setList1(ArrayList list1) {
		this.list1 = list1;
	}


	private String category_name;
	public int getCategory_id() {
		return category_id;
	}
	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}
	public String getCategory_name() {
		return category_name;
	}
	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}
	
	
	public String execute()
	{
			
		return SUCCESS;
	   
	}
	
	
	public String show()
	{	
		updateTable ut=new updateTable();
		ut.updateTB();
		session=ActionContext.getContext().getSession();
		System.out.println("test name"+name);
		list1=new ArrayList();
		Categorymodel cat=new Categorymodel();
		homemodel hd=new homemodel();
		list1=cat.showCategory();
		session.put("catlist",list1);
     	return SUCCESS;
	}

	public String showSub()
	{	
		session=ActionContext.getContext().getSession();
		this.category_id=category_id;
		System.out.println("inside sub category666"+category_id);
		session.put("category_id", category_id);
		list2=new ArrayList();
		Categorymodel cat1=new Categorymodel();
		System.out.println("out sub category666");
		list2=cat1.showSubCategory();
		session.put("subcatlist",list2);
		 
		return SUCCESS;
	}
	public String nextPage(){
		
		this.subcategory_id=subcategory_id;
		session=ActionContext.getContext().getSession();
		System.out.println("before session"+subcategory_id);
		session.put("subcategory_id",subcategory_id);
		return "success";
	}
	
	public String finalSell(){
		System.out.println("aftere session");
		session=ActionContext.getContext().getSession();
		sellPageSetmodel sps=new sellPageSetmodel();
		rslt=sps.execute();
		return rslt;
	}

}
