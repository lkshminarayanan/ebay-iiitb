package com.ebay.admin;
import com.ebay.model.*;


import java.util.*;

import com.opensymphony.xwork2.ActionSupport;


public class categoriesaction  extends ActionSupport{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int category_id;
	public ArrayList list1,cat_list,day;
	public ArrayList getList1() {
		return list1;
	}
	public void setList1(ArrayList list1) {
		this.list1 = list1;
	}


	private String category_name;
	public int getCategory_id() {
		return category_id;
	}
	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}
	public String getCategory_name() {
		return category_name;
	}
	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}
	
	
	public String execute()
	{
		int category_id=this.getCategory_id();
		String category_name=this.getCategory_name();
		Categorymodel cat=new Categorymodel();
		
		cat.setCategory_id(category_id);
		cat.setCategory_name(category_name);
		int i=cat.insertCategory();
	    if(i==1)
	    {		
		return SUCCESS;
	    }
	    else
	    {
	    	return ERROR;
	    }
	}
	
	
	public String show()
	{	day = new ArrayList();	
	    cat_list=new ArrayList();
		Categorymodel cat=new Categorymodel();
		homemodel hd=new homemodel();
		list1=cat.showCategory();
		cat_list=(ArrayList)hd.getCat_list();
		 
		return SUCCESS;
	}

}
