package com.ebay.admin;
import com.opensymphony.*;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.util.Map;


public class LoginCheck extends ActionSupport {
	
	Map session;
	private String userid;
	
	public String execute(){
		session=ActionContext.getContext().getSession();
		userid=(String)session.get("userid");
		if(userid.equals("NULL")){
			return "error";
			
		}
		
		return "success";
	}

}
