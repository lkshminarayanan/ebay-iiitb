package com.ebay.admin;
import com.opensymphony.xwork2.ActionSupport;


import com.ebay.model.*;
import java.util.*;

public class subcategoryaction extends ActionSupport{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String subcategory_name;
	private String category_name;
	private int subcategory_id;
	private int category_id;
	private ArrayList list1;
	private ArrayList list2;
	public String getSubcategory_name() {
		return subcategory_name;
	}
	
	public String getCategory_name() {
		return category_name;
	}


	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}

	public ArrayList getList1() {
		return list1;
	}
	public void setList1(ArrayList list1) {
		this.list1 = list1;
	}
	

	public ArrayList getList2() {
		return list2;
	}
	public void setList2(ArrayList list1) {
		this.list2 = list2;
	}

	
	public void setSubcategory_name(String subcategory_name) {
		this.subcategory_name = subcategory_name;
	}
	public int getSubcategory_id() {
		return subcategory_id;
	}
	public void setSubcategory_id(int subcategory_id) {
		this.subcategory_id = subcategory_id;
	}
	public int getCategory_id() {
		return category_id;
	}
	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}
	
	public String execute()
	{
		int i = 0;
		category_id=this.getCategory_id();
		subcategory_name=this.getSubcategory_name();
		
		Subcategorymodel smodel=new Subcategorymodel();
		smodel.setCategory_id(category_id);
		smodel.setSubcategory_name(subcategory_name);
		i=smodel.insertdata();
		
		System.out.println("Cat ID : "+category_id);
		
		if(i==1)
		{		
		return SUCCESS;
		}
		else
		{
			return ERROR;
		}
		
	}
	
	
	public String display()
	{
		//category_id=this.getCategory_id();
		System.out.println("inside display"+category_id);
		Subcategorymodel model=new Subcategorymodel();
		list1=(ArrayList)model.displaydata(category_id);
		System.out.println("Cat ID : "+category_id);
		return SUCCESS;
	}

}