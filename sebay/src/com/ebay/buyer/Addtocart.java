package com.ebay.buyer;

import java.util.Map;

import java.util.*;
import com.ebay.model.*;
import com.opensymphony.xwork2.*;
public class Addtocart extends ActionSupport{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int category_id;
	private int subcategory_id;
	
	private int seller_id1;
	private String user_id;
	private String seller_id;
	private int product_id;
	private String title;
	private String image;
	private float price;
	private float subtotal;
	private ArrayList<Cart> listitems;

	private int quantitypurchased;
	int i;
	private float sub_total;

	
	public float getPrice() {
		return price;
	}



	public void setPrice(float price) {
		this.price = price;
	}


	public String getUser_id() {
		return user_id;
	}



	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}



	public String getSeller_id() {
		return seller_id;
	}



	public void setSeller_id(String seller_id) {
		this.seller_id = seller_id;
	}



	public int getProduct_id() {
		return product_id;
	}



	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}



	public String getTitle() {
		return title;
	}



	public void setTitle(String title) {
		this.title = title;
	}



	public String getImage() {
		return image;
	}



	public void setImage(String image) {
		this.image = image;
	}



	
	public String execute()
	{
        System.out.println("in add to cart");
		category_id=this.getCategory_id();
		subcategory_id=this.getSubcategory_id();
		seller_id=this.getSeller_id();
		product_id=this.getProduct_id();
		quantitypurchased=this.getQuantitypurchased();
		price=this.getPrice();
		title=this.getTitle();
		image=this.getImage();
		sub_total=quantitypurchased*price;
		this.setSub_total(sub_total);
		Map<String,Object> session = ActionContext.getContext().getSession();
		System.out.println("title"+title+"price"+price);
		user_id= (String) session.get("ebay_id");
				
		Cart cart=new Cart();
	//	cart.setCategory_id(category_id);
	
		cart.setUser_id(user_id);
		cart.setCategory_id(category_id);
		cart.setSubcategory_id(subcategory_id);
		//cart.setImage(image);
		cart.setProduct_id(product_id);
		cart.setSeller_id(seller_id);
		cart.setQuantitypurchased(quantitypurchased);
		//cart.setSub_total(sub_total);
		cart.setPrice(price);
		cart.setTitle(title);
		cart.setImage(image);
		i=cart.insert();
		System.out.println("testing arraylist in cart action "+i);
		System.out.println(user_id);
		System.out.println(seller_id);
		if(i==1)
		{
		
		}
		return SUCCESS;
	}


	
	
	public int getSeller_id1() {
		return seller_id1;
	}



	public void setSeller_id1(int seller_id1) {
		this.seller_id1 = seller_id1;
	}



	public int getQuantitypurchased() {
		return quantitypurchased;
	}



	public void setQuantitypurchased(int quantitypurchased) {
		this.quantitypurchased = quantitypurchased;
	}



	public float getSub_total() {
		return sub_total;
	}



	public void setSub_total(float sub_total) {
		this.sub_total = sub_total;
	}



	public ArrayList<Cart> getListitems() {
		return listitems;
	}



	public void setListitems(ArrayList<Cart> listitems) {
		this.listitems = listitems;
	}



	public int getCategory_id() {
		return category_id;
	}



	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}



	public int getSubcategory_id() {
		return subcategory_id;
	}



	public void setSubcategory_id(int subcategory_id) {
		this.subcategory_id = subcategory_id;
	}

}
