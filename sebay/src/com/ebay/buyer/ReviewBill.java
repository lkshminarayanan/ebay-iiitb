package com.ebay.buyer;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Map;

import com.ebay.model.*;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class ReviewBill extends ActionSupport{
	
	public String getUrl(){
		return url;
	}
	
	public int getProduct_id() {
		return product_id;
	}

	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}

	public int getCourierCharges() {
		return courierCharges;
	}

	public void setCourierCharges(int courierCharges) {
		this.courierCharges = courierCharges;
	}

	public float getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(float subtotal) {
		this.subtotal = subtotal;
	}

	public ArrayList<Item> getListItems() {
		return listItems;
	}

	public void setListItems(ArrayList<Item> listItems) {
		this.listItems = listItems;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public ArrayList<String> getShippingAddress() {
		return shippingAddress;
	}

	public void setShippingAddress(ArrayList<String> shippingAddress) {
		this.shippingAddress = shippingAddress;
	}
	
	public int getQuantitypurchased() {
		return quantitypurchased;
	}

	public void setQuantitypurchased(int quantitypurchased) {
		this.quantitypurchased = quantitypurchased;
	}
	
	public int getCategory_id() {
		return category_id;
	}

	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}

	public int getSubcategory_id() {
		return subcategory_id;
	}

	public void setSubcategory_id(int subcategory_id) {
		this.subcategory_id = subcategory_id;
	}

	public float getTotal() {
		return total;
	}

	public void setTotal(float total) {
		this.total = total;
	}

	



	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}





	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int category_id;
	private int subcategory_id;
	private ArrayList<String> shippingAddress;
	private int user_id;
	private float subtotal;
	ArrayList<Item> listItems = new ArrayList<Item>();
	private int product_id;
	private int quantitypurchased;
	private int courierCharges;
	Item item;
	private float total;
	private String errorMessage;
	private String url;

	

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}
	
	public String getSubtotalText(){
		DecimalFormat df = new DecimalFormat("###.00");
		return df.format((double)subtotal);
	}
	
	public String getTotalText(){
		DecimalFormat df = new DecimalFormat("###.00");
		return df.format((double)total);
	}

	public ReviewBill() {
		super();
	}

	public String execute()
	{
		Map<String,Object> session = ActionContext.getContext().getSession();

		//check if the user is same as seller if yes redirect back.
		
		UserModel u = (UserModel)session.get("user");
		
		System.out.println("in review"+product_id);
        try{
		item = Item.getItemDetails(category_id,subcategory_id,product_id);
		
		if(u.getEbay_id().equals(item.getSeller_id())){
			url = (String)session.get("prevPrevUrl");
			System.out.println("url is"+url);
			return "sameuser";
		}
        }catch(Exception e){
        	System.out.println("error is"+e.getMessage());
        }
		
		if(item.getQuantity()<quantitypurchased){
			quantitypurchased = item.getQuantity();
			setErrorMessage("Quantity you have specified for some items is not available.<br>The available quantity is highlighted in red.");
		}
		
		subtotal = item.getPrice()*quantitypurchased;
		listItems.add(item);
		
		
		shippingAddress=u.getshippingaddress(u.getEbay_id());

		System.out.println("Size "+shippingAddress.size());
		
		total = subtotal + item.getShippingCharges();

		return SUCCESS;

	}
}
