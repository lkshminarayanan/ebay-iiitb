package com.ebay.buyer;

import java.text.DecimalFormat;
import java.util.Map;

import com.ebay.model.*;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class ReviewBid extends ActionSupport{
	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public boolean isPlaceBid() {
		return placeBid;
	}

	public void setPlaceBid(boolean placeBid) {
		this.placeBid = placeBid;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public int getProduct_id() {
		return product_id;
	}

	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}

	public int getCategory_id() {
		return category_id;
	}

	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}

	public int getSubcategory_id() {
		return subcategory_id;
	}

	public void setSubcategory_id(int subcategory_id) {
		this.subcategory_id = subcategory_id;
	}

	public float getBidAmount() {
		return bidAmount;
	}

	public void setBidAmount(float bidAmount) {
		this.bidAmount = bidAmount;
	}
	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getBidAmountText(){
		DecimalFormat df = new DecimalFormat("##0.00");
		return df.format((double)getBidAmount());
	}



	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int product_id;
	private Item item = null;
	private boolean placeBid = false;
	private String errorMsg;
	private int category_id;
	private int subcategory_id;
	private float bidAmount;
	private String url;

	public ReviewBid() {
		super();
	}

	public String execute()
	{
		
		Map<String,Object> session = ActionContext.getContext().getSession();

		//check if the user is same as seller if yes redirect back.
		
		UserModel u = (UserModel)session.get("user");

		item = Book.getItemDetails(product_id);
		
		if(u.getEbay_id().equals(item.getSeller_id())){
			url = (String)session.get("prevPrevUrl");
			return "sameuser";
		}

		System.out.println("Bidding : "+product_id);

		item = Item.getItemDetails(category_id,subcategory_id,product_id);

		System.out.println(bidAmount + "  : "+item.getBid().getNextValidBid());
		if(!item.isExpired())
		{	
			if(bidAmount >= item.getBid().getNextValidBid()){
				//TODO:placebid
				System.out.println(bidAmount + "  : "+item.getBid().getNextValidBid());
				
				placeBid = true;
			}
		}
		return SUCCESS;

	}
}
