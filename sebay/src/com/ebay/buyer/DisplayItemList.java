package com.ebay.buyer;

import java.util.LinkedHashMap;

import com.ebay.model.Item;
import com.ebay.services.Services;

import com.opensymphony.xwork2.ActionSupport;


public class DisplayItemList extends ActionSupport{


	public boolean isFilterAuction() {
		return filterAuction;
	}

	public void setFilterAuction(String filterAuction) {
		this.filterAuction = true;
	}

	public boolean isFilterBuyNow() {
		return filterBuyNow;
	}

	public void setFilterBuyNow(String filterBuyNow) {
		this.filterBuyNow = true;
	}

	public LinkedHashMap<String, String> getSubCatList() {
		return subCatList;
	}

	public void setSubCatList(LinkedHashMap<String, String> subCatList) {
		this.subCatList = subCatList;
	}

	public int getSubcategory_id() {
		return subcategory_id;
	}

	public void setSubcategory_id(int subcategory_id) {
		this.subcategory_id = subcategory_id;
	}

	public int getCategory_id() {
		return category_id;
	}

	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}

	public float getFilterPriceRangeLow() {
		return filterPriceRangeLow;
	}

	public void setFilterPriceRangeLow(float filterPriceRangeLow) {
		this.filterPriceRangeLow = filterPriceRangeLow;
	}

	public float getFilterPriceRangeHigh() {
		return filterPriceRangeHigh;
	}

	public void setFilterPriceRangeHigh(float filterPriceRangeHigh) {
		this.filterPriceRangeHigh = filterPriceRangeHigh;
	}

	public int getFilterItemsToDisplay() {
		return filterItemsToDisplay;
	}

	public void setFilterItemsToDisplay(int filterItemsToDisplay) {
		this.filterItemsToDisplay = (filterItemsToDisplay==0)?1:filterItemsToDisplay;
	}

	public int getFilterSort() {
		return filterSort;
	}

	public void setFilterSort(int filterSort) {
		this.filterSort = filterSort;
	}

	public LinkedHashMap<String, Item> getItemList() {
		return itemList;
	}

	public void setItemList(LinkedHashMap<String, Item> itemList) {
		this.itemList = itemList;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public int getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}


	public int getFilterReset() {
		return 0;
	}

	public void setFilterReset(int filterReset) {
		if(filterReset == 1)
			this.filterReset = true;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}


	public boolean isFilterCondOld() {
		return filterCondOld;
	}

	public void setFilterCondOld(String filterOld) {
		this.filterCondOld = true;
	}

	public boolean isFilterCondNew() {
		return filterCondNew;
	}

	public void setFilterCondNew(String filterNew) {
		this.filterCondNew = true;
	}

	public String getSearchTerm() {
		return searchTerm;
	}

	public void setSearchTerm(String searchTerm) {
		this.searchTerm = (searchTerm==null||searchTerm.equals(""))?null:searchTerm;
	}

	public String getFilterSellerId() {
		return filterSellerId;
	}

	public void setFilterSellerId(String filterSellerId) {
		this.filterSellerId = filterSellerId;
	}





	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int subcategory_id;
	private int category_id;
	private LinkedHashMap<String,Item> itemList = new LinkedHashMap<String, Item>();
	private LinkedHashMap<String,String> subCatList = new LinkedHashMap<String, String>();
	private String searchTerm;

	private boolean filterAuction = false;
	private boolean filterBuyNow = false;
	private boolean filterCondOld = false;
	private boolean filterCondNew = false;
	private float filterPriceRangeLow;
	private float filterPriceRangeHigh;
	private int filterItemsToDisplay;
	private int filterSort;
	private String filterSellerId;//filter

	private boolean filterReset = false;
	private int pageNumber;
	private int totalPages;

	public DisplayItemList(){
		super();
	}

	public boolean isFilterSet(){
		if((category_id!=0&&searchTerm!=null&&searchTerm.length()>0)
				||filterBuyNow
				||filterAuction
				||filterCondNew
				||filterCondOld
				||filterPriceRangeHigh!=0.0
				||filterPriceRangeLow!=0.0
				||(filterSellerId!=null&&!filterSellerId.equals(""))){
			return true;
		}
		return false;
	}

	public void resetFilter(){
		if(filterReset){
			filterPriceRangeHigh = filterPriceRangeLow  = 0;
			filterAuction = filterBuyNow = filterCondNew = filterCondOld = false;
			filterSellerId = "";
			if(searchTerm!=null&&searchTerm.length()>0&category_id!=0)
				category_id=0;

		}
	}

	public String execute()
	{	
		resetFilter();

		String queryFilters = generateFilterQuery();
		
		if(subcategory_id==0){
			
		}

		if(searchTerm!=null){
			totalPages = Services.doSearch(searchTerm, queryFilters, itemList,category_id,subcategory_id,pageNumber,filterItemsToDisplay);

		}else{
			totalPages = Item.getItemList(queryFilters,itemList,category_id,subcategory_id);
		}
		totalPages = totalPages/filterItemsToDisplay + ((totalPages%filterItemsToDisplay>0)?1:0);


		//get the subcategory list to be displayed in side.
		subCatList = Services.getSubcategoryList(category_id, subcategory_id);
		System.out.println("Pages : "+totalPages);
		return SUCCESS;

	}

	private String generateFilterQuery() {
		String queryFilter = "";

		if(filterSellerId!=null&&!filterSellerId.equals("")){
			queryFilter += " AND seller_id = '"+filterSellerId+"' ";
		}

		if(filterAuction&&!filterBuyNow){
			queryFilter += " AND biddingstatus = 1 ";

		}else if(!filterAuction&&filterBuyNow){
			queryFilter += " AND biddingstatus = 0 ";
		}

		if(filterCondNew&&!filterCondOld){
			queryFilter += " AND cond = 'new' ";		
		}else if(!filterCondNew&&filterCondOld){
			queryFilter += " AND cond = 'old' ";
		}

		if(filterPriceRangeHigh!=0||filterPriceRangeLow!=0){
			if(filterPriceRangeHigh<filterPriceRangeLow){
				float temp = filterPriceRangeHigh;
				filterPriceRangeHigh = filterPriceRangeLow;
				filterPriceRangeLow = temp;
			}
			queryFilter += " AND Price > "+filterPriceRangeLow+" AND Price < "+filterPriceRangeHigh+" ";

		}

		switch (filterSort) {
		case 1:
			queryFilter += " ORDER BY Price ASC ";
			break;
		case 2:
			queryFilter += " ORDER BY Price DESC ";
			break;
		case 3:
			queryFilter += " ORDER BY expdate ASC ";
			break;
		default:
			break;
		}

		if(searchTerm==null||searchTerm.length()==0){
			filterItemsToDisplay=(filterItemsToDisplay==0)?2:filterItemsToDisplay;//First time initialization
			queryFilter += " LIMIT "+pageNumber*filterItemsToDisplay+","+filterItemsToDisplay;
		}

		return queryFilter;
	}
}
