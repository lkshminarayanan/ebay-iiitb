package com.ebay.buyer;

import java.util.ArrayList;

import com.ebay.model.BidHistory;
import com.ebay.model.Item;
import com.opensymphony.xwork2.ActionSupport;

public class DisplayBiddingHistory extends ActionSupport{

	
	
	
	public int getCategory_id() {
		return category_id;
	}
	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}
	public int getSubcategory_id() {
		return subcategory_id;
	}
	public void setSubcategory_id(int subcategory_id) {
		this.subcategory_id = subcategory_id;
	}
	public int getProduct_id() {
		return product_id;
	}
	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}
	public Item getItem() {
		return item;
	}
	public void setItem(Item item) {
		this.item = item;
	}
	
	public ArrayList<BidHistory> getHistory() {
		return history;
	}
	public void setHistory(ArrayList<BidHistory> history) {
		this.history = history;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int category_id;
	private int subcategory_id;
	private int product_id;
	private Item item;
	ArrayList<BidHistory> history;
	
	public String execute(){
		
		
		item = Item.getItemDetails(category_id,subcategory_id, product_id);
		
		history = BidHistory.getBiddingHistory(category_id,subcategory_id, product_id);
		
		return SUCCESS;
	}

}
