package com.ebay.buyer;




import java.util.Map;

import com.ebay.model.Cart;
import com.ebay.model.Item;
import com.ebay.model.UserModel;
import com.ebay.services.Services;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;


public class ShowItemDetails extends ActionSupport{



	public String getCategoryName(){
		return Services.getCategoryName(item.getCategory_id());
	}

	public String getSubCategoryName(){
		return Services.getSubCategoryName(item.getCategory_id(),item.getSubcategory_id());
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public int getProduct_id() {
		return product_id;
	}

	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}

	public String getLocationInfo(){
		return UserModel.getLocationInfo(item.getSeller_id());
	}

	public int getCategory_id() {
		return category_id;
	}

	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}

	public int getSubcategory_id() {
		return subcategory_id;
	}

	public void setSubcategory_id(int subcategory_id) {
		this.subcategory_id = subcategory_id;
	}
	
	public boolean isAlreadyInCart() {
		return isAlreadyInCart;
	}

	public void setAlreadyInCart(boolean isAlreadyInCart) {
		this.isAlreadyInCart = isAlreadyInCart;
	}



	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int category_id;
	private int subcategory_id;
	private int product_id;
	Item item = null;
	private boolean isAlreadyInCart;

	public String execute(){
		
		Map<String,Object> session = ActionContext.getContext().getSession(); 
		
		item = Item.getItemDetails(category_id,subcategory_id,product_id);
		
		UserModel user = (UserModel)session.get("user");
		
		if(user!=null)
		isAlreadyInCart = Cart.isAlreadyInCart(category_id,subcategory_id,product_id,user.getEbay_id());
		
		return SUCCESS;
	}





}