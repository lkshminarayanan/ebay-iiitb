package com.ebay.buyer;

import java.util.Map;

import com.ebay.model.Item;
import com.ebay.model.UserModel;
import com.ebay.services.Services;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class ConfirmBid extends ActionSupport{

	public float getBidAmount() {
		return bidAmount;
	}

	public void setBidAmount(float bidAmount) {
		this.bidAmount = bidAmount;
	}

	public int getProduct_id() {
		return product_id;
	}

	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}
		
	public int getCategory_id() {
		return category_id;
	}

	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}

	public int getSubcategory_id() {
		return subcategory_id;
	}

	public void setSubcategory_id(int subcategory_id) {
		this.subcategory_id = subcategory_id;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public int getSuccessFlag() {
		return successFlag;
	}

	public void setSuccessFlag(int successFlag) {
		this.successFlag = successFlag;
	}



	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int category_id;
	private int subcategory_id;
	private int product_id;
	private Item item = null;
	private float bidAmount;
	private int successFlag = 0;

	public ConfirmBid() {
		super();
	}

	public String execute()
	{
		Map<String,Object> session = ActionContext.getContext().getSession();

		System.out.println("Bidding : "+product_id);
		UserModel user = (UserModel)session.get("user");
		
		item = Item.getItemDetails(category_id, subcategory_id, product_id);
		
		if(!item.isExpired()&&bidAmount >= item.getBid().getNextValidBid()){
			successFlag = item.getBid().placeBid(bidAmount,user.getEbay_id());
			if(successFlag>0)
				Services.updatePrice(category_id,subcategory_id,product_id,bidAmount);
		}
		
		return SUCCESS;

	}
}
