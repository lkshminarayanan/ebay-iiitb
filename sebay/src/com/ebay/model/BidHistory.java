package com.ebay.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.ebay.util.DBconn;

public class BidHistory {
	
	
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getTimeStamp() {
		return new SimpleDateFormat("d MMMM yyyy, kk:mm:ss zz").format(timeStamp);
	}
	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}
	public float getBidPrice() {
		return bidPrice;
	}
	public void setBidPrice(float bidPrice) {
		this.bidPrice = bidPrice;
	}
	public String getBidPriceText(){
		DecimalFormat df = new DecimalFormat("###.00");
		return df.format((double)bidPrice);
	}
	
	
	@Override
	public String toString() {
		return "BidHistory [user_id=" + user_id + ", timeStamp=" + timeStamp
				+ ", bidPrice=" + bidPrice + "]";
	}


	private String user_id;
	private Date timeStamp;
	private float bidPrice;
	
	
	
	public BidHistory(String user_id, Date timeStamp, float bidPrice) {
		super();
		this.user_id = user_id;
		this.timeStamp = timeStamp;
		this.bidPrice = bidPrice;
	}
	public String getUserIdEnc(){
		char[] temp = new char[user_id.length()];
		int i;
		temp[0] = user_id.charAt(0);
		for(i=1;i<user_id.length()-1;i++){
			temp[i] = '*';
		}
		temp[i] = user_id.charAt(i);
		return new String(temp);
			
	}
	public static ArrayList<BidHistory> getBiddingHistory(int category_id,
			int subCategory_id, int product_id) {
		ArrayList<BidHistory> history = new ArrayList<BidHistory>();
		String query = "select user_id,price,bidTime from userbidding where category_id = "+category_id+" AND " +
				"subcategory_id = "+subCategory_id+" AND product_id = "+product_id+" ORDER BY bidTime DESC;";
		System.out.println("query : "+query);
		ResultSet rs = null;
		try {
			rs = DBconn.getConnection().createStatement().executeQuery(query);
			while(rs.next()){
				history.add(new BidHistory(rs.getString("user_id"), rs.getTimestamp("bidTime"), rs.getFloat("price")));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("Error Here!");
			e.printStackTrace();
		}
		for(BidHistory b:history)
			System.out.println(b.toString());
		return history;
		
	}
	
}
