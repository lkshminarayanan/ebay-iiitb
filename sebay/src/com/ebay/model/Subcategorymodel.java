package com.ebay.model;

import java.sql.*;

import com.ebay.util.DBconn;

import java.util.*;

public class Subcategorymodel{
	private String subcategory_name;
	private int subcategory_id;
	private int category_id;
	DBconn db;
	
	
	private String category_name;
	
	String query;
	ResultSet rs;
	Statement stmt;
	Connection con;
	int i;
	
	public Subcategorymodel(int category_id2, String category_name2,
			int subcategory_id2, String subcategory_name2) {
		// TODO Auto-generated constructor stub
		this.category_id = category_id2;
		this.category_name = category_name2;
		this.subcategory_id = subcategory_id2;
		this.subcategory_name = subcategory_name2;
	}
	public Subcategorymodel() {
		// TODO Auto-generated constructor stub
		super();
	}
	public String getCategory_name() {
		return category_name;
	}
	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}
	
	

	public int getCategory_id() {
		return category_id;
	}
	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}
	

	
	
	public String getSubcategory_name() {
		return subcategory_name;
	}
	public void setSubcategory_name(String subcategory_name) {
		this.subcategory_name = subcategory_name;
	}
	public int getSubcategory_id() {
		return subcategory_id;
	}
	public void setSubcategory_id(int subcategory_id) {
		this.subcategory_id = subcategory_id;
	}
	
	public int insertdata()
	{
		String subcategory_name=this.getSubcategory_name();
		int category_id=this.getCategory_id();
		try
		{
			con=DBconn.getConnection();			
			stmt=con.createStatement();
			query="insert into subcategory(category_id,subcategory_name) values("+ category_id + ",'"+subcategory_name+"')";
			i=stmt.executeUpdate(query);
			
		}
		catch(Exception e)
		{
			System.out.println(e.getLocalizedMessage());
		}
		return i;
	}

	public ArrayList displaydata(int category_id)
	{
		System.out.println("inside display data"+category_id);
		ArrayList list1=new ArrayList();
		PreparedStatement prepStmt=null;
		try
		{
			db=new DBconn();
			con=db.getConnection();			
			
			stmt=con.createStatement();
			query="select * from subcategory join category on category.category_id=subcategory.category_id where subcategory.category_id=?";
			prepStmt=con.prepareStatement(query);
			prepStmt.setInt(1, category_id);
			rs=prepStmt.executeQuery();
			while(rs.next())
			{
				category_id=rs.getInt("category.category_id");
				subcategory_id=rs.getInt("subcategory_id");
				category_name=rs.getString("category_name");
				subcategory_name=rs.getString("subcategory_name");
				Subcategorymodel cat=new Subcategorymodel(category_id,category_name,subcategory_id,subcategory_name);
				list1.add(cat);		
			}
		}
		catch(Exception e)
		{
			System.out.println(e.getLocalizedMessage());
		}
		
		return list1;
	}
		


}