package com.ebay.model;
import com.ebay.util.*;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import java.sql.*;
import java.util.ArrayList;
import java.util.Map;

import org.apache.struts2.views.freemarker.tags.SetModel;
public class UserModel {
	private String fname;
	private 	String lname;
	private String streetaddress;
	private int zip;
	private String city;
	private String state;
	private String country;
	private String email;
	private String phone_no;
	private String ebay_id;
	private String password;
	private String secret_question;
	private String secret_answer;
	private String dob;
	private String user_id;
	private String line1;
	private String line2;
	private String pincode;
	Map session;
	
	Connection con;
     String query;
	Statement stmt;
	int i;
	private ArrayList<UserModel> lstshipaddress;

	
	
	public UserModel()
	{
		
	}
	
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getStreetaddress() {
		return streetaddress;
	}
	public void setStreetaddress(String streetaddress) {
		this.streetaddress = streetaddress;
	}
	public int getZip() {
		return zip;
	}
	public void setZip(int zip) {
		this.zip = zip;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
		
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone_no() {
		return phone_no;
	}
	public void setPhone_no(String phone_no) {
		this.phone_no = phone_no;
	}
	public String getEbay_id() {
		return ebay_id;
	}
	public void setEbay_id(String ebay_id) {
		this.ebay_id = ebay_id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getSecret_question() {
		return secret_question;
	}
	public void setSecret_question(String secret_question) {
		this.secret_question = secret_question;
	}
	public String getSecret_answer() {
		return secret_answer;
	}
	public void setSecret_answer(String secret_answer) {
		this.secret_answer = secret_answer;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	
	public int insertdata()
	{
		fname=this.getFname();
		lname=this.getLname();
		city=this.getCity();
		country=this.getCountry();
		dob=this.getDob();
		ebay_id=this.getEbay_id();
		email=this.getEmail();
		password=this.getPassword();
		phone_no=this.getPhone_no();
		secret_answer=this.getSecret_answer();
		secret_question=this.getSecret_question();
		streetaddress=this.getStreetaddress();
		zip=this.getZip();
		city=this.getCity();
		System.out.println(city);
		try
		{
			Connection con=null;
			con=DBconn.getConnection();
			stmt=con.createStatement();
			query="insert into users(fname,lname,streetaddress,zip,city,state,country,email,phone_no,ebay_id,password,secret_question,secret_answer,dob) values('"+ fname + "','"+ lname + "','"+ streetaddress + "',"+zip +",'"+city+"','"+state+"','"+country+"','"+email+"','"+phone_no+"','"+ebay_id+"','"+password+"','"+secret_question+"','"+secret_answer+"','"+dob+"')";
			i=stmt.executeUpdate(query);
			
			
		}
		catch(Exception e)
		{
			System.out.println(e.getLocalizedMessage());
		}
		finally
		{
			try
			{
				con.close();
			}
			catch(Exception e)
			{
				System.out.println(e.getLocalizedMessage());
			}
		}
		
		
		return i;
	}
	
	
	public int check(String email,String password)
	{
		System.out.println("checking"+email + password);
		Connection con=DBconn.getConnection();
		session=ActionContext.getContext().getSession();
		try
		{
			String query="select * from users where email=? and password=?";
			PreparedStatement stmt=con.prepareStatement(query);
			stmt.setString(1, email);
			stmt.setString(2, password);
			ResultSet rs=stmt.executeQuery();
			if(rs.next())
			{
				
				setEbay_id(rs.getString("ebay_id"));
				setFname(rs.getString("fname"));
				setCity(rs.getString("city"));
				setStreetaddress(rs.getString("streetaddress"));
				setState(rs.getString("state"));
				setPhone_no(rs.getString("phone_no"));
				setZip(rs.getInt("zip"));
				setEmail(rs.getString("email"));
				System.out.println("ramu"+fname);
				session.put("email",email);
				session.put("fname",fname);
				session.put("ebay_id",ebay_id);
				System.out.println("ebay id is"+ebay_id);
				i=1;
			}
			else
				i=0;
			
		}
		catch(Exception e)
		{
			
		}
		
		return i;
	}
	
	public void getdetails()
	{
		System.out.println(email);
		email=this.getEmail();
		Connection con=DBconn.getConnection();
		Statement stmt=null;
		ResultSet rs=null;
		try
		{
			String query="select * from users where email='"+email+"'";
			stmt=con.createStatement();
			rs=stmt.executeQuery(query);
			while(rs.next())
			{
				fname=rs.getString("fname");
				lname=rs.getString("lname");
				ebay_id=rs.getString("ebay_id");
				
				
			}
		}
		catch(Exception e)
		{
			System.out.println(e.getLocalizedMessage());
		}
		
	}
	
	public ArrayList<String> getshippingaddress(String u)
	{
		System.out.println("ebay id"+ebay_id);
		ArrayList<String> shippingAddress = new ArrayList<String>();
		Connection con=DBconn.getConnection();
		Statement stmt=null;
		ResultSet rs=null;
		try
		{
			stmt=con.createStatement();
			String query="select * from users where ebay_id='"+ebay_id+"';";
			rs=stmt.executeQuery(query);
			while(rs.next())
			{
				shippingAddress.add(rs.getString("streetaddress"));
				shippingAddress.add(rs.getString("city")+" "+rs.getString("zip"));
				shippingAddress.add(rs.getString("state"));
				shippingAddress.add(rs.getString("country"));
				//shippingAddress.add();
			}
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
		System.out.println("Size address" +shippingAddress.size());
		return shippingAddress;
	}

	  public ArrayList<String>getaddress(String user_id1)
	    {
	        
	        ArrayList<String> listaddress = new ArrayList<String>();
	        Connection con=DBconn.getConnection();
	        Statement stmt=null;
	        ResultSet rs=null;
	        try
	        {
	            stmt=con.createStatement();
	            String query="select * from users where user_id='"+user_id1+"'";
	            rs=stmt.executeQuery(query);
	            while(rs.next())
	            {
	                listaddress.add(rs.getString("fname"));
	                listaddress.add(rs.getString("lname"));
	                listaddress.add(rs.getString("streetaddress"));
	                listaddress.add(rs.getString("zip"));
	                listaddress.add(rs.getString("city"));
	                listaddress.add(rs.getString("state"));
	                listaddress.add(rs.getString("country"));
	                
	                //shippingAddress.add();
	            }
	        }
	        catch(Exception e)
	        {
	            System.out.println(e.getMessage());
	        }
	        
	        
	        return listaddress;
	    }
	public ArrayList<UserModel> getLstshipaddress() {
		return lstshipaddress;
	}

	public void setLstshipaddress(ArrayList<UserModel> lstshipaddress) {
		this.lstshipaddress = lstshipaddress;
	}

	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getLine1() {
		return line1;
	}

	public void setLine1(String line1) {
		this.line1 = line1;
	}

	public String getLine2() {
		return line2;
	}

	public void setLine2(String line2) {
		this.line2 = line2;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public static String getLocationInfo(String seller_id) {
		String query = "select city,state from users where ebay_id = '"+seller_id+"';";
		Connection con = DBconn.getConnection();
		Statement stmt = null;
		ResultSet rs = null;
		try {
			stmt = con.createStatement();
			rs = stmt.executeQuery(query);
			if(rs.next()){
				return rs.getString(1)+", "+rs.getString(2)+".";
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}

}
