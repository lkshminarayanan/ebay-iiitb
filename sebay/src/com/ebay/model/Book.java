package com.ebay.model;

import com.ebay.util.DBconn;

import java.sql.*;
import java.util.*;
import java.util.Date;

public class Book extends Item{

	public int getPages() {
		return pages;
	}

	public void setPages(int pages) {
		this.pages = pages;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getFormat_type() {
		return format_type;
	}

	public void setFormat_type(String format_type) {
		this.format_type = format_type;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	
	private int pages;
	private String author;
	private String publisher;
	private String lang;
	private String format_type;
	private int user_id;
    private float shipcost;
	public Book(int category_id, int subcategory_id, int product_id,
			String title, String description, Float price, String cond,
			int pages, String author, String publisher, String lang,
			String format_type, String image, int biddingstatus,
			int quantity,Date expdate,String seller_id,int status,Float shipcost) {
		super(category_id,subcategory_id,product_id,title,description,price,cond,image,biddingstatus,quantity,expdate,seller_id,status,shipcost);
		this.pages = pages;
		this.author = author;
		this.publisher = publisher;
		this.lang = lang;
		this.format_type = format_type;

	}

	public static int getItemList(String query,LinkedHashMap<String, Item> lstbooks, int subcategory_id){
		Connection connection = null;
		ResultSet rs = null;
		
		if(subcategory_id!=0)
			query = " AND subcategory_id = "+subcategory_id+" "+query;

		query = "select SQL_CALC_FOUND_ROWS * from bookdetails where status = 1 "+query+";";
		System.out.println("Query : "+query);
		try {
			connection=DBconn.getConnection();
			Statement stmt=connection.createStatement();

			rs=stmt.executeQuery(query);
			while (rs.next()) {
				int category_id=rs.getInt("category_id");
				subcategory_id=rs.getInt("subcategory_id");
				int product_id=rs.getInt("product_id");
				subcategory_id = rs.getInt("subcategory_id");
				String title=rs.getString("title");
				String description=rs.getString("description");
				Float price=rs.getFloat("price");
				String cond=rs.getString("cond");
				int pages=rs.getInt("pages");
				String author=rs.getString("author");
				String publisher=rs.getString("publisher");
				String lang=rs.getString("lang");
				String format_type=rs.getString("format_type");
				String image=rs.getString("image");
				int biddingstatus=rs.getInt("biddingstatus");
				//				int bid_id=rs.getInt("bid_id");
				int quantity=rs.getInt("quantity");
				Date expdate = rs.getTimestamp("expdate");
				String seller_id = rs.getString("seller_id");
				int status = rs.getInt("status");
                Float shipcost=rs.getFloat("shipcost");
				Book book=new Book(category_id,subcategory_id,product_id,title,description,price,cond,pages,author,publisher,lang,format_type,image,biddingstatus,quantity,expdate,seller_id,status,shipcost);
				
				System.out.println(book.toString());
				lstbooks.put(author+" | "+lang+" | "+format_type,book);
			}
			query = "SELECT FOUND_ROWS();";
			rs = stmt.executeQuery(query);
			while (rs.next()) {
				return rs.getInt(1);
			}
		}
		catch(Exception e)
		{
			System.out.println("error is"+e.getMessage());
			e.printStackTrace();

		}
		return -1;
	}




	public static Book getItemDetails(int product_id) {
		Book book = null;
		PreparedStatement prepStmt = null;
		try {
			Connection connection=null;
			connection=DBconn.getConnection();
			ResultSet rs = null;			
			String QueryString = "select * from bookdetails where product_id=?";
			prepStmt = connection.prepareStatement(QueryString);
			prepStmt.setInt(1, product_id);

			rs=prepStmt.executeQuery();

			while (rs.next()) {
				int category_id=rs.getInt("category_id");
				int subcategory_id=rs.getInt("subcategory_id");
				product_id=rs.getInt("product_id");
				String title=rs.getString("title");
				String description=rs.getString("description");
				Float price=rs.getFloat("price");
				String cond=rs.getString("cond");
				int pages=rs.getInt("pages");
				String author=rs.getString("author");
				String publisher=rs.getString("publisher");
				String lang=rs.getString("lang");
				String format_type=rs.getString("format_type");
				String image=rs.getString("image");
				Date expdate = rs.getTimestamp("expdate");
				System.out.println("Image : "+image);
				int biddingstatus=rs.getInt("biddingstatus");
				String seller_id = rs.getString("seller_id");
				int status = rs.getInt("status");
				Float shipcost=rs.getFloat("shipcost");

				//				int bid_id=rs.getInt("bid_id");
				int quantity=rs.getInt("quantity");
				
				book=new Book(category_id,subcategory_id,product_id,title,description,price,cond,pages,author,publisher,lang,format_type,image,biddingstatus,quantity,expdate,seller_id,status,shipcost);
				
				System.out.println(book.toString());
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return book;
	}
}
