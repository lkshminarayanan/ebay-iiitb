package com.ebay.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;

import com.ebay.services.Bidding;
import com.ebay.services.Services;
import com.ebay.util.DBconn;

public class Item {
	public Item(int category_id, int subcategory_id, int product_id,
			String title, String description, Float price, String cond,
			String image, int biddingstatus, int quantity, Date expdate, String seller_id,int status,float shipping) {
		this.category_id = category_id;
		this.subcategory_id = subcategory_id;
		this.product_id = product_id;
		this.title = title;
		this.description = description;
		this.price = price;
		this.cond = cond;
		this.image = image;
		this.biddingstatus = (biddingstatus==1)?true:false;
		this.quantity = quantity;
		this.expdate = expdate;
		this.seller_id = seller_id;
		this.setStatus(status);
		shippingCharges = shipping;
		if(this.biddingstatus){
			System.out.println("Constructor Bidding");
			bid = new Bidding(category_id, subcategory_id, product_id);
			bid.getBiddingDetails();
			this.setPrice(bid.getBidPrice());
		}
	}
	
	public String toString(){
		String str = null;
		str +="\n Category ID : " + category_id;
		str +="\n SubCAT ID : " + subcategory_id;
		str +="\n Pdct ID : " + product_id;
		str +="\n Title : " + title;
		str +="\n Bidding Status : " + biddingstatus;
		//bid.toString();
		return str;
	}
	
	public float getShippingCharges() {
		return shippingCharges;
	}

	public void setShippingCharges(float shippingCharges) {
		this.shippingCharges = shippingCharges;
	}

	public Bidding getBid() {
		return bid;
	}

	public void setBid(Bidding bid) {
		this.bid = bid;
	}

	
	public String getPriceText(){
		DecimalFormat df = new DecimalFormat("###.00");
		return df.format((double)price);
	}
	
	public String getShippingChargesText(){
		DecimalFormat df = new DecimalFormat("##0.00");
		return df.format((double)shippingCharges);
	}

	public String getTimeRemaining(){
		return Services.getEndTimeText(expdate);
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public int getProduct_id() {
		return product_id;
	}

	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}

	public int getCategory_id() {
		return category_id;
	}

	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}

	public boolean getBiddingstatus() {
		return biddingstatus;
	}

	public void setBiddingstatus(boolean biddingstatus) {
		this.biddingstatus = biddingstatus;
	}

	public int getSubcategory_id() {
		return subcategory_id;
	}

	public void setSubcategory_id(int subcategory_id) {
		this.subcategory_id = subcategory_id;
	}

	public int getproduct_id() {
		return product_id;
	}

	public void setproduct_id(int product_id) {
		this.product_id = product_id;
	}

	public float getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(float subtotal) {
		this.subtotal = subtotal;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public String getCond() {
		return cond;
	}

	public void setCond(String cond) {
		this.cond = cond;
	}
	
	public String getSeller_id() {
		return seller_id;
	}

	public void setSeller_id(String seller_id) {
		this.seller_id = seller_id;
	}

	public Date getExpdate() {
		return expdate;
	}

	public String getEndTime() {
		return new SimpleDateFormat("d MMMM yyyy, kk:mm:ss zz").format(expdate);
	}
	
	public void setExpdate(Date expdate) {
		this.expdate = expdate;
	}
	
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	private int category_id;
	private boolean biddingstatus;
	private int subcategory_id;
	private int product_id;
	private String title;
	private String description;
	private int quantity;
	private float price;
	private String cond;
	private String image;
	private float subtotal;
	private Bidding bid;
	private float shippingCharges;
	private Date expdate;
	private String seller_id;
	private int status;
	
	public Item(){
	
	}
	
	public boolean isExpired() {
		if(status == 0)
			return true;
		return false;
	}

	public static int getItemList(String query,LinkedHashMap<String, Item> lstbooks,int category_id, int subcategory_id){
		Connection connection = null;
		ResultSet rs = null;
		String desc = "";
		
		if(subcategory_id!=0)
			query = " AND subcategory_id = "+subcategory_id+" "+query;

		query = "select SQL_CALC_FOUND_ROWS * from "+Services.getTableName(category_id,subcategory_id)+" where status = 1 "+query+";";
		System.out.println("Query : "+query);
		try {
			connection=DBconn.getConnection();
			Statement stmt=connection.createStatement();

			rs=stmt.executeQuery(query);
			while (rs.next()) {
				
				subcategory_id=rs.getInt("subcategory_id");
				int product_id=rs.getInt("product_id");
				subcategory_id = rs.getInt("subcategory_id");
				String title=rs.getString("title");
				String description=rs.getString("description");
				Float price=rs.getFloat("price");
				String cond=rs.getString("cond");
				String image=rs.getString("image");
				int biddingstatus=rs.getInt("biddingstatus");
				//				int bid_id=rs.getInt("bid_id");
				int quantity=rs.getInt("quantity");
				Date expdate = rs.getTimestamp("expdate");
				String seller_id = rs.getString("seller_id");
				int status = rs.getInt("status");
                float shipcost=rs.getFloat("shipcost");
				
				//Book book=new Book(category_id,subcategory_id,product_id,title,description,price,cond,pages,author,publisher,lang,format_type,image,biddingstatus,quantity,expdate,seller_id,status);
				
				switch (category_id) {
				case 1:
					desc = rs.getString("author")+" | "+rs.getString("lang")+" | "+rs.getString("format_type");
					break;
				case 2:
					desc = rs.getString("genre");
					break;
				case 3:
					desc = rs.getString("brand");
					break;
				default:
					break;
				}
				
				Item item = new Item(category_id, subcategory_id, product_id, title, description, price, cond, image, biddingstatus, quantity, expdate, seller_id, status,shipcost);
				
				System.out.println(item.toString());
				lstbooks.put(desc,item);
			}
			query = "SELECT FOUND_ROWS();";
			rs = stmt.executeQuery(query);
			while (rs.next()) {
				return rs.getInt(1);
			}
		}
		catch(Exception e)
		{
			System.out.println("error is"+e.getMessage());
			e.printStackTrace();

		}
		return -1;
	}

	public static Item getItemDetails(int category_id,int subcategory_id,int product_id) {
		Item item = null;
		PreparedStatement prepStmt = null;
		try {
			Connection connection=null;
			connection=DBconn.getConnection();
			ResultSet rs = null;			
			String QueryString = "select * from "+Services.getTableName(category_id, subcategory_id)+" where product_id=?";
			prepStmt = connection.prepareStatement(QueryString);
			prepStmt.setInt(1, product_id);

			rs=prepStmt.executeQuery();

			while (rs.next()) {
				product_id=rs.getInt("product_id");
				String title=rs.getString("title");
				String description=rs.getString("description");
				Float price=rs.getFloat("price");
				String cond=rs.getString("cond");
				String image=rs.getString("image");
				Date expdate = rs.getTimestamp("expdate");
				System.out.println("Image : "+image);
				int biddingstatus=rs.getInt("biddingstatus");
				String seller_id = rs.getString("seller_id");
				int status = rs.getInt("status");

				int quantity=rs.getInt("quantity");
                float shipcost=rs.getFloat("shipcost");
				
				item = new Item(category_id, subcategory_id, product_id, title, description, price, cond, image, biddingstatus, quantity, expdate, seller_id, status,shipcost);
				
				System.out.println(item.toString());
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return item;
	}


}
