/**
 * 
 */
package com.ebay.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;
import java.util.LinkedHashMap;

import com.ebay.services.Services;
import com.ebay.util.DBconn;



public class Music extends Item {
	
	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	private String genre;

	public Music(int category_id, int subcategory_id, int product_id,
			String title, String description, Float price, String cond,
			String image, int biddingstatus, int quantity, Date expdate,
			String seller_id, int status, String genre,Float shipcost) {
		super(category_id, subcategory_id, product_id, title, description,
				price, cond, image, biddingstatus, quantity, expdate,
				seller_id, status,shipcost);
		this.genre = genre;
	}
	
	public static int getItemList(String query,LinkedHashMap<String, Object> lstbooks){
		Connection connection = null;
		ResultSet rs = null;
		
		query = "select SQL_CALC_FOUND_ROWS * from musiccassette where status = 1"+query+";";
		System.out.println("Query : "+query);
		try {
			connection=DBconn.getConnection();
			Statement stmt=connection.createStatement();

			rs=stmt.executeQuery(query);
			while (rs.next()) {
				int category_id=rs.getInt("category_id");
				int subcategory_id=rs.getInt("subcategory_id");
				int product_id=rs.getInt("product_id");
				String title=rs.getString("title");
				String description=rs.getString("description");
				Float price=rs.getFloat("price");
				String cond=rs.getString("cond");
				String image=rs.getString("image");
				int biddingstatus=rs.getInt("biddingstatus");
				//				int bid_id=rs.getInt("bid_id");
				int quantity=rs.getInt("quantity");
				Date expdate = rs.getTimestamp("expdate");
				String seller_id = rs.getString("seller_id");
				int status = rs.getInt("status");
				String genre = rs.getString("genre");
                Float shipcost=rs.getFloat("shipcost");
				Music music = new Music(category_id, subcategory_id, product_id, title, description, price, cond, image, biddingstatus, quantity, expdate, seller_id, status, genre,shipcost);
				
				System.out.println(music.toString());
				lstbooks.put(genre,music);
			}
			query = "SELECT FOUND_ROWS();";
			rs = stmt.executeQuery(query);
			while (rs.next()) {
				return rs.getInt(1);
			}
		}
		catch(Exception e)
		{
			System.out.println("error is"+e.getMessage());

		}
		return -1;
	}




	public static Music getItemDetails(int category_id,int subcategory_id,int product_id) {
		Music music = null;
		PreparedStatement prepStmt = null;
		try {
			Connection connection=null;
			connection=DBconn.getConnection();
			ResultSet rs = null;			
			String QueryString;
			QueryString = "select * from music"+Services.getSubCategoryName(category_id, subcategory_id)+" where product_id=?";
			prepStmt = connection.prepareStatement(QueryString);
			prepStmt.setInt(1, product_id);

			rs=prepStmt.executeQuery();

			while (rs.next()) {
				
				subcategory_id=rs.getInt("subcategory_id");
				product_id=rs.getInt("product_id");
				String title=rs.getString("title");
				String description=rs.getString("description");
				Float price=rs.getFloat("price");
				String cond=rs.getString("cond");
				String image=rs.getString("image");
				Date expdate = rs.getTimestamp("expdate");
				System.out.println("Image : "+image);
				int biddingstatus=rs.getInt("biddingstatus");
				String seller_id = rs.getString("seller_id");
				int status = rs.getInt("status");
				String genre = rs.getString("genre");
                Float shipcost=rs.getFloat("shipcost");
				//				int bid_id=rs.getInt("bid_id");
				int quantity=rs.getInt("quantity");
				
				music = new Music(category_id, subcategory_id, product_id, title, description, price, cond, image, biddingstatus, quantity, expdate, seller_id, status, genre,shipcost);
				
				System.out.println(music.toString());
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return music;
	}
	
	
}
