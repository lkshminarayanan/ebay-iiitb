package com.ebay.model;

import java.sql.*;



import java.util.*;
import com.ebay.util.*;
import com.opensymphony.xwork2.ActionContext;

public class Categorymodel {
	private int category_id,subcategory_id;
	int i;
	ResultSet rs;
	private String category_name,subcategory_name;
	public int getSubcategory_id() {
		return subcategory_id;
	}
	public void setSubcategory_id(int subcategory_id) {
		this.subcategory_id = subcategory_id;
	}
	public String getSubcategory_name() {
		return subcategory_name;
	}
	public void setSubcategory_name(String subcategory_name) {
		this.subcategory_name = subcategory_name;
	}

	Connection con;
	Statement stmt;
	private String query;
	Map<String,Object> session=ActionContext.getContext().getSession();;
	DBconn db;
	
	
	public int getCategory_id() {
		return category_id;
	}
	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}
	public String getCategory_name() {
		return category_name;
	}
	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}
	
	public Categorymodel()
	{
		
	}
	
	public Categorymodel(int category_id2, String category_name2) {
		this.category_id = category_id2;
		this.category_name = category_name2;
	}
	
	public ArrayList<String> showCategoryforSearch()
	{
		ArrayList<String> list1=new ArrayList<String>();
		
		try
		{
			con=DBconn.getConnection();
			stmt=con.createStatement();
			query="select * from category";
			rs=stmt.executeQuery(query);
			while(rs.next())
			{
				category_id=rs.getInt("category_id");
				category_name=rs.getString("category_name");
				Categorymodel cat=new Categorymodel(category_id,category_name);
				list1.add(cat.getCategory_name());		
			}
		}
		catch(Exception e)
		{
			System.out.println(e.getLocalizedMessage());
		}
		
		return list1;
	}
	
	
	public ArrayList showCategory()
	{
		ArrayList list1=new ArrayList();
		
		try
		{
			con=DBconn.getConnection();
			stmt=con.createStatement();
			query="select * from category";
			rs=stmt.executeQuery(query);
			while(rs.next())
			{
				category_id=rs.getInt("category_id");
				category_name=rs.getString("category_name");
				Categorymodel cat=new Categorymodel(category_id,category_name);
				list1.add(cat);		
			}
		}
		catch(Exception e)
		{
			System.out.println(e.getLocalizedMessage());
		}
		
		return list1;
	}
	public ArrayList showSubCategory()
	{
		System.out.println("inside sub category");
		ArrayList list2=new ArrayList();
		db=new DBconn();
		category_id=(Integer) session.get("category_id");
		try
		{
			con=db.getConnection();
			stmt=con.createStatement();
			query="select * from subcategory where category_id='"+category_id+"'";
			rs=stmt.executeQuery(query);
			while(rs.next())
			{   
				Categorymodel cm=new Categorymodel();
				cm.subcategory_id=rs.getInt("subcategory_id");
				cm.subcategory_name=rs.getString("subcategory_name");
				System.out.println("inside sub category"+rs.getInt("subcategory_id") +rs.getString("subcategory_name"));
				list2.add(cm);		
			}
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
		
		return list2;
	}
	
	public int insertCategory()
	{		
		category_id=this.getCategory_id();
		category_name=this.getCategory_name();
		db=new DBconn();
		
		try
		{
			con=db.getConnection();			
			stmt=con.createStatement();
			query="insert into category(category_name) values('"+ category_name + "')";
			i=stmt.executeUpdate(query);
			
		}
		
		catch(Exception e)
		{
			System.out.println(e.getLocalizedMessage());
		}
		return i;
		
		
	}
	
}
