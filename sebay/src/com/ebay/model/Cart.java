package com.ebay.model;
import com.ebay.util.*;
import com.opensymphony.xwork2.ActionContext;

import java.text.DecimalFormat;
import java.util.*;
import java.sql.*;
public class Cart {

	private int category_id;
	private int subcategory_id;
	private String seller_id;
	private String image;
	private String user_id;
	private String title;
	private float price;
	private int quantitypurchased;
	private float sub_total;
	private int product_id;
	private String details;
	



	private ArrayList<Cart> cartItems;
	ResultSet rs=null;
	Statement stmt=null;
	DBconn db;
	Map<String,Object> session;
	
	
	
	
	@Override
	public String toString() {
		return "Cart [category_id=" + category_id + ", subcategory_id="
				+ subcategory_id + ", seller_id=" + seller_id
				+ ", image=" + image
				+ ", user_id=" + user_id + ", title=" + title + ", price="
				+ price + ", quantitypurchased=" + quantitypurchased
				+ ", sub_total=" + sub_total + ", product_id=" + product_id
				+ ", details=" + details + ", cartItems=" + cartItems + ", rs="
				+ rs + ", stmt=" + stmt + ", db=" + db + ", session=" + session
				+ ", i=" + i + "]";
	}
	public String getSeller_id() {
		return seller_id;
	}
	public void setSeller_id(String seller_id) {
		this.seller_id = seller_id;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public int getQuantitypurchased() {
		return quantitypurchased;
	}
	public void setQuantitypurchased(int quantitypurchased) {
		this.quantitypurchased = quantitypurchased;
	}
	public float getSub_total() {
		return sub_total;
	}
	public void setSub_total(float sub_total) {
		this.sub_total = sub_total;
	}
	public String getPriceText(){
		DecimalFormat df = new DecimalFormat("##0.00");
		return df.format((double)price);
	}
	public String getSubTotalText(){
		DecimalFormat df = new DecimalFormat("##0.00");
		return df.format((double)sub_total);
	}
	
	
	int i;
	
	public int insert()
	{
		System.out.println("in insert");
		db=new DBconn();
		category_id=getCategory_id();
		subcategory_id=getSubcategory_id();
		System.out.println("category"+category_id);
		title=getTitle();
		user_id=getUser_id();
		
		seller_id=getSeller_id();
		
		product_id=getProduct_id();
		
		title=getTitle();
		image=getImage();
		//image=this.getImage();
		
		quantitypurchased=getQuantitypurchased();
		
		quantitypurchased = (quantitypurchased==0)?1:quantitypurchased;
		
		//sub_total=this.getSub_total();
		try
		{
			
			Connection con=DBconn.getConnection();
			String query="insert into cart values('"+user_id+"','"+seller_id+"','"+product_id+"','"+title+"','"+quantitypurchased+"',0,'"+category_id+"','"+subcategory_id+"','"+price+"','"+image+"')";
			System.out.println("values 1"+title+"values 1"+quantitypurchased+"values 1"+price);
			Statement stmt=con.createStatement();
			i=stmt.executeUpdate(query);
			
		}
		catch(Exception e)
		{
			System.out.println("error is"+e.getMessage());
		}
		return i;
	}
	
	public String showcart()
	{
		try
		{
		System.out.println("in show cart");
		session=ActionContext.getContext().getSession();
		String user_id=(String)session.get("ebay_id");
		cartItems=new ArrayList<Cart>();
		System.out.println("user id in show is"+user_id);
		Connection con=DBconn.getConnection();
		//String query="select distinct seller_id from cart where buyer_id="+user_id+" and status=0 ";
		String query1="select * from cart where buyer_id='"+user_id+"'";
		
		//String query1="select fname,lname from users where user_id in(select seller_id from dummyusercart1 where user_id="+user_id1+")";
		
			System.out.println("in seller_name");
			stmt=con.createStatement();
			rs=stmt.executeQuery(query1);
			while(rs.next())
			{
				System.out.println("keppeing count");
				Cart c=new Cart();
				c.seller_id=rs.getString("seller_id");
				System.out.println("seller id is"+rs.getString("seller_id"));
				c.image=rs.getString("image");
				c.title=rs.getString("title");
				c.price=rs.getFloat("price");
				c.category_id=rs.getInt("category_id");
				c.subcategory_id=rs.getInt("subcategory_id");
				c.product_id=rs.getInt("product_id");
				c.quantitypurchased=rs.getInt("quantitypurchased");
				c.sub_total=rs.getInt("quantitypurchased")*rs.getFloat("price");
		        cartItems.add(c);
		        System.out.println(c.toString());
			}		 
				
			setCartItems(cartItems);
			
			
			
		}
		catch(Exception e)
		{
			System.out.println("error in cart"+e.getMessage());
			System.out.println(e.getLocalizedMessage());
		}
		return "success";
	}
	
	public String removeCart(){
		session=ActionContext.getContext().getSession();
		user_id=(String)session.get("ebay_id");
		category_id=getCategory_id();
		subcategory_id=getSubcategory_id();
		product_id=getProduct_id();
		Connection con=DBconn.getConnection();
		try {
			Statement s=con.createStatement();
			String query = "delete from cart where buyer_id='"+user_id+"' AND category_id="+category_id+" AND subcategory_id="+subcategory_id+" AND product_id="+product_id+";";
			System.out.println(query);
			s.executeUpdate(query);
	
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("error in cart"+e.getMessage());
			e.printStackTrace();
		}
		return "success";
		
	}
	public ArrayList<Cart> getCartItems() {
		return cartItems;
	}
	public void setCartItems(ArrayList<Cart> cartItems) {
		this.cartItems = cartItems;
	}
	public int getProduct_id() {
		return product_id;
	}
	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}
	public int getCategory_id() {
		return category_id;
	}
	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}
	public int getSubcategory_id() {
		return subcategory_id;
	}
	public void setSubcategory_id(int subcategory_id) {
		this.subcategory_id = subcategory_id;
	}
	
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	public static boolean isAlreadyInCart(int category_id,
			int subcategory_id, int product_id, String ebay_id) {
		String query = "select * from cart where category_id = "+category_id+" AND " +
				"subcategory_id = "+subcategory_id+" AND product_id = "+product_id;
		ResultSet rs;
		try {
			rs = DBconn.getConnection().createStatement().executeQuery(query);
			if(rs.next()){
				return true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}
}
