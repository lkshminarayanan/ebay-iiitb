package com.ebay.services;

import com.opensymphony.*;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.ebay.model.*;
import com.ebay.util.*;
import java.util.*;
import java.sql.*;

public class updateTable extends ActionSupport {

	private int category_id;
	private int subcategory_id;
	// private int user_id1;
	private int seller_id1;
	private String user_id;
	private String seller_id;
	private int product_id;
	private String title;
	private String image;
	private float price;
	private float subtotal;
	public String tableName;
	private ArrayList<Cart> listitems;
	DBconn db;
	Map session;
	private int quantitypurchased;
	int i;
	private float sub_total;
	Connection con, con1, con2;
	Statement st1, st2, st3, st4, st5, st6, st7, st8, st9, st10, st11, st12,
			st13, st14;

	public void updateTB() {
		session = ActionContext.getContext().getSession();

		con = db.getConnection();
		try {
			st1 = con.createStatement();
			st2 = con.createStatement();
			st3 = con.createStatement();
			st4 = con.createStatement();
			st5 = con.createStatement();
			st6 = con.createStatement();
			st7 = con.createStatement();
			st8 = con.createStatement();
			st9 = con.createStatement();
			st10 = con.createStatement();
			st11 = con.createStatement();
			st12 = con.createStatement();
			st13 = con.createStatement();
			st14 = con.createStatement();
			st14.executeUpdate("update  bidding set status=2 where endtime<current_timestamp and status=0 ");

			ResultSet rs = st1
					.executeQuery("select * from bidding where status=2");
			while (rs.next()) {
//				Cart c = new Cart();
//				c.setSeller_id(rs.getString("seller_id"));
//				c.setUser_id(rs.getString("buyer_id"));
//				c.setCategory_id(rs.getInt("category_id"));
//				c.setSubcategory_id(rs.getInt("subcategory_id"));
//				c.setProduct_id(rs.getInt("product_id"));
//				c.setPrice(rs.getFloat("currprice"));
				
				setSeller_id(rs.getString("seller_id"));
				setUser_id(rs.getString("buyer_id"));
				setCategory_id(rs.getInt("category_id"));
				setSubcategory_id(rs.getInt("subcategory_id"));
				setProduct_id(rs.getInt("product_id"));
				setPrice(rs.getFloat("currprice"));
				setQuantitypurchased(1);
				ResultSet rs1 = st2
						.executeQuery("select category_name,subcategory_name from category,subcategory where category.category_id=subcategory.category_id and subcategory.subcategory_id='"
								+ getSubcategory_id() + "' ");
				if (rs1.next()) {
					String cat_name = rs1.getString("category_name");
					String sub_cat = rs1.getString("subcategory_name");
					System.out
							.println("In updateTable.java : Cattegory Name is "
									+ cat_name);
					System.out
							.println("In updateTable.java : SubCategory Name is "
									+ sub_cat);
					if (cat_name.equals("books"))
						tableName = "bookdetails";
					else
						tableName = cat_name + sub_cat;
				}
				System.out.println("In updateTable.java : Table Name is "
						+ tableName);
				/*ResultSet rs2 = st3.executeQuery("select * from " + tableName
						+ " where category_id=" + getCategory_id()
						+ " and subcategory_id=" + getSubcategory_id()
						+ " and product_id=" + getProduct_id() + "");
				if (rs2.next()) {
					c.setImage(rs2.getString("image"));
					c.setTitle(rs2.getString("title"));
				}
				c.insert();*/
				st4.executeUpdate("update  " + tableName
						+ " set status=0 where category_id=" + getCategory_id()
						+ " and subcategory_id=" + getSubcategory_id()
						+ " and product_id=" + getProduct_id() + " ");
				// st5.executeUpdate("update  bidding set status=2 where  category_id="+getCategory_id()+" and subcategory_id="+getSubcategory_id()+" and product_id="+getProduct_id()+" and endtime<current_timestamp and status=1 ");
				// st6.executeUpdate("update  userbidding set won=1 where  category_id="+getCategory_id()+" and subcategory_id="+getSubcategory_id()+" and product_id="+getProduct_id()+" and user_id='"+getUser_id()+"'");
				st7.executeUpdate("update  bidhistory set status=1 where  category_id="
						+ getCategory_id()
						+ " and subcategory_id="
						+ getSubcategory_id()
						+ " and product_id="
						+ getProduct_id()
						+ " and buyer_id='"
						+ getUser_id()
						+ "' ");
				
			}

			st3.executeUpdate("update bank_third_party set status=2 where date<current_timestamp and status=1 ");
			st8.executeUpdate("update musiccassette set status=0 where  expdate<current_timestamp");
			st9.executeUpdate("update musiccd set status=0 where  expdate<current_timestamp");
			st10.executeUpdate("update bookdetails set status=0 where  expdate<current_timestamp");
			st11.executeUpdate("update mobilesmartphone  set status=0 where  expdate<current_timestamp");
			st12.executeUpdate("update mobileaccessories set status=0 where  expdate<current_timestamp");
			con.close();
			con = db.getConnection();
			st1 = con.createStatement();
			st2 = con.createStatement();
			st3 = con.createStatement();
			st4 = con.createStatement();
			st5 = con.createStatement();
			st6 = con.createStatement();
			st7 = con.createStatement();
			rs = st1.executeQuery("select bank_acc_num,bank_name,amount from paisapayreg,bank_third_party where paisapayreg.ebay_id=bank_third_party.seller_id and status=2");
			while (rs.next()) {
				st2.executeUpdate("update bank_db set balance=balance+"
						+ rs.getFloat("amount") + " where bank_acc_num="
						+ rs.getInt("bank_acc_num") + " and bank_name='"
						+ rs.getString("bank_name") + "'");

			}
			st3.executeUpdate("update bank_third_party set status=4 where status=2 ");
			ResultSet rs1 = st4
					.executeQuery("select category_id,subcategory_id,product_id,buyer_id from bidding where endtime<current_timestamp and status=2");
			while (rs1.next()) {

				st6.executeUpdate("update  userbidding set won=2 where  category_id="
						+ rs1.getInt("category_id")
						+ " and subcategory_id="
						+ rs1.getInt("subcategory_id")
						+ " and product_id="
						+ rs1.getInt("product_id")
						+ " and user_id='"
						+ rs1.getString("buyer_id") + "' and won=1");

			}
			st5.executeUpdate("update  bidding set status=3 where endtime<current_timestamp and status=2 ");

			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("error in table" + e.getMessage());
			e.printStackTrace();
		}

		// return "success";
	}

	public int getCategory_id() {
		return category_id;
	}

	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}

	public int getSubcategory_id() {
		return subcategory_id;
	}

	public void setSubcategory_id(int subcategory_id) {
		this.subcategory_id = subcategory_id;
	}

	public int getSeller_id1() {
		return seller_id1;
	}

	public void setSeller_id1(int seller_id1) {
		this.seller_id1 = seller_id1;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getSeller_id() {
		return seller_id;
	}

	public void setSeller_id(String seller_id) {
		this.seller_id = seller_id;
	}

	public int getProduct_id() {
		return product_id;
	}

	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public float getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(float subtotal) {
		this.subtotal = subtotal;
	}

	public ArrayList<Cart> getListitems() {
		return listitems;
	}

	public void setListitems(ArrayList<Cart> listitems) {
		this.listitems = listitems;
	}

	public int getQuantitypurchased() {
		return quantitypurchased;
	}

	public void setQuantitypurchased(int quantitypurchased) {
		this.quantitypurchased = quantitypurchased;
	}

	public int getI() {
		return i;
	}

	public void setI(int i) {
		this.i = i;
	}

	public float getSub_total() {
		return sub_total;
	}

	public void setSub_total(float sub_total) {
		this.sub_total = sub_total;
	}

}
