package com.ebay.services;


import java.text.DecimalFormat;
import java.util.Date;

import com.ebay.util.DBconn;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class Bidding {

	@Override
	public String toString() {
		return "Bidding [product_id=" + product_id + ", category_id="
				+ category_id + ", subcategory_id=" + subcategory_id
				+ ", bidPrice=" + bidPrice + ", bidUserId=" + bidUserId
				+ ", StartTime=" + StartTime + ", EndTime=" + EndTime
				+ ", noOfBidders=" + noOfBidders + "]";
	}

	public Bidding(){
		minIncrement = 10;
	}

	public Bidding(int category_id, int subcategory_id, int product_id) {
		// TODO Auto-generated constructor stub
		minIncrement = 10;
		this.category_id = category_id;
		this.subcategory_id = subcategory_id;
		this.product_id = product_id;

	}

	public int getProduct_id() {
		return product_id;
	}

	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}

	public int getCategory_id() {
		return category_id;
	}

	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}

	public int getSubcategory_id() {
		return subcategory_id;
	}

	public void setSubcategory_id(int subcategory_id) {
		this.subcategory_id = subcategory_id;
	}

	public float getBidPrice() {
		return bidPrice;
	}

	public void setBidPrice(float bidPrice) {
		this.bidPrice = bidPrice;
	}

	public String getBidPriceText(){
		DecimalFormat df = new DecimalFormat("###.00");
		return df.format((double)bidPrice);
	}

	public String getBidUserId() {
		return bidUserId;
	}

	public void setBidUserId(String bidUserId) {
		this.bidUserId = bidUserId;
	}

	public Date getStartTime() {
		return StartTime;
	}

	public void setStartTime(Date startTime) {
		StartTime = startTime;
	}

	public Date getEndTime() {
		return EndTime;
	}

	public void setEndTime(Date endTime) {
		EndTime = endTime;
	}

	public int getNoOfBidders() {
		return noOfBidders;
	}

	public void setNoOfBidders(int noOfBidders) {
		this.noOfBidders = noOfBidders;
	}

	public float getMinIncrement() {
		return minIncrement;
	}

	public void setMinIncrement(float minIncrement) {
		this.minIncrement = minIncrement;
	}



	private int product_id;
	private int category_id;
	private int subcategory_id;
	private float bidPrice;
	private String bidUserId;
	private Date StartTime;
	private Date EndTime;
	private int noOfBidders;
	private float minIncrement;

	public float getNextValidBid(){
		if(noOfBidders==0)
			return bidPrice;
		return bidPrice + minIncrement;
	}

	public String getNextValidBidText(){
		DecimalFormat df = new DecimalFormat("###.00");
		return df.format((double)getNextValidBid());
	}

	public void getBiddingDetails() {

		String query = "select * from bidding where category_id = "+category_id+" AND " +
				"subcategory_id = "+subcategory_id+" AND product_id = "+product_id+";";
		Connection con = DBconn.getConnection();
		Statement stmt;
		try {
			stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while(rs.next()){
				this.setBidPrice(rs.getFloat("currprice"));
				this.setBidUserId(rs.getString("buyer_id"));
				this.setNoOfBidders(rs.getInt("noofbidders"));
				this.setStartTime(rs.getTimestamp("startingtime"));
				this.setEndTime(rs.getTimestamp("endtime"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public int placeBid(float bidAmount, String user_id) {
		// TODO Auto-generated method stub\
		int ret = 0;
		++noOfBidders;
		this.bidUserId = user_id;
		this.bidPrice = bidAmount;
		String query = "UPDATE bidding SET buyer_id = '"+user_id+"', " +
				"currprice = "+bidAmount+", noofbidders = "+noOfBidders+" " +
						"WHERE category_id = "+category_id+" AND subcategory_id = "
				+subcategory_id+" AND product_id = "+product_id;

		Connection conn = DBconn.getConnection();
		try {
			Statement stmt = conn.createStatement();
			Statement stmt1 = conn.createStatement();
			ret = stmt.executeUpdate(query);
			stmt1.executeUpdate("update userbidding set won=0 where category_id = "+category_id+" AND subcategory_id = "+subcategory_id+" AND product_id = "+product_id+" and won=1");
			query = "insert into userbidding (user_id, category_id, subcategory_id, " +
					"product_id, price,won) VALUES ('"+user_id+"',"+category_id+ 
					","+subcategory_id +","+product_id+", "+bidAmount+",1);";
			System.out.println(query);
			ret = stmt.executeUpdate(query);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		return ret;
	}
}
