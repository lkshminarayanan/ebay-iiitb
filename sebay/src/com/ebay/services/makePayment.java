package com.ebay.services;

import com.ebay.util.DBconn;
import com.opensymphony.*;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class makePayment {

	Map session;
	private String seller_id, buyer_id, paisapay_acnt, category_id,
			subcategory_id, product_id, bankname,accountno,custname,shipaddress,accntType,tableName;
	

	private float cost, balance, bill,price;
	int status, qnt, accnt_no,quantitypurchased,quantity;
	Connection con;
	Statement st;
	private String query;
	DBconn db;
	ResultSet rs;
    String date;
	public String updateAccount() {
		try {
	    session=ActionContext.getContext().getSession();
		accntType=(String) session.get("accntType");
		session.get("accntType");
        DateFormat df=new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Calendar cal=Calendar.getInstance();
		cal.add(cal.DATE,4);
		date = df.format(cal.getTime());
		session = ActionContext.getContext().getSession();
		setBuyer_id((String) session.get("ebay_id"));
		setSeller_id((String) session.get("seller_id"));
		setCategory_id((String) session.get("category_id"));
		setSubcategory_id((String) session.get("subcategory_id") );
		setProduct_id((String) session.get("product_id"));
		setPrice((Float) session.get("price"));
		setQnt((Integer) session.get("quantitypurchased"));
		setAccountno(accountno);
		setBankname(bankname);
		System.out.println("accnt-"+getAccountno()+"name-"+getBankname()+"quantity-"+getQnt());
		setShipaddress((String) session.get("shipaddress"));
		
		    con=db.getConnection();
			st=con.createStatement();
			ResultSet rs=st.executeQuery("select category_name,subcategory_name from category,subcategory where subcategory_id='"+getSubcategory_id()+"' and category.category_id='"+getCategory_id()+"'");
			rs.next();
			String category_name=rs.getString("category_name");
			String subcategory_name=rs.getString("subcategory_name");
			if(category_name.equals("books"))
			tableName="bookdetails";
		else
			tableName=category_name+subcategory_name;
		setTableName(tableName);
		Statement st5=con.createStatement();
		System.out.println("table is"+getTableName());
		ResultSet rs1=st5.executeQuery("select quantity,status from "+getTableName()+" where subcategory_id='"+getSubcategory_id()+"' and category_id='"+getCategory_id()+"' and product_id="+getProduct_id()+" ");
		rs1.next();
		setStatus(rs1.getInt("status"));
		setQuantity(rs1.getInt("quantity"));
		System.out.println("status-"+rs1.getInt("status")+"quantity--"+rs1.getInt("quantity")+"tablename--"+getTableName());
		if(getStatus()==0 || getQuantity()<getQnt())
			return "notAvailable";
		else if ((getQuantity()-getQnt())==0) {
			setStatus(0);
		}
		else
			setStatus(1);
		
		query = "insert into bank_third_party(category_id,subcategory_id,product_id,buyer_id,seller_id,amount,status,date) values('" + getCategory_id()
				+ "','" + getSubcategory_id() + "','" + getProduct_id() + "','"
				+ getBuyer_id() + "','" + getSeller_id() + "','" + getPrice()
				+ "',1,'"+date+"')";
		db = new DBconn();

		if (0 == checkBalance()) {

			return "accountCheck";
		}
		con = db.getConnection();

		
			
			String query2="update bank_db set balance=balance-"+getPrice()+" where bank_acc_num='"
							+ getAccountno()
							+ "' and bank_name='"
							+ getBankname()+ "'";
			
			
			Statement st3 = con.createStatement();
            int i=st.executeUpdate(query);
			System.out.print("update sttus is:"+i);
			i = st3.executeUpdate(query2);
			Statement st4 = con.createStatement();
			st4.executeUpdate("update "+tableName+" set quantity=quantity-'"+getQnt()+"',status='"+getStatus()+"' ");
			System.out.print("update status is:"+i);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("makePayment"+e.getMessage());
			e.printStackTrace();
		}
		return "success";
	}

	public int checkBalance() {
		System.out.println("in accnt check");
		float bankOverflow;
		session = ActionContext.getContext().getSession();
		con = db.getConnection();

		try {
			st = con.createStatement();
			ResultSet rs = st
					.executeQuery("select balance from  bank_db where bank_acc_num='"
							+ getAccountno()
							+ "' and bank_name='"
							+ getBankname()+ "' and cust_name='"+getCustname()+"' and bank_acc_type='"+getAccntType()+"'");
			System.out.println("in accnt check2-"+getPrice()+"bank"+getBankname());
			if (rs.next()) {
				if ((rs.getFloat("balance") < getPrice()) || !getAccntType().equals("debit")){
					
					System.out.println("in accnt check3");return 0;
					}
				if ((rs.getFloat("balance") < getPrice()) || !getAccntType().equals("credit")){
					bankOverflow=rs.getFloat("balance")+5000;//here 500 shows max credit given by bank
					if(bankOverflow < getBalance())
					return 0;
					else
						return 1;
					
				}
				else
					return 1;
			} else
				return 0;
		} catch (Exception e) {
         System.out.println("makePayment"+e.getMessage());
		}
		return 1;
	}

	public String prepPayment(){
		
		category_id=getCategory_id();
		subcategory_id=getSubcategory_id();
		product_id=getProduct_id();
		System.out.println("price "+getPrice()+"quantity"+getQuantitypurchased());
		quantitypurchased=getQuantitypurchased();
		seller_id=getSeller_id();
		session=ActionContext.getContext().getSession();
		//session.put("payItem",ap);
		session.put("category_id",getCategory_id());
		session.put("subcategory_id",getSubcategory_id());
		session.put("product_id",product_id);
		session.put("quantitypurchased",getQuantitypurchased());
		session.put("seller_id",getSeller_id());
		session.put("price",getPrice());
		return "success";
		
	}
	public String getSeller_id() {
		return seller_id;
	}
	public String getShipaddress() {
		return shipaddress;
	}

	public String getAccntType() {
		return accntType;
	}

	public void setAccntType(String accntType) {
		this.accntType = accntType;
	}

	public void setShipaddress(String shipaddress) {
		this.shipaddress = shipaddress;
	}
	public void setSeller_id(String seller_id) {
		this.seller_id = seller_id;
	}

	public String getBuyer_id() {
		return buyer_id;
	}

	public void setBuyer_id(String buyer_id) {
		this.buyer_id = buyer_id;
	}

	public String getPaisapay_acnt() {
		return paisapay_acnt;
	}

	public void setPaisapay_acnt(String paisapay_acnt) {
		this.paisapay_acnt = paisapay_acnt;
	}

	public float getCost() {
		return cost;
	}

	public void setCost(float cost) {
		this.cost = cost;
	}

	public float getBalance() {
		return balance;
	}

	public String getBankname() {
		return bankname;
	}

	public void setBankname(String bankname) {
		this.bankname = bankname;
	}

	public String getAccountno() {
		return accountno;
	}

	public void setAccountno(String accountno) {
		this.accountno = accountno;
	}

	public String getCustname() {
		return custname;
	}

	public void setCustname(String custname) {
		this.custname = custname;
	}

	public void setBalance(float balance) {
		this.balance = balance;
	}

	public String getCategory_id() {
		return category_id;
	}

	public void setCategory_id(String category_id) {
		this.category_id = category_id;
	}

	public String getSubcategory_id() {
		return subcategory_id;
	}

	public void setSubcategory_id(String subcategory_id) {
		this.subcategory_id = subcategory_id;
	}

	public String getProduct_id() {
		return product_id;
	}

	public void setProduct_id(String product_id) {
		this.product_id = product_id;
	}

	public float getBill() {
		return bill;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public void setBill(float bill) {
		this.bill = bill;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getQnt() {
		return qnt;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public void setQnt(int qnt) {
		this.qnt = qnt;
	}

	public int getAccnt_no() {
		return accnt_no;
	}

	public void setAccnt_no(int accnt_no) {
		this.accnt_no = accnt_no;
	}

    
	public int getQuantitypurchased() {
		return quantitypurchased;
	}

	public void setQuantitypurchased(int quantitypurchased) {
		this.quantitypurchased = quantitypurchased;
	}
	

	
	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}
	
	
}
