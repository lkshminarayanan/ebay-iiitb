package com.ebay.myebay;

public class BidModel {
private String image;
private String name;
private String timeleft;
private int no_of_bids;
private Float price;
private String actions;
private String buyeruserid;
private String selleruserid;
private int category_id, subcategory_id;
private String product_id;
private String status;
private int won;

public String getImage() {
	return image;
}
public void setImage(String image) {
	this.image = image;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getTimeleft() {
	return timeleft;
}
public void setTimeleft(String timeleft) {
	this.timeleft = timeleft;
}
public int getNo_of_bids() {
	return no_of_bids;
}
public void setNo_of_bids(int no_of_bids) {
	this.no_of_bids = no_of_bids;
}
public Float getPrice() {
	return price;
}
public void setPrice(Float price) {
	this.price = price;
}
public String getActions() {
	return actions;
}
public void setActions(String actions) {
	this.actions = actions;
}
public String getBuyeruserid() {
	return buyeruserid;
}
public void setBuyeruserid(String buyeruserid) {
	this.buyeruserid = buyeruserid;
}
public String getSelleruserid() {
	return selleruserid;
}
public void setSelleruserid(String selleruserid) {
	this.selleruserid = selleruserid;
}
public String getProduct_id() {
	return product_id;
}
public void setProduct_id(String product_id) {
	this.product_id = product_id;
}
public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}
public int getWon() {
	return won;
}
public void setWon(int won) {
	this.won = won;
}
public int getSubcategory_id() {
	return subcategory_id;
}
public void setSubcategory_id(int subcategory_id) {
	this.subcategory_id = subcategory_id;
}
public int getCategory_id() {
	return category_id;
}
public void setCategory_id(int category_id) {
	this.category_id = category_id;
}

}
