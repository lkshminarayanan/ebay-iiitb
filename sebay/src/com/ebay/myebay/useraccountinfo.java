package com.ebay.myebay;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;



import com.ebay.admin.HomeAct;
import com.ebay.model.UserModel;
import com.ebay.util.DBconn;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;


public class useraccountinfo extends ActionSupport implements ServletRequestAware,ServletResponseAware{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String buyer_id;

	private HttpServletRequest request;
	private HttpServletResponse response;
	private String url = null;
	Map<String, Object> session1 ;
	private String fname;
	private String lname;
	private String streetaddress;
	private int zip;
	private String city;
	private String state;
	private String country;
	private String email;
	private String phone_no;
	private String ebay_id;
	private int status;
	private String user_id;
	private String dob;
	private String dobm;
	
	private int biddingstatus;
	private int salestatus;
	private int purchasestatus;
	private int category_id;
	private int subcategory_id;
	private int product_id;
	private String image;
	private String title;
	private Float price;
	private String description;
	private ArrayList<UserAccountModel>listbid;
	private ArrayList<BidModel>listbidm;
	private ArrayList<UserAccountModel>listlossbid;
	private ArrayList<UserAccountModel>listsoldsale;
	private ArrayList<UserAccountModel>listunsoldsale;
	private ArrayList<UserAccountModel>listpurchase;
	private ArrayList<UserModel>listpersonalinfo;
	private ArrayList<String>listaddress;
	private ArrayList<String>listshippingaddress;
	public ArrayList<UserModel> getListpersonalinfo() {
		return listpersonalinfo;
	}
	public void setListpersonalinfo(ArrayList<UserModel> listpersonalinfo) {
		this.listpersonalinfo = listpersonalinfo;
	}
	public ArrayList<String> getListaddress() {
		return listaddress;
	}
	public void setListaddress(ArrayList<String> listaddress) {
		this.listaddress = listaddress;
	}
	public ArrayList<String> getListshippingaddress() {
		return listshippingaddress;
	}
	public void setListshippingaddress(ArrayList<String> listshippingaddress) {
		this.listshippingaddress = listshippingaddress;
	}

	private ArrayList<UserAccountModel>listsale;
	Connection con;
	ResultSet rs;
	Statement stmt;
	PreparedStatement prepstmt;
	int i;
	
	
	public String getBuyer_id() {
		return buyer_id;
	}
	public void setBuyer_id(String buyer_id) {
		this.buyer_id = buyer_id;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	
	public ArrayList<UserAccountModel> getListlossbid() {
		return listlossbid;
	}
	public void setListlossbid(ArrayList<UserAccountModel> listlossbid) {
		this.listlossbid = listlossbid;
	}
	public ArrayList<UserAccountModel> getListsoldsale() {
		return listsoldsale;
	}
	public void setListsoldsale(ArrayList<UserAccountModel> listsoldsale) {
		this.listsoldsale = listsoldsale;
	}
	public ArrayList<UserAccountModel> getListunsoldsale() {
		return listunsoldsale;
	}
	public void setListunsoldsale(ArrayList<UserAccountModel> listunsoldsale) {
		this.listunsoldsale = listunsoldsale;
	}


	public int getBiddingstatus() {
		return biddingstatus;
	}
	public void setBiddingstatus(int biddingstatus) {
		this.biddingstatus = biddingstatus;
	}
	public int getSalestatus() {
		return salestatus;
	}
	public void setSalestatus(int salestatus) {
		this.salestatus = salestatus;
	}
	public int getPurchasestatus() {
		return purchasestatus;
	}
	public void setPurchasestatus(int purchasestatus) {
		this.purchasestatus = purchasestatus;
	}
	public int getCategory_id() {
		return category_id;
	}
	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}
	public int getSubcategory_id() {
		return subcategory_id;
	}
	public void setSubcategory_id(int subcategory_id) {
		this.subcategory_id = subcategory_id;
	}
	public int getProduct_id() {
		return product_id;
	}
	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Float getPrice() {
		return price;
	}
	public void setPrice(Float price) {
		this.price = price;
	}
	public ArrayList<UserAccountModel> getListbid() {
		return listbid;
	}
	public void setListbid(ArrayList<UserAccountModel> listbid) {
		this.listbid = listbid;
	}
	public ArrayList<UserAccountModel> getListpurchase() {
		return listpurchase;
	}
	public void setListpurchase(ArrayList<UserAccountModel> listpurchase) {
		this.listpurchase = listpurchase;
	}
	public ArrayList<UserAccountModel> getListsale() {
		return listsale;
	}
	public void setListsale(ArrayList<UserAccountModel> listsale) {
		this.listsale = listsale;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}


	
			public HttpServletResponse getResponse() {
		return response;
	}

	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}

	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	@Override
	public void setServletResponse(HttpServletResponse res) {
		this.response = res;
	}

	@Override
	public void setServletRequest(HttpServletRequest req) {
		this.request = req;

	}


	
	public String showbids()
	{
		
		System.out.println("In show bids");
		Map<String,Object> session = ActionContext.getContext().getSession();
		session1=ActionContext.getContext().getSession();
		session1.put("status", "bidshow");
		
		System.out.println(session1.get("status"));
		if(session == null || session.get("loggedin") == null || session.get("user") == null || !((String) session.get("loggedin")).equals("true") ){
			url = "/"+ActionContext.getContext().getName()+"?"+request.getQueryString();
			System.out.println("URL : "+url); 
			return LOGIN;
		}
		new HomeAct().show();
		String u = (String)session.get("ebay_id");
		UserAccountModel um=new UserAccountModel();
		System.out.println(u);
		listbidm=um.getbids(u);
		setListbidm(listbidm);	
		
		return SUCCESS;
	}
	
	public String loosebids()
	{

		Map<String,Object> session = ActionContext.getContext().getSession();
		session1=ActionContext.getContext().getSession();
		session1.put("status", "loosebidshow");
		
		if(session == null || session.get("loggedin") == null || session.get("ebay_id") == null || !((String) session.get("loggedin")).equals("true") ){
			url = "/"+ActionContext.getContext().getName()+"?"+request.getQueryString();
			System.out.println("URL : "+url); 
			return LOGIN;
		}
		new HomeAct().show();
		UserModel u = (UserModel)session.get("ebay_id");
		UserAccountModel um=new UserAccountModel();
		listlossbid=um.getlossbids(u.getEbay_id());
		setListlossbid(listlossbid);	
		
		return SUCCESS;

		
	}
	
	public String purchasehistory()
	{
		
		session1=ActionContext.getContext().getSession();
		session1.put("status", "purchasehistoryshow");
		
		Map<String,Object> session = ActionContext.getContext().getSession();

		if(session == null || session.get("loggedin") == null || session.get("user") == null || !((String) session.get("loggedin")).equals("true") ){
			url = "/"+ActionContext.getContext().getName()+"?"+request.getQueryString();
			System.out.println("URL : "+url); 
			return LOGIN;
		}
		new HomeAct().show();
		String u = (String)session.get("ebay_id");
		UserAccountModel um=new UserAccountModel();
		listpurchase=um.getpurchasehistory(u);
		setListpurchase(listpurchase);	

		return SUCCESS;
	}
	
	public String allsales()
	{
		session1=ActionContext.getContext().getSession();
		session1.put("status", "allsalesshow");
		HomeAct ha =new HomeAct();
		ha.show();
		Map<String,Object> session = ActionContext.getContext().getSession();

		if(session == null || session.get("loggedin") == null || session.get("user") == null || !((String) session.get("loggedin")).equals("true") ){
			url = "/"+ActionContext.getContext().getName()+"?"+request.getQueryString();
			System.out.println("URL : "+url); 
			return LOGIN;
		}
		
		String u = (String)session.get("ebay_id");
		UserAccountModel um=new UserAccountModel();
		listsale=um.getsalehistory(u);
		setListsale(listsale);	

		
		return SUCCESS;
	}

	public String soldsales()
	{
		
		session1=ActionContext.getContext().getSession();
		session1.put("status", "soldsalesshow");
		new HomeAct().show();
		Map<String,Object> session = ActionContext.getContext().getSession();

		if(session == null || session.get("loggedin") == null || session.get("ebay_id") == null || !((String) session.get("loggedin")).equals("true") ){
			url = "/"+ActionContext.getContext().getName()+"?"+request.getQueryString();
			System.out.println("URL : "+url); 
			return LOGIN;
		}
		
		UserModel u = (UserModel)session.get("ebay_id");
		UserAccountModel um=new UserAccountModel();
		listsoldsale=um.getsoldsalehistory(u.getEbay_id());
		setListsoldsale(listsoldsale);	


		
		return SUCCESS;
	}
	
	public String unsoldsales()
	{
		
		session1=ActionContext.getContext().getSession();
		session1.put("status", "unsoldsalesshow");
		new HomeAct().show();
		Map<String,Object> session = ActionContext.getContext().getSession();

		if(session == null || session.get("loggedin") == null || session.get("ebay_id") == null || !((String) session.get("loggedin")).equals("true") ){
			url = "/"+ActionContext.getContext().getName()+"?"+request.getQueryString();
			System.out.println("URL : "+url); 
			return LOGIN;
		}
		
		UserModel u = (UserModel)session.get("ebay_id");
		UserAccountModel um=new UserAccountModel();
		listunsoldsale=um.getunsoldsalehistory(u.getEbay_id());
		setListunsoldsale(listunsoldsale);	


		return SUCCESS;
	}
	
	public String feedback()
	{
		new HomeAct().show();
		return SUCCESS;
	}
	
	public String address()
	{

		status=1;
		setStatus(status);
		session1=ActionContext.getContext().getSession();
		session1.put("status", "addressshow");
		new HomeAct().show();
		Map<String,Object> session = ActionContext.getContext().getSession();

		if(session == null || session.get("loggedin") == null || session.get("ebay_id") == null || !((String) session.get("loggedin")).equals("true") ){
			url = "/"+ActionContext.getContext().getName()+"?"+request.getQueryString();
			System.out.println("URL : "+url); 
			return LOGIN;
		}
		
		String u = (String)session.get("ebay_id");
		UserModel um=new UserModel();
		System.out.println("Address");
		listaddress=um.getaddress(u);
		System.out.println("Shipping Address");
		listshippingaddress=um.getshippingaddress(u);
		setListshippingaddress(listshippingaddress);
		setListaddress(listaddress);

		
		return SUCCESS;
	}
	
	public String personalinfo()
	{

		status=2;
		setStatus(status);
		Map<String,Object> session = ActionContext.getContext().getSession();
		new HomeAct().show();

		if(session == null || session.get("loggedin") == null || session.get("ebay_id") == null || !((String) session.get("loggedin")).equals("true") ){
			url = "/"+ActionContext.getContext().getName()+"?"+request.getQueryString();
			System.out.println("URL : "+url); 
			return LOGIN;
		}

		System.out.println("status is"+getStatus());
		String u = (String)session.get("ebay_id");
		System.out.println("user id is"+u);
		System.out.println("to get arraylist for personal info");
		listpersonalinfo=getListpersonalinfo1(u);
		setListpersonalinfo(listpersonalinfo);
		
		
		return SUCCESS;
	}
	
	public String paypal()
	{
		new HomeAct().show();
		return SUCCESS;
	}

	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public ArrayList<BidModel> getListbidm() {
		return listbidm;
	}
	public void setListbidm(ArrayList<BidModel> listbidm) {
		this.listbidm = listbidm;
	}

public ArrayList<UserModel> getListpersonalinfo1(String user_id1) {
		
		listpersonalinfo=new ArrayList<UserModel>();
		Connection con=null;
		Statement stmt=null;
		ResultSet rs=null;
		System.out.println("in personalinfo");
		new HomeAct().show();
		try
		{
			con=DBconn.getConnection();
			String query="select * from users where ebay_id='"+user_id1+"'";
			stmt=con.createStatement();
			rs=stmt.executeQuery(query);
			while(rs.next())
			{
				UserModel m=new UserModel();
				m.setFname(rs.getString("fname"));
				System.out.println("In UserModel fname is"+rs.getString("fname"));
				m.setLname(rs.getString("lname"));
				m.setStreetaddress(rs.getString("streetaddress"));
				m.setZip(rs.getInt("zip"));
				m.setCity(rs.getString("city"));
				m.setState(rs.getString("state"));
				m.setCountry(rs.getString("country"));
				m.setEmail(rs.getString("email"));
				m.setPhone_no(rs.getString("phone_no"));
				Date dob = rs.getDate("dob");
				SimpleDateFormat sdf;
				sdf = new SimpleDateFormat("dd MMM yyyy");
				m.setDob(sdf.format(dob));
				m.setDob(rs.getString("dob"));
				m.setCity("city");
				
				listpersonalinfo.add(m);
			}
			
		}
		catch(Exception e)
		{
			System.out.println(e.getLocalizedMessage());
		}
		
		
		return listpersonalinfo;
	}
	

public String savedata()
{
	
	
	Map<String,Object> session = ActionContext.getContext().getSession();
	
	String u = (String)session.get("ebay_id");
	fname=this.getFname();
	lname=this.getLname();
	dob=this.getDob();
	streetaddress=this.getStreetaddress();
	city=this.getCity();
	state=this.getState();
	country=this.getCountry();
	zip=this.getZip();
	email=this.getEmail();
	phone_no=this.getPhone_no();
	try
	{
		Connection con=DBconn.getConnection();
		String query="update users set fname='"
		+fname+"',lname='"+lname+"',dob='"+dob+"',streetaddress='"+
		streetaddress+"',city='"+city+"'," +
				"state='"+state+"'," +
				"country='"+country+"'," +
				"zip="+zip+"," +
				"email='"+email+"'," +
				"phone_no='"+phone_no+"' where ebay_id='"+u+"'";
		Statement stmt=con.createStatement();
		i=stmt.executeUpdate(query);
		
	}
	catch(Exception e)
	{
		System.out.println(e.getLocalizedMessage());
	}
	
	if(i==1)
	{
	return SUCCESS;
	}
	else
	{
		return ERROR;
	}
}
public String getFname() {
	return fname;
}
public void setFname(String fname) {
	this.fname = fname;
}
public String getLname() {
	return lname;
}
public void setLname(String lname) {
	this.lname = lname;
}
public String getStreetaddress() {
	return streetaddress;
}
public void setStreetaddress(String streetaddress) {
	this.streetaddress = streetaddress;
}
public int getZip() {
	return zip;
}
public void setZip(int zip) {
	this.zip = zip;
}
public String getCity() {
	return city;
}
public void setCity(String city) {
	this.city = city;
}
public String getState() {
	return state;
}
public void setState(String state) {
	this.state = state;
}
public String getCountry() {
	return country;
}
public void setCountry(String country) {
	this.country = country;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getPhone_no() {
	return phone_no;
}
public void setPhone_no(String phone_no) {
	this.phone_no = phone_no;
}
public String getEbay_id() {
	return ebay_id;
}
public void setEbay_id(String ebay_id) {
	this.ebay_id = ebay_id;
}
public String getDob() {
	return dob;
}
public void setDob(String dob) {
	this.dob = dob;
}
public String getDobm() {
	return dobm;
}
public void setDobm(String dobm) {
	this.dobm = dobm;
}
	
}
