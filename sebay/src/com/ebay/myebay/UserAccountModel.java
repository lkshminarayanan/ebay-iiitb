package com.ebay.myebay;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;
import java.sql.*;
import com.ebay.util.*;

public class UserAccountModel {

	private String buyer_id;
	private String user_id;
	private int biddingstatus;
	private int salestatus;
	private int purchasestatus;
	private int category_id;
	private int subcategory_id;
	private int product_id;
	private String image;
	private String title;
	private Float price;
	private String description;
	private ArrayList<UserAccountModel> listbid;
	private ArrayList<BidModel> listbidm;
	private ArrayList<UserAccountModel> listpurchase;
	private ArrayList<UserAccountModel> listsale;
	Connection con;
	ResultSet rs;
	Statement stmt;
	PreparedStatement prepstmt;
	int i;

	public String getBuyer_id() {
		return buyer_id;
	}

	public void setBuyer_id(String buyer_id) {
		this.buyer_id = buyer_id;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public int getBiddingstatus() {
		return biddingstatus;
	}

	public void setBiddingstatus(int biddingstatus) {
		this.biddingstatus = biddingstatus;
	}

	public int getSalestatus() {
		return salestatus;
	}

	public void setSalestatus(int salestatus) {
		this.salestatus = salestatus;
	}

	public int getPurchasestatus() {
		return purchasestatus;
	}

	public void setPurchasestatus(int purchasestatus) {
		this.purchasestatus = purchasestatus;
	}

	public int getCategory_id() {
		return category_id;
	}

	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}

	public int getSubcategory_id() {
		return subcategory_id;
	}

	public void setSubcategory_id(int subcategory_id) {
		this.subcategory_id = subcategory_id;
	}

	public int getProduct_id() {
		return product_id;
	}

	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	public ArrayList<UserAccountModel> getListbid() {
		return listbid;
	}

	public void setListbid(ArrayList<UserAccountModel> listbid) {
		this.listbid = listbid;
	}

	public ArrayList<UserAccountModel> getListpurchase() {
		return listpurchase;
	}

	public void setListpurchase(ArrayList<UserAccountModel> listpurchase) {
		this.listpurchase = listpurchase;
	}

	public ArrayList<UserAccountModel> getListsale() {
		return listsale;
	}

	public void setListsale(ArrayList<UserAccountModel> listsale) {
		this.listsale = listsale;
	}

	public ArrayList<UserAccountModel> getpurchase(String user_id1) {
		return listpurchase;
	}

	public ArrayList<UserAccountModel> getsale(String user_id1) {
		return listsale;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ArrayList<BidModel> getbids(String j) {
		listbid = new ArrayList<UserAccountModel>();
		listbidm = new ArrayList<BidModel>();
		con = DBconn.getConnection();
		// String
		// query="select * from userpurchasesale where user_id="+user_id1+" and biddingstatus=1";
		String query = "SELECT U.category_id, U.subcategory_id, U.product_id, U.price, C.category_name, "
				+ "			S.subcategory_name, B.endtime, B.noofbidders, B.status, U.won "
				+ "	FROM UserBidding U, Category C, Subcategory S, Bidding B "
				+ "	WHERE U.user_id='"
				+ j
				+ "' AND U.category_id=C.category_id AND C.category_id=S.category_id AND U.subcategory_id=S.subcategory_id AND U.product_id=B.product_id AND U.category_id=B.category_id AND U.subcategory_id=B.subcategory_id";

		try {
			stmt = con.createStatement();
			rs = stmt.executeQuery(query);
			while (rs.next()) {

				/*
				 * UserAccountModel m=new UserAccountModel();
				 * 
				 * m.setBiddingstatus(rs.getInt("biddingstatus"));
				 * System.out.println
				 * ("In getbids:biddingstatus"+rs.getInt("biddingstatus"));
				 * m.setSalestatus(rs.getInt("salestatus"));
				 * m.setPurchasestatus(rs.getInt("purchasestatus"));
				 * m.setCategory_id(rs.getInt("category_id"));
				 * m.setSubcategory_id(rs.getInt("subcategory_id"));
				 * m.setImage(rs.getString("image"));
				 * System.out.println("In getbids:image"+rs.getString("image"));
				 * 
				 * m.setTitle(rs.getString("title"));
				 * m.setPrice(rs.getFloat("price"));
				 * m.setDescription(rs.getString("description"));
				 * listbid.add(m);
				 */

				int category_id = rs.getInt(1);
				int subcategory_id = rs.getInt(2);
				String product_id = rs.getString(3);
				price = rs.getFloat(4);
				System.out.println("in getBid: ");
				String table_name;
				if (rs.getString(5).toLowerCase().equals("books")
						|| rs.getString(5).toLowerCase().equals("book")) {
					table_name = "bookdetails";
				} else {
					table_name = rs.getString(5) + rs.getString(6);
				}
				System.out.println("in getBid: table name is " + table_name);
				Date endtime = rs.getDate(7);
				int no_of_bids = rs.getInt(8);
				String status;// = rs.getString(9);
				int won = rs.getInt(10);
				if(won==0)
					status="You Loose the bid.";
				else if(won==1)
					status="Result Pending...";
				else
					status ="You Won the Bid!!!";
				
				SimpleDateFormat sdf;
				sdf = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss ");
				String timeleft = sdf.format(endtime);
				System.out.println("IN BidAction.java table name is "
						+ table_name);
				String q2 = "SELECT P.title, P.image FROM " + table_name
						+ " P WHERE P.product_id='" + product_id + "'";
				// q2="SELECT P.title, P.image FROM bookfiction P WHERE P.product_id= 1";
				Connection cn2 = DBconn.getConnection();
				Statement st2 = cn2.createStatement();
				ResultSet rs2 = st2.executeQuery(q2);
				if (rs2.next()) {
					title = rs2.getString(1);
					image = rs2.getString(2);
					BidModel bm = new BidModel();
					bm.setName(title);
					bm.setImage(image);
					bm.setProduct_id(product_id);
					bm.setCategory_id(category_id);
					bm.setSubcategory_id(subcategory_id);
					bm.setPrice(price);
					bm.setNo_of_bids(no_of_bids);
					bm.setTimeleft(timeleft);
					bm.setBuyeruserid(j);
					bm.setWon(won);
					
					bm.setStatus(status);
					
					listbidm.add(bm);
				}
			}
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			e.printStackTrace();
		}
		return listbidm;
	}

	public ArrayList<UserAccountModel> getlossbids(String user_id1) {
		listbid = new ArrayList<UserAccountModel>();
		con = DBconn.getConnection();
		String query = "select * from userbidding where user_id='" + user_id1
				+ "' and won=0";
		try {
			stmt = con.createStatement();
			rs = stmt.executeQuery(query);
			while (rs.next()) {
				UserAccountModel m = new UserAccountModel();
				m.setCategory_id(rs.getInt("category_id"));
				m.setSubcategory_id(rs.getInt("subcategory_id"));
				m.setImage(rs.getString("image"));
				m.setTitle(rs.getString("title"));
				m.setPrice(rs.getFloat("price"));
				m.setDescription(rs.getString("description"));
				listbid.add(m);

			}

		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
		}
		return listbid;
	}

	public ArrayList<UserAccountModel> getpurchasehistory(String user_id1) {
		listpurchase = new ArrayList<UserAccountModel>();
		con = DBconn.getConnection();
		String query = "select * from userpurchasesale where buyer_id='"
				+ user_id1 + "'";
		try {
			stmt = con.createStatement();
			rs = stmt.executeQuery(query);
			while (rs.next()) {
				System.out.println("In while of getpurchasehistory");
				UserAccountModel m = new UserAccountModel();
				// m.setBiddingstatus(rs.getInt("biddingstatus"));
				m.setSalestatus(rs.getInt("soldstatus"));
				m.setPurchasestatus(rs.getInt("purchasestatus"));
				int Category_id = (rs.getInt("category_id"));
				int Subcategory_id = (rs.getInt("subcategory_id"));
				int Product_id = (rs.getInt("product_id"));
				m.setCategory_id(Category_id);
				m.setSubcategory_id(Subcategory_id);
				m.setProduct_id(Product_id);
				System.out.println("In while of getpurchasehistory->"
						+ Category_id + Subcategory_id + Product_id);
				Connection c1 = DBconn.getConnection();
				Statement s1 = c1.createStatement();
				ResultSet r1 = s1
						.executeQuery("SELECT category_name from category where category_id="
								+ Category_id + "");
				if (r1.next()) {
					Connection c2 = DBconn.getConnection();
					Statement s2 = c2.createStatement();
					ResultSet r2 = s2
							.executeQuery("SELECT subcategory_name from subcategory where category_id="
									+ Category_id
									+ " AND subcategory_id="
									+ Subcategory_id + "");
					if (r2.next()) {
						String table_name;
						String cat = r1.getString(1).toLowerCase();
						System.out.println(cat);
						if (cat.equals("books") || cat.equals("book")) {
							table_name = "bookdetails";
						} else {
							table_name = r1.getString(1) + r2.getString(1);
						}
						System.out.println(table_name);
						Connection c3 = DBconn.getConnection();
						Statement s3 = c3.createStatement();
						ResultSet r3 = s3
								.executeQuery("SELECT title , image, price, description from "
										+ table_name
										+ " where product_id="
										+ Product_id + "");
						if (r3.next()) {
							System.out.println(r3.getString("title"));
							m.setImage(r3.getString("image"));
							m.setTitle(r3.getString("title"));
							m.setPrice(r3.getFloat("price"));
							 m.setDescription(r3.getString("description"));
							listpurchase.add(m);
							DBconn.close(c3);
						}
					}
					DBconn.close(c2);
				}
				DBconn.close(c1);

			}

		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			e.printStackTrace();
		}
		return listpurchase;
	}

	public ArrayList<UserAccountModel> getsalehistory(String user_id1) {
		listsale = new ArrayList<UserAccountModel>();
		con = DBconn.getConnection();
		String query = "select * from userpurchasesale where seller_id='"
				+ user_id1 + "'";
		try {
			stmt = con.createStatement();
			rs = stmt.executeQuery(query);
			while (rs.next()) {
				System.out.println("In while of getpurchasehistory");
				UserAccountModel m = new UserAccountModel();
				// m.setBiddingstatus(rs.getInt("biddingstatus"));
				m.setSalestatus(rs.getInt("soldstatus"));
				m.setPurchasestatus(rs.getInt("purchasestatus"));
				int Category_id = (rs.getInt("category_id"));
				int Subcategory_id = (rs.getInt("subcategory_id"));
				int Product_id = (rs.getInt("product_id"));
				m.setCategory_id(Category_id);
				m.setSubcategory_id(Subcategory_id);
				m.setProduct_id(Product_id);
				System.out.println("In while of getpurchasehistory->"
						+ Category_id + Subcategory_id + Product_id);
				Connection c1 = DBconn.getConnection();
				Statement s1 = c1.createStatement();
				ResultSet r1 = s1
						.executeQuery("SELECT category_name from category where category_id="
								+ Category_id + "");
				if (r1.next()) {
					Connection c2 = DBconn.getConnection();
					Statement s2 = c2.createStatement();
					ResultSet r2 = s2
							.executeQuery("SELECT subcategory_name from subcategory where category_id="
									+ Category_id
									+ " AND subcategory_id="
									+ Subcategory_id + "");
					if (r2.next()) {
						String table_name;
						String cat = r1.getString(1).toLowerCase();
						System.out.println(cat);
						if (cat.equals("books") || cat.equals("book")) {
							table_name = "bookdetails";
						} else {
							table_name = r1.getString(1) + r2.getString(1);
						}
						System.out.println(table_name);
						Connection c3 = DBconn.getConnection();
						Statement s3 = c3.createStatement();
						ResultSet r3 = s3
								.executeQuery("SELECT title , image, price, description from "
										+ table_name
										+ " where product_id="
										+ Product_id + "");
						if (r3.next()) {
							System.out.println(r3.getString("title"));
							m.setImage(r3.getString("image"));
							m.setTitle(r3.getString("title"));
							m.setPrice(r3.getFloat("price"));
							 m.setDescription(r3.getString("description"));
							listsale.add(m);
							DBconn.close(c3);
						}
					}
					DBconn.close(c2);
				}
				DBconn.close(c1);

			}

		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			e.printStackTrace();
		}
		return listsale;
	}

	public ArrayList<UserAccountModel> getsoldsalehistory(String user_id1) {
		listsale = new ArrayList<UserAccountModel>();
		con = DBconn.getConnection();
		String query = "select * from userpurchasesale where seller_id="
				+ user_id1 + " and soldstatus=1";
		try {
			stmt = con.createStatement();
			rs = stmt.executeQuery(query);
			while (rs.next()) {
				UserAccountModel m = new UserAccountModel();
				m.setBiddingstatus(rs.getInt("biddingstatus"));
				m.setSalestatus(rs.getInt("salestatus"));
				m.setPurchasestatus(rs.getInt("purchasestatus"));
				m.setCategory_id(rs.getInt("category_id"));
				m.setSubcategory_id(rs.getInt("subcategory_id"));
				m.setImage(rs.getString("image"));
				m.setTitle(rs.getString("title"));
				m.setPrice(rs.getFloat("price"));
				m.setDescription(rs.getString("description"));
				listsale.add(m);

			}

		} catch (Exception e) {
			System.err.println(e.getLocalizedMessage());
			e.printStackTrace();
		}
		return listsale;
	}

	public ArrayList<UserAccountModel> getunsoldsalehistory(String user_id1) {
		listsale = new ArrayList<UserAccountModel>();
		con = DBconn.getConnection();
		String query = "select * from userpurchasesale where user_id="
				+ user_id1 + " and soldstatus=0 and salestatus=1";
		try {
			stmt = con.createStatement();
			rs = stmt.executeQuery(query);
			while (rs.next()) {
				UserAccountModel m = new UserAccountModel();
				m.setBiddingstatus(rs.getInt("biddingstatus"));
				m.setSalestatus(rs.getInt("salestatus"));
				m.setPurchasestatus(rs.getInt("purchasestatus"));
				m.setCategory_id(rs.getInt("category_id"));
				m.setSubcategory_id(rs.getInt("subcategory_id"));
				m.setImage(rs.getString("image"));
				m.setTitle(rs.getString("title"));
				m.setPrice(rs.getFloat("price"));
				m.setDescription(rs.getString("description"));
				listsale.add(m);

			}

		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
		}
		return listsale;
	}

	public ArrayList<BidModel> getListbidm() {
		return listbidm;
	}

	public void setListbidm(ArrayList<BidModel> listbidm) {
		this.listbidm = listbidm;
	}

}
