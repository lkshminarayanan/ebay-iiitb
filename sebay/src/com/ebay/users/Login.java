package com.ebay.users;
import com.ebay.model.*;

import java.util.Map;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;


public class Login extends ActionSupport{
	Map<String,Object> session;
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUrl() {
		if(url == null|| url.equals(""))
			return "/home";
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String url;
	private String email;
	private String password;

	public void validate(){
		//TODO:Validations if any!!.
	}
	

	public String execute()
	{
		UserModel user=new UserModel();
		user.setEmail(email);
		setEmail(email);
        System.out.println("inside login"+getEmail());
		user.setPassword(password);
		int i=user.check(getEmail(),getPassword());
		if(i==1)
		{
			Map<String,Object> session = ActionContext.getContext().getSession();
			
//		session.put("email",email);	
			//TODO:put ebay id here
			session.put("loggedin","true");
			
			user.getdetails();
			session.put("user", user);
			
			System.out.println("Sign in URL : "+url);
			
			return SUCCESS;
		}
		else
		{
			return INPUT;
		}
	}
	public String logout(){
		System.out.println("logginout");
		session=ActionContext.getContext().getSession();
		session.clear();
		return "success";
		
	}
}
