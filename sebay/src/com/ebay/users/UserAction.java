package com.ebay.users;
import com.ebay.model.*;

import com.ebay.util.*;
import java.sql.*;
import java.util.Map;
import java.util.Properties;
import javax.servlet.http.*;
import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;


public class UserAction extends ActionSupport{
	public String getUrl() {
		if(url == null|| url.equals(""))
			return "/home";
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String msgtosend;
	String from;
	String to;
	String subject;
	String text;
	String fname;
	String lname;
	String streetaddress;
	int zip;
	String msg;
	HttpServletRequest request;
	HttpServletResponse response;
	String city;
	String state;
	String country;
	String email;
	String phone_no;
	String ebay_id;
	String password;
	String secret_question;
	String secret_answer;
	String dob;
	int i;
	int index;
	public String str;
	private String url;



	public void setStr(String str)
	{
		this.str=str;
	}

	public String getStr()
	{
		return str;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getStreetaddress() {
		return streetaddress;
	}
	public void setStreetaddress(String streetaddress) {
		this.streetaddress = streetaddress;
	}
	public int getZip() {
		return zip;
	}
	public void setZip(int zip) {
		this.zip = zip;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone_no() {
		return phone_no;
	}
	public void setPhone_no(String phone_no) {
		this.phone_no = phone_no;
	}
	public String getEbay_id() {
		return ebay_id;
	}
	public void setEbay_id(String ebay_id) {
		this.ebay_id = ebay_id;
	}
	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}
	public String getSecret_question() {
		return secret_question;
	}
	public void setSecret_question(String secret_question) {
		this.secret_question = secret_question;
	}
	public String getSecret_answer() {
		return secret_answer;
	}
	public void setSecret_answer(String secret_answer) {
		this.secret_answer = secret_answer;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}

	public void sendmail(String from, String to,String subject, String text)
	{
		this.from=from;
		this.to=to;
		this.subject=subject;
		this.text=text;

	}

	public void send()
	{
		Boolean debug=false;
		Properties props=new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port",465);
		Session mailSession = Session.getDefaultInstance(props,null);
		mailSession.setDebug(debug);
		Message simpleMessage = new MimeMessage(mailSession);

		InternetAddress fromAddress = null;
		InternetAddress toAddress = null;
		try {
			fromAddress = new InternetAddress(from);
			toAddress = new InternetAddress(to);
		} catch (AddressException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("mail problem");
		}

		try {
			simpleMessage.setFrom(fromAddress);
			simpleMessage.setRecipient(RecipientType.TO, toAddress);
			simpleMessage.setSubject(subject);
			simpleMessage.setText(text);

			Transport.send(simpleMessage);			
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		

	}


	public String execute()
	{		

		index=email.indexOf('@');
		System.out.println(index);
		int index1=email.lastIndexOf('.');
		System.out.println(index1);
		String str1=email.substring(index+1,index1);
		System.out.println(str1);
		if(str1.equals("gmail"))
		{
			str="https://www.gmail.com";
		}
		if(str1.equals("yahoo"))
		{
			str="https://www.yahoo.com";
		}
		if(str1.equals("rediff"))
		{
			str="https://www.rediff.com";
		}


		UserModel usr=new UserModel();

		usr.setCity(city);
		usr.setCountry(country);
		usr.setDob(dob);
		usr.setEbay_id(ebay_id);
		usr.setEmail(email);
		usr.setFname(fname);
		usr.setLname(lname);
		usr.setPassword(password);
		usr.setPhone_no(phone_no);
		usr.setSecret_answer(secret_answer);
		usr.setSecret_question(secret_question);
		usr.setState(state);
		usr.setStreetaddress(streetaddress);
		usr.setZip(zip);
		int a;

		if(usr.insertdata()==1)
		{
			System.out.println(email);
			msgtosend="Please confirm it";
			sendmail("somi.11ce@gmail.com", email, "ebay confirmation", msgtosend);
			send();
			return SUCCESS;
		}
		else
		{
			return ERROR;
		}

	}


	public String showaddress()
	{
		return SUCCESS;
	}

}
