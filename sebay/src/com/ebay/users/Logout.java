package com.ebay.users;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;


public class Logout extends ActionSupport implements ServletRequestAware,ServletResponseAware{
	public String getUrl() {
		if(url == null|| url.equals(""))
			return "/home";
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}


	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public HttpServletResponse getResponse() {
		return response;
	}

	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private HttpServletRequest request;
	private HttpServletResponse response;

	@Override
	public void setServletResponse(HttpServletResponse res) {
		this.response = res;
	}

	@Override
	public void setServletRequest(HttpServletRequest req) {
		this.request = req;
		
	}

	private String url;

	public String execute()
	{
		Map<String,Object> session = ActionContext.getContext().getSession();
		session.clear();
		url = "/"+ActionContext.getContext().getName()+"?"+request.getQueryString();
		System.out.println("Sign in URL : "+url);
		return SUCCESS;
	}
}
