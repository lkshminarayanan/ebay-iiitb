package com.ebay.paisapay;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.ebay.util.*;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class register extends ActionSupport {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String bank_name, Payee, City, Bank_acc_num, branch;
	ResultSet rs1;
	public ArrayList bk;
	public bank b;
	Map mapping = new HashMap();
	Map session;
	
	
	
	
	
	public String load_page() {
//		session = ActionContext.getContext().getSession();
//		String ebay_id = (String) session.get("user");
//		if(ebay_id==null){
//			return "login";
//		}
		DBconn db1, db2;
		System.out.println("bank");
		db1 = new DBconn();
		db2 = new DBconn();
		System.out.println("in Register.load_page");
		Connection conn = null, conn1 = null;
		bk = new ArrayList();
		try {

			// Statement statement = null;
			rs1 = null;

			conn = DBconn.getConnection();
			conn1 = DBconn.getConnection();
			Statement stmt = conn.createStatement();
			Statement stmt1 = conn1.createStatement();
			String QueryString = "select * from bank_name";
			rs1 = stmt.executeQuery(QueryString);
			while (rs1.next()) {
				// System.out.println("enterd bank ka resultset");
				b = new bank();
				b.bank_name = rs1.getString("bank_name");
	//			System.out.println("Bank: "+ b.bank_name);
				ResultSet rs2 = stmt1
						.executeQuery("select * from bank_branch where Bank_id='"
								+ rs1.getString(1) + "'");
	//			System.out.println("HA HA	");
				ArrayList<String> options = new ArrayList<String>();
				while (rs2.next()) {
					options.add(rs2.getString("branch_name"));
				}
				mapping.put(rs1.getString("bank_name"), options);

	//			System.out.println("shikha");
				bk.add(b);

				

				//return "success";

			}
			// return "success";

		} catch (Exception e) {
			e.printStackTrace();
		}

		DBconn.close(conn);
		DBconn.close(conn1);
		return SUCCESS;
	}
	
	
	public String execute() {
		String ebay_id;
		session = ActionContext.getContext().getSession();
		ebay_id= (String) session.get("ebay_id");

	
		DBconn db1, db2;
		System.out.println("bank");
		db1 = new DBconn();
		db2 = new DBconn();
		System.out.println("bank");
		Connection conn = null, conn1 = null;

		int rs = 0;
		try {

			bank_name = this.getBank_name();
			Payee = this.getPayee();
			Bank_acc_num = this.getBank_acc_num();
			City = this.getCity();

			Connection connection = null;
			// Statement statement = null;

			connection = DBconn.getConnection();
			Statement stmt = connection.createStatement();
			String QueryString = "insert into paisapayreg(payee_name,bank_acc_num,bank_name,branch_name,city,ebay_id) values('"
					+ Payee
					+ "','"
					+ Bank_acc_num
					+ "','"
					+ bank_name
					+ "','"
					+ branch
					+ "','"
					+ City 
					+ "','"
					+ ebay_id +"')";
			rs = stmt.executeUpdate(QueryString);
			System.out.println(Payee + Bank_acc_num + bank_name + City);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (rs != 0) {
			return "success";
		} else
			return "error";
	}

	
	
	
	
	
	
	
	
	
	public String getBank_name() {
		return bank_name;
	}

	public void setBank_name(String bank_name) {
		this.bank_name = bank_name;
	}

	public String getPayee() {
		return Payee;
	}

	public void setPayee(String payee) {
		Payee = payee;
	}

	public String getCity() {
		return City;
	}

	public void setCity(String city) {
		City = city;
	}

	public String getBank_acc_num() {
		return Bank_acc_num;
	}

	public void setBank_acc_num(String bank_acc_num) {
		Bank_acc_num = bank_acc_num;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public Map getMapping() {
		return mapping;
	}

	public void setMapping(Map mapping) {
		this.mapping = mapping;
	}

}