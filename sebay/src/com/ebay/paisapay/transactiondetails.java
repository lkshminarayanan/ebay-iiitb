package com.ebay.paisapay;

import java.sql.*;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ebay.admin.HomeAct;
import com.ebay.util.DBconn;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class transactiondetails extends ActionSupport {
	private String title;
	private String shippingmethod;
	private String quantity;
	private Float total;
	private String currentstatus;
	private Float allamountpaid = (float) 0.0, buyamountpaid = (float) 0.0,
			soldamountpaid = (float) 0.0;

	private String buytitle;
	private String buyshippingmethod;
	private String buyquantity;
	private Float buytotal;
	private String buycurrentstatus;

	private String customername, bankname, branchname, accounttype, balance;
	public String value;
	String soldtitle;
	private int id;
	private int bank_third_party_id;
	private HttpServletRequest request;
	private HttpServletResponse response;

	String soldshippingmethod;
	String soldquantity;
	Float soldtotal;

	String soldcurrentstatus;

	private String payee_name, bank_acc_num, bank_name, branch, city;
	Map session;
	public ArrayList<transactiondetailsmodel> alllist, buylist, soldlist;
	Connection conn = null;
	Statement stmt = null;
	ResultSet rs = null;
	
	public String statusupdate(){
		String sql;
		if(id==1){
			sql="UPDATE `ebay`.`bank_third_party` SET `status`=2 WHERE `bank_third_party_id`='"+bank_third_party_id+"'";			
			conn = DBconn.getConnection();
			try {
				stmt = conn.createStatement();
				int result = stmt.executeUpdate(sql);
				if(result==1)
					System.out.println("Update Successfully for bank_third_party_id : " + bank_third_party_id);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			DBconn.close(conn);
		
		}
		
			
		return SUCCESS;
	}
	
	
	
	
	
	public void registrationdetails() {
		String ebayid;
		session = ActionContext.getContext().getSession();
		ebayid = (String) session.get("ebay_id");
		Connection connx = DBconn.getConnection();
		System.out.println("Line 112 of registrationdetail: session : "
				+ ebayid);
		String url = "select * from paisapayreg where ebay_id='" + ebayid + "'";
		try {
			Statement statement = connx.createStatement();
			rs = statement.executeQuery(url);
			if (rs.next()) {
				setPayee_name(rs.getString("payee_name"));
				setBank_acc_num(rs.getString("bank_acc_num"));
				System.out.println("bank account no" + bank_acc_num);
				setBank_name(rs.getString("bank_name"));
				setBranch(rs.getString("branch_name"));
				setCity(rs.getString("city"));
			}

		} catch (SQLException e) {
			System.out.println("error in paisa pay " + e.getMessage());
			e.printStackTrace();
			// TODO Auto-generated catch block

		}

		DBconn.close(connx);

	}

	/*
	 * void allList() { String category_name; String subcategory_name; String
	 * product_id; String table_name;
	 * 
	 * session=ActionContext.getContext().getSession();
	 * 
	 * String myebay_id = (String) session.get("ebay_id"); conn =
	 * DBconn.getConnection(); try { stmt = conn.createStatement();
	 * 
	 * String sql =
	 * "Select C.category_name, S.subcategory_name, B.product_id, B.buyer_id, B.seller_id, B.quantity, B.amount, B.status From bank_third_party B, category C, subcategory S WHERE B.category_id=C.category_id and B.category_id=  S.category_id and B.subcategory_id=S.subcategory_id and (B.buyer_id='"
	 * +myebay_id+"' or B.seller_id='"+myebay_id+"')"; rs =
	 * stmt.executeQuery(sql); while (rs.next()) { transactiondetailsmodel m=
	 * new transactiondetailsmodel(); category_name=rs.getString(1);
	 * subcategory_name=rs.getString(2);
	 * table_name=category_name+subcategory_name; product_id=rs.getString(3);
	 * m.setAllquantity(rs.getString(6)); m.setAlltotal(rs.getFloat(7)); int
	 * status= rs.getInt(8); if(status==0)
	 * m.setAllcurrentstatus("Payment Pending"); else if(status==1)
	 * m.setAllcurrentstatus("Payment Done"); else if(status==2)
	 * m.setAllcurrentstatus("Transfer Pending"); else if(status==3)
	 * m.setAllcurrentstatus("Transfer Done"); String sql_title =
	 * "SELECT P.title FROM " + table_name + " P WHERE P.product_id='"+
	 * product_id +"'"; Connection con_title= DBconn.getConnection(); Statement
	 * stmt_title= con_title.createStatement(); ResultSet rs_title =
	 * stmt_title.executeQuery(sql_title); if(rs_title.next()){
	 * m.setAlltitle(rs_title.getString(1)); } DBconn.close(con_title);
	 * allamountpaid=allamountpaid+m.getAlltotal();
	 * System.out.println("Title All\t: "+ m.getAlltitle());
	 * System.out.println("Quantity All\t: " + m.getAllquantity());
	 * System.out.println("Total All\t: " + m.getAlltotal());
	 * System.out.println("All Current Status\t: " + m.getAllcurrentstatus());
	 * System.out.println("***********************************************");
	 * alllist.add(m); } } catch (Exception e) { System.err
	 * .println("Exception : In TransactionDetail.execute.allList()");
	 * e.printStackTrace(); } DBconn.close(conn);
	 * 
	 * }
	 */
	void buyList() {
		String category_name;
		String subcategory_name;
		String product_id;
		String table_name;

		session = ActionContext.getContext().getSession();
		String myebay_id = (String) session.get("ebay_id");
		conn = DBconn.getConnection();
		try {
			stmt = conn.createStatement();
			
			String sql = "Select C.category_name, S.subcategory_name, B.product_id, B.buyer_id, B.seller_id, B.quantity, B.amount, B.status, B.bank_third_party_id From bank_third_party B, category C, subcategory S WHERE B.category_id=C.category_id and B.category_id=  S.category_id and B.subcategory_id=S.subcategory_id and B.buyer_id='"
					+ myebay_id + "'";
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				session.put("valuebuy", "check");
				
				transactiondetailsmodel mn = new transactiondetailsmodel();
				category_name = rs.getString(1);
				subcategory_name = rs.getString(2);
				String cn = category_name.toLowerCase();
				if (cn.equals("book") || cn.equals("books")) {
					table_name = "bookdetails";
				} else {
					table_name = category_name + subcategory_name;
				}
				product_id = rs.getString(3);
				mn.setBuyquantity(rs.getString(6));
				mn.setBuytotal(rs.getFloat(7));
				mn.setBuystatus(rs.getInt(8));
				mn.setBank_third_party_id(rs.getInt(9));

				if (mn.getBuystatus() == 0)
					mn.setBuycurrentstatus("Invalid");
				else if (mn.getBuystatus() == 1)
					mn.setBuycurrentstatus("Click If Item Recieved");
				else if (mn.getBuystatus() == 2)
					mn.setBuycurrentstatus("Item Recieved");


				String sql_title = "SELECT P.title FROM " + table_name
						+ " P WHERE P.product_id='" + product_id + "'";
				Connection con_title = DBconn.getConnection();
				Statement stmt_title = con_title.createStatement();
				ResultSet rs_title = stmt_title.executeQuery(sql_title);
				if (rs_title.next()) {
					mn.setBuytitle(rs_title.getString(1));
				}
				DBconn.close(con_title);
				buyamountpaid = buyamountpaid + mn.getBuytotal();
				// System.out.println("Title Buy\t: "+ mn.getBuytitle());
				// System.out.println("Quantity Buy\t: " + mn.getBuyquantity());
				// System.out.println("Total Buy\t: " + mn.getBuytotal());
				// System.out.println("Current Status Buy\t: " +
				// mn.getBuycurrentstatus());
				// System.out.println("******************Buy*****************************");
				buylist.add(mn);
			}
		} catch (Exception e) {
			System.err
					.println("Exception : In TransactionDetail.execute.BuyList()");
			e.printStackTrace();
		}
		DBconn.close(conn);

	}

	void soldList() {
		System.out.println("in soldList of transaction details");
		String category_name;
		String subcategory_name;
		String product_id;
		String table_name;

		session = ActionContext.getContext().getSession();
		String myebay_id = (String) session.get("ebay_id");
		conn = DBconn.getConnection();
		try {
			stmt = conn.createStatement();
			
			String sql = "Select C.category_name, S.subcategory_name, B.product_id, B.buyer_id, B.seller_id, B.quantity, B.amount, B.status, B.bank_third_party_id From bank_third_party B, category C, subcategory S WHERE B.category_id=C.category_id and B.category_id=  S.category_id and B.subcategory_id=S.subcategory_id and B.seller_id='"
					+ myebay_id + "'";
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				session.put("valuesold", "check");
				transactiondetailsmodel m = new transactiondetailsmodel();
				category_name = rs.getString(1);
				subcategory_name = rs.getString(2);
				String cn = category_name.toLowerCase();
				if (cn.equals("book") || cn.equals("books")) {
					table_name = "bookdetails";
				} else {
					table_name = category_name + subcategory_name;
				}
				product_id = rs.getString(3);
				m.setSoldquantity(rs.getString(6));
				m.setSoldtotal(rs.getFloat(7));
				m.setSoldstatus(rs.getInt(8));
				m.setBank_third_party_id(rs.getInt(9));
				if (m.getSoldstatus() == 0)
					m.setSoldcurrentstatus("Invalid");
				else if (m.getSoldstatus() == 1)
					m.setSoldcurrentstatus("Ship the item to the Buyer.");
				else if (m.getSoldstatus() == 2)
					m.setSoldcurrentstatus("Amount Transfer Done.");
				
				String sql_title = "SELECT P.title FROM " + table_name
						+ " P WHERE P.product_id='" + product_id + "'";
				Connection con_title = DBconn.getConnection();
				Statement stmt_title = con_title.createStatement();
				ResultSet rs_title = stmt_title.executeQuery(sql_title);
				if (rs_title.next()) {
					m.setSoldtitle(rs_title.getString(1));
				}
				DBconn.close(con_title);
				soldamountpaid = soldamountpaid + m.getSoldtotal();
				// System.out.println("Title sold\t: "+ m.getSoldtitle());
				// System.out.println("Quantity sold\t: " +
				// m.getSoldquantity());
				// System.out.println("Total sold\t: " + m.getSoldtotal());
				// System.out.println("Current Status\t: " +
				// m.getSoldcurrentstatus());
				// System.out.println("***********************************************");
				soldlist.add(m);
			}
		} catch (Exception e) {
			System.err
					.println("Exception : In TransactionDetail.execute.SoldList()");
			e.printStackTrace();
		}
		DBconn.close(conn);

	}

	public void getBankDetails() {
		System.out.println("In getBankDetails of TransactionDetails.java");
		Connection con = DBconn.getConnection();
		try {
			Statement stmt = con.createStatement();
			ResultSet rs = stmt
					.executeQuery("Select * FROM bank_db where bank_acc_num = '"
							+ bank_acc_num + "'");
			if (rs.next()) {
				accounttype = rs.getString("bank_acc_type");
				customername = rs.getString("cust_name");
				bankname = rs.getString("bank_name");
				branchname = rs.getString("branch_name");
				balance = rs.getString("balance");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		DBconn.close(con);

	}

	@Override
	public String execute() throws Exception {

		Map<String, Object> session = ActionContext.getContext().getSession();
		session = ActionContext.getContext().getSession();

		// if(session == null || session.get("loggedin") == null ||
		// session.get("user") == null || !((String)
		// session.get("loggedin")).equals("true") ){
		// String url =
		// "/"+ActionContext.getContext().getName()+"?"+request.getQueryString();
		// System.out.println("URL : "+url);
		// return LOGIN;
		// }
		System.out.println("in execute of transaction details");
		alllist = new ArrayList<transactiondetailsmodel>();
		soldlist = new ArrayList<transactiondetailsmodel>();
		buylist = new ArrayList<transactiondetailsmodel>();
		// new HomeAct().show();
		registrationdetails();
		// allList();
		buyList();
		soldList();
		getBankDetails();
		// for(transactiondetailsmodel o: buylist){
		// System.out.println(o.getAlltitle());
		// }
		return SUCCESS;
	}

	public String getBuytitle() {
		return buytitle;
	}

	public void setBuytitle(String buytitle) {
		this.buytitle = buytitle;
	}

	public String getBuyshippingmethod() {
		return buyshippingmethod;
	}

	public void setBuyshippingmethod(String buyshippingmethod) {
		this.buyshippingmethod = buyshippingmethod;
	}

	public String getBuyquantity() {
		return buyquantity;
	}

	public void setBuyquantity(String buyquantity) {
		this.buyquantity = buyquantity;
	}

	public Float getBuytotal() {
		return buytotal;
	}

	public void setBuytotal(Float buytotal) {
		this.buytotal = buytotal;
	}

	public String getBuycurrentstatus() {
		return buycurrentstatus;
	}

	public void setBuycurrentstatus(String buycurrentstatus) {
		this.buycurrentstatus = buycurrentstatus;
	}

	public String getSoldtitle() {
		return soldtitle;
	}

	public void setSoldtitle(String soldtitle) {
		this.soldtitle = soldtitle;
	}

	public String getSoldshippingmethod() {
		return soldshippingmethod;
	}

	public void setSoldshippingmethod(String soldshippingmethod) {
		this.soldshippingmethod = soldshippingmethod;
	}

	public String getSoldquantity() {
		return soldquantity;
	}

	public void setSoldquantity(String soldquantity) {
		this.soldquantity = soldquantity;
	}

	public Float getSoldtotal() {
		return soldtotal;
	}

	public void setSoldtotal(Float soldtotal) {
		this.soldtotal = soldtotal;
	}

	public String getSoldcurrentstatus() {
		return soldcurrentstatus;
	}

	public void setSoldcurrentstatus(String soldcurrentstatus) {
		this.soldcurrentstatus = soldcurrentstatus;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public Float getTotal() {
		return total;
	}

	public void setTotal(Float total) {
		this.total = total;
	}

	public String getCurrentstatus() {
		return currentstatus;
	}

	public void setCurrentstatus(String currentstatus) {
		this.currentstatus = currentstatus;
	}

	public Float getAllamountpaid() {
		return allamountpaid;
	}

	public void setAllamountpaid(Float allamountpaid) {
		this.allamountpaid = allamountpaid;
	}

	public Float getBuyamountpaid() {
		return buyamountpaid;
	}

	public void setBuyamountpaid(Float buyamountpaid) {
		this.buyamountpaid = buyamountpaid;
	}

	public Float getSoldamountpaid() {
		return soldamountpaid;
	}

	public void setSoldamountpaid(Float soldamountpaid) {
		this.soldamountpaid = soldamountpaid;
	}

	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public HttpServletResponse getResponse() {
		return response;
	}

	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}

	public Map getSession() {
		return session;
	}

	public void setSession(Map session) {
		this.session = session;
	}

	public String getCustomername() {
		return customername;
	}

	public void setCustomername(String customername) {
		this.customername = customername;
	}

	public String getBankname() {
		return bankname;
	}

	public void setBankname(String bankname) {
		this.bankname = bankname;
	}

	public String getBranchname() {
		return branchname;
	}

	public void setBranchname(String branchname) {
		this.branchname = branchname;
	}

	public String getAccounttype() {
		return accounttype;
	}

	public void setAccounttype(String accounttype) {
		this.accounttype = accounttype;
	}

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	public String getPayee_name() {
		return payee_name;
	}

	public void setPayee_name(String payee_name) {
		this.payee_name = payee_name;
	}

	public String getBank_acc_num() {
		return bank_acc_num;
	}

	public void setBank_acc_num(String bank_acc_num) {
		this.bank_acc_num = bank_acc_num;
	}

	public String getBank_name() {
		return bank_name;
	}

	public void setBank_name(String bank_name) {
		this.bank_name = bank_name;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}





	public int getId() {
		return id;
	}





	public void setId(int id) {
		this.id = id;
	}





	public int getBank_third_party_id() {
		return bank_third_party_id;
	}





	public void setBank_third_party_id(int bank_third_party_id) {
		this.bank_third_party_id = bank_third_party_id;
	}

}
