package com.ebay.paisapay;

public class transactiondetailsmodel {
//	String category_id;
//	String subcategory_id;
//	String product_id;
	private String alltitle;
	private String allshippingmethod;
	private String allquantity;
	private Float alltotal,allamountpaid;
	private String allcurrentstatus;
	private int allstatus;
	
	private String buytitle;
	private String buyshippingmethod;
	private String buyquantity;
	private Float buytotal,buyamountpaid;
	private String buycurrentstatus;
	private int buystatus;
	
	String soldtitle;
	String soldshippingmethod;
	String soldquantity;
	Float soldtotal,amountpaid;
	String soldcurrentstatus;
	private int soldstatus;
	
	private int bank_third_party_id;
	public int getBank_third_party_id() {
		return bank_third_party_id;
	}
	public void setBank_third_party_id(int bank_third_party_id) {
		this.bank_third_party_id = bank_third_party_id;
	}
	public String getAlltitle() {
		return alltitle;
	}
	public void setAlltitle(String alltitle) {
		this.alltitle = alltitle;
	}
	public String getAllshippingmethod() {
		return allshippingmethod;
	}
	public void setAllshippingmethod(String allshippingmethod) {
		this.allshippingmethod = allshippingmethod;
	}
	public String getAllquantity() {
		return allquantity;
	}
	public void setAllquantity(String allquantity) {
		this.allquantity = allquantity;
	}

	public String getAllcurrentstatus() {
		return allcurrentstatus;
	}
	public void setAllcurrentstatus(String allcurrentstatus) {
		this.allcurrentstatus = allcurrentstatus;
	}
	public void setAlltotal(Float alltotal) {
		this.alltotal = alltotal;
	}
	public String getBuytitle() {
		return buytitle;
	}
	public void setBuytitle(String buytitle) {
		this.buytitle = buytitle;
	}
	public String getBuyshippingmethod() {
		return buyshippingmethod;
	}
	public void setBuyshippingmethod(String buyshippingmethod) {
		this.buyshippingmethod = buyshippingmethod;
	}
	public String getBuyquantity() {
		return buyquantity;
	}
	public void setBuyquantity(String buyquantity) {
		this.buyquantity = buyquantity;
	}
	public Float getBuytotal() {
		return buytotal;
	}
	public void setBuytotal(Float buytotal) {
		this.buytotal = buytotal;
	}
	public String getBuycurrentstatus() {
		return buycurrentstatus;
	}
	public void setBuycurrentstatus(String buycurrentstatus) {
		this.buycurrentstatus = buycurrentstatus;
	}
	public String getSoldtitle() {
		return soldtitle;
	}
	public void setSoldtitle(String soldtitle) {
		this.soldtitle = soldtitle;
	}
	public String getSoldshippingmethod() {
		return soldshippingmethod;
	}
	public void setSoldshippingmethod(String soldshippingmethod) {
		this.soldshippingmethod = soldshippingmethod;
	}
	public String getSoldquantity() {
		return soldquantity;
	}
	public void setSoldquantity(String soldquantity) {
		this.soldquantity = soldquantity;
	}
	public Float getSoldtotal() {
		return soldtotal;
	}
	public void setSoldtotal(Float soldtotal) {
		this.soldtotal = soldtotal;
	}
	public String getSoldcurrentstatus() {
		return soldcurrentstatus;
	}
	public void setSoldcurrentstatus(String soldcurrentstatus) {
		this.soldcurrentstatus = soldcurrentstatus;
	}
	public Float getAlltotal() {
		return alltotal;
	}
	public Float getAllamountpaid() {
		return allamountpaid;
	}
	public void setAllamountpaid(Float allamountpaid) {
		this.allamountpaid = allamountpaid;
	}
	public int getAllstatus() {
		return allstatus;
	}
	public void setAllstatus(int allstatus) {
		this.allstatus = allstatus;
	}
	public Float getBuyamountpaid() {
		return buyamountpaid;
	}
	public void setBuyamountpaid(Float buyamountpaid) {
		this.buyamountpaid = buyamountpaid;
	}
	public int getBuystatus() {
		return buystatus;
	}
	public void setBuystatus(int buystatus) {
		this.buystatus = buystatus;
	}
	public int getSoldstatus() {
		return soldstatus;
	}
	public void setSoldstatus(int soldstatus) {
		this.soldstatus = soldstatus;
	}
}
