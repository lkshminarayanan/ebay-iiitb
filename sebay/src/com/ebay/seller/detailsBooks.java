package com.ebay.seller;

import com.ebay.util.DBconn;
import com.opensymphony.*;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;
import java.io.File;
import java.sql.*;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.apache.struts2.interceptor.ServletRequestAware;

public class detailsBooks extends ActionSupport implements ServletRequestAware {

	private int category_id, subcategory_id, product_id, biddingstatus = 0,
			quantity, duration, year, status, noofbidders, temp = 1, pages;

	private float price, currprice,shipcost,weight;
	

	private String title, description, subject, image, cond, spclatt, paisaPay,
			currDate, bidDate, expdate, author, language, format, publisher,
			filePath;

	private String userID;

	Date date;
	DBconn db;
	Connection con, con1;
	String query, query2;
	ResultSet rs;
	Statement st, st1;
	bidDetails bd;
	String a = null;
	Map session;
	public File userImage;
	public String userImageContentType, tmp;
	public String userImageFileName;
	//File fileToCreate;
	private HttpServletRequest servletRequest;

	public String addBooks() {
		session = ActionContext.getContext().getSession();
		userID = (String) session.get("ebay_id");

		setCategory_id((Integer) session.get("category_id"));
		setSubcategory_id((Integer) session.get("subcategory_id"));
		setUserID(userID);
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		Calendar cal1 = Calendar.getInstance();
		cal1.add(Calendar.DATE, 20);
		expdate = dateFormat.format(cal1.getTime());
		System.out.println("Today : " + cal.getTime());
		// add days from the calendar
		try // for storing it dynamically!!
		{
			// String filePath =
			// servletRequest.getSession().getServletContext().getPath("/images");
			filePath = "D:\\installation\\eclipsworkspace\\sebay\\WebContent\\images";
			System.out.println("Server path:" + filePath);
			tmp = "s" + getCategory_id() * 1000 + getSubcategory_id()
					+ getProduct_id() + ".jpg";
			System.out.println("tmp is" + tmp);
			File fileToCreate = new File(filePath, tmp);
			System.out.println("image is" + userImageFileName);
			FileUtils.copyFile(this.userImage, fileToCreate);
			tmp = "images/" + tmp;
			System.out.println("imgid" + tmp + "cat id" + getCategory_id());
		}

		catch (Exception e) {
			e.printStackTrace();
			System.out.println("error is" + e.getMessage());

			return INPUT;
		}
		if (getDuration() > 0)
			setBiddingstatus(1);
		else
			setBiddingstatus(0);
		
		query = "insert into  bookDetails values('" + getCategory_id() + "','"
				+ getSubcategory_id() + "','" + getProduct_id() + "','"
				+ userID + "','" + getTitle() + "','" + getDescription()
				+ "','" + getPrice() + "','" + getCond() + "','" + getAuthor()
				+ "','" + getYear() + "','" + getPages() + "','"
				+ getPublisher() + "','" + getLanguage() + "','" + getFormat()
				+ "','" + getSpclatt() + "','" + tmp + "','"
				+ getBiddingstatus() + "','0 ','" + getQuantity() + "','"
				+ expdate + "','" + temp + "','"+getShipcost()+"','"+getWeight()+"')";

		String query3 = "insert into userpurchasesale values('" + userID
				+ "','" + getCategory_id() + "','" + getSubcategory_id()
				+ "','" + getProduct_id() + "','" + getTitle() + "','"
				+ getImage() + "','" + getPrice() + "','" + getDescription()
				+ "',0,1,0,'" + cal.getTime() + "',0)";

		if (getDuration() > 0) {
			setBiddingstatus(1);
			currDate = dateFormat.format(cal.getTime());
			setCurrDate(currDate);
			// System.out.println("string date is"+dt);
			cal.add(Calendar.DATE, getDuration());
			bidDate = dateFormat.format(cal.getTime());

			setBidDate(bidDate);

			con = db.getConnection();
			try {
				st1=con.createStatement();
				System.out.println("bidding status"+getBiddingstatus());
				st1.executeUpdate(query);
				rs = st1.executeQuery("select product_id from bookDetails where category_id='"
						+ getCategory_id()
						+ "' and subcategory_id='"
						+ getSubcategory_id()
						+ "' order by product_id desc limit 1");
				rs.next();
				product_id = rs.getInt("product_id");
				System.out.println("product id" + product_id);
				setProduct_id(product_id);
				query2 = "insert into  bidding values('" + getCategory_id()
						+ "','" + getSubcategory_id() + "','" + product_id
						+ "',' ','" + currDate + "','" + bidDate
						+ "','" + getPrice() + "','" + getStatus() + "','" + userID + " ','"
						+ getNoofbidders() + "')";

				st = con.createStatement();
				st.executeUpdate(query2);
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				System.out.println("error is" + e.getMessage());
				e.printStackTrace();
			}
		} else {
			setBiddingstatus(0);
			con1 = db.getConnection();
			try {
				st1 = con1.createStatement();
				st1.executeUpdate(query);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return "success";
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getNoofbidders() {
		return noofbidders;
	}

	public void setNoofbidders(int noofbidders) {
		this.noofbidders = noofbidders;
	}

	public int getCategory_id() {
		return category_id;
	}

	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}

	public int getSubcategory_id() {
		return subcategory_id;
	}

	public void setSubcategory_id(int subcategory_id) {
		this.subcategory_id = subcategory_id;
	}

	public int getProduct_id() {
		return product_id;
	}

	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public int getBiddingstatus() {
		return biddingstatus;
	}

	public float getWeight() {
		return weight;
	}

	public void setWeight(float weight) {
		this.weight = weight;
	}
	
	public float getShipcost() {
		return shipcost;
	}

	public void setShipcost(float shipcost) {
		this.shipcost = shipcost;
	}

	public void setBiddingstatus(int biddingstatus) {
		this.biddingstatus = biddingstatus;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public float getCurrprice() {
		return currprice;
	}

	public void setCurrprice(float currprice) {
		this.currprice = currprice;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getCond() {
		return cond;
	}

	public void setCond(String cond) {
		this.cond = cond;
	}

	public String getSpclatt() {
		return spclatt;
	}

	public void setSpclatt(String spclatt) {
		this.spclatt = spclatt;
	}

	public String getPaisaPay() {
		return paisaPay;
	}

	public void setPaisaPay(String paisaPay) {
		this.paisaPay = paisaPay;
	}

	public String getCurrDate() {
		return currDate;
	}

	public void setCurrDate(String currDate) {
		this.currDate = currDate;
	}

	public String getBidDate() {
		return bidDate;
	}

	public void setBidDate(String bidDate) {
		this.bidDate = bidDate;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public int getPages() {
		return pages;
	}

	public void setPages(int pages) {
		this.pages = pages;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public void setUserImageFileName(String userImageFileName) {
		this.userImageFileName = userImageFileName;
	}

	@Override
	public void setServletRequest(HttpServletRequest servletRequest) {
		this.servletRequest = servletRequest;

	}
}
