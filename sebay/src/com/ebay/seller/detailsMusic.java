package com.ebay.seller;

import com.ebay.util.DBconn;
import com.opensymphony.*;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FileUtils;
import org.apache.struts2.interceptor.ServletRequestAware;

public class detailsMusic extends ActionSupport  implements ServletRequestAware {

	private int category_id, subcategory_id, product_id, biddingstatus,
			bid_id, quantity, duration, temp=0,temp1=1, status, noofbidders;
	private float price, currprice,shipcost,weight;
	private String title, description, genre, image, cond, specialatt,
			paisaPay, currDate, bidDate;
	private String userID="shekhar";
	Date date;
	DBconn db;
	Connection con, con1;
	String query, query2,query3,tmp;
	ResultSet rs;
	Statement st, st1,st2;
	bidDetails bd;
	String a = null;
	Map session ;
	public File userImage;
    public String userImageContentType;
    public String userImageFileName;
 
    private HttpServletRequest servletRequest;
	public String addCassette() {
		System.out.println("cst");
		session = ActionContext.getContext().getSession();
		userID=(String) session.get("ebay_id");
		setCategory_id((Integer) session.get("category_id"));
		setSubcategory_id((Integer) session.get("subcategory_id"));
		setUserID(userID);
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		Calendar cal1 = Calendar.getInstance();
		cal1.add(Calendar.DATE, 20);
		String expdate = dateFormat.format(cal1.getTime());
		System.out.println("Today : " + cal.getTime());
		// add days from the calendar
		try	//for storing it dynamically!!
		{
			//String filePath = servletRequest.getSession().getServletContext().getPath("/images");
			String filePath="D:\\installation\\eclipsworkspace\\sebay\\WebContent\\images";
			System.out.println("Server path:" + filePath);
			tmp="s"+getCategory_id()*1000
			+ getSubcategory_id() + getProduct_id()+".jpg";
			System.out.println("tmp is"+tmp);
			File fileToCreate = new File(filePath,tmp);
			System.out.println("image is"+userImageFileName);
			FileUtils.copyFile(this.userImage, fileToCreate);
			tmp="images/"+tmp;
			System.out.println("imgid"+tmp+"cat id"+getCategory_id());
			
		} 
		
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("error is"+e.getMessage());

			return INPUT;
		}
		if (getDuration() > 0) 
			setBiddingstatus(1);
		else
			setBiddingstatus(0);
		
		query = "insert into  musiccassette values('" + getCategory_id()
				+ "','" + getSubcategory_id() + "','" + getProduct_id()
				+ "','"+userID+"','" + getTitle() + "','" + getDescription() + "','"
				+ getPrice() + "','" + getCond() + "','" + getGenre() + "','"
				+ tmp + "','" + getBiddingstatus() + "','"+temp+"','"
				+ getQuantity() + "','"+expdate+"','"+temp1+"','"+getShipcost()+"','"+getWeight()+"')";
        
		query3="insert into userpurchasesale values('"+userID+"','" + getCategory_id()
				+ "','" + getSubcategory_id() + "','" + getProduct_id()
				+ "','" + getTitle() + "','"
				+ getImage() + "','"+ getPrice() + "','" + getDescription() + "',0,1,0,'"+cal.getTime()+"',0)";
		// query2="insert into  bidding values('"+getCategory_id()+"','"+getSubcategory_id()+"','"+getProduct_id()+"',' ','"+currDate+"','"+bidDate+"','"+getPrice()+"')";
		
     System.out.println("duration "+getDuration());
		// query="insert into  musiccassette values('"+getCategory_id()+"','"+getSubcategory_id()+"','"+getProduct_id()+"',' ','"+getTitle()+"','"+getDescription()+"','"+getPrice()+"','"+getCond()+"','"+getGenre()+"','"+getImage()+"','"+getBiddingstatus()+"',' ','"+getQuantity()+"')";
		if (getDuration() > 0) {
			setBiddingstatus(1);
			currDate = dateFormat.format(cal.getTime());
			setCurrDate(currDate);
			// System.out.println("string date is"+dt);
			cal.add(Calendar.DATE, getDuration());
			bidDate = dateFormat.format(cal.getTime());

			setBidDate(bidDate);

			con = db.getConnection();
			try {
				st1=con.createStatement();
				Statement st5=con.createStatement();
				st5=con.createStatement();
				st5.executeUpdate(query);
				ResultSet rs = st1.executeQuery("select product_id from musiccassette where category_id='"
						+ getCategory_id()
						+ "' and subcategory_id='"
						+ getSubcategory_id()
						+ "' order by product_id desc limit 1");
				rs.next();
				product_id = rs.getInt("product_id");
				System.out.println("product id" + product_id);
				setProduct_id(product_id);
				query2 = "insert into  bidding values('" + getCategory_id()+ "','" + getSubcategory_id() + "','" + product_id+ "',' ','" + currDate + "','" + bidDate + "','"+ getPrice() + "','"+getStatus()+"','"+userID+" ','"+getNoofbidders()+"')";
				
				//st = con.createStatement();
				st1.executeUpdate(query2);
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				System.out.println("shekhar error is" + e.getMessage());
				e.printStackTrace();
			}
		}
		else{
			setBiddingstatus(0);
			con1 = db.getConnection();
			try {
				st1 = con1.createStatement();
				st1.executeUpdate(query);
				
				} catch (SQLException e) {
				// TODO Auto-generated catch block
				System.out.println("sekhu error is" + e.getMessage());
				e.printStackTrace();
			}
		}
		return "success";
	}

	
	public String addCD() {
		System.out.println("in cd");
		session = ActionContext.getContext().getSession();
		userID=(String) session.get("ebay_id");
		setCategory_id((Integer) session.get("category_id"));
		setSubcategory_id((Integer) session.get("subcategory_id"));
		setUserID(userID);
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		Calendar cal1 = Calendar.getInstance();
		cal1.add(Calendar.DATE, 20);
		String expdate = dateFormat.format(cal1.getTime());
		System.out.println("Today : " + cal.getTime());
		// add days from the calendar
		try	//for storing it dynamically!!
		{
			//String filePath = servletRequest.getSession().getServletContext().getPath("/images");
			String filePath="D:\\installation\\eclipsworkspace\\sebay\\WebContent\\images";
			System.out.println("Server path:" + filePath);
			tmp="s"+getCategory_id()*1000
			+ getSubcategory_id() + getProduct_id()+".jpg";
			System.out.println("tmp is"+tmp);
			File fileToCreate = new File(filePath,tmp);
			System.out.println("image is"+userImageFileName);
			FileUtils.copyFile(this.userImage, fileToCreate);
			tmp="images/"+tmp;
			System.out.println("imgid"+tmp+"cat id"+getCategory_id());
			
		} 
		
		catch (Exception e)
		{
			e.printStackTrace();
			System.out.println("error is"+e.getMessage());

			return INPUT;
		}
		if (getDuration() > 0)
			setBiddingstatus(1);
		else
			setBiddingstatus(0);
		query = "insert into  musiccd values('" + getCategory_id()
		+ "','" + getSubcategory_id() + "','" + getProduct_id()
		+ "','"+userID+"','" + getTitle() + "','" + getDescription() + "','"
		+ getPrice() + "','" + getCond() + "','" + getGenre() + "','"
		+ tmp + "','" + getBiddingstatus() + "','"+temp+"','"
		+ getQuantity() + "','"+expdate+"','"+temp1+"','"+getShipcost()+"','"+getWeight()+"')";
        
		query3="insert into userpurchasesale values('"+userID+"','" + getCategory_id()
				+ "','" + getSubcategory_id() + "','" + getProduct_id()
				+ "','" + getTitle() + "','"
				+ getImage() + "','"+ getPrice() + "','" + getDescription() + "',0,1,0,'"+cal.getTime()+"',0)";
		// query2="insert into  bidding values('"+getCategory_id()+"','"+getSubcategory_id()+"','"+getProduct_id()+"',' ','"+currDate+"','"+bidDate+"','"+getPrice()+"')";
		
     System.out.println("duration "+getDuration());
		// query="insert into  musiccassette values('"+getCategory_id()+"','"+getSubcategory_id()+"','"+getProduct_id()+"',' ','"+getTitle()+"','"+getDescription()+"','"+getPrice()+"','"+getCond()+"','"+getGenre()+"','"+getImage()+"','"+getBiddingstatus()+"',' ','"+getQuantity()+"')";
		if (getDuration() > 0) {
			setBiddingstatus(1);
			currDate = dateFormat.format(cal.getTime());
			setCurrDate(currDate);
			// System.out.println("string date is"+dt);
			cal.add(Calendar.DATE, getDuration());
			bidDate = dateFormat.format(cal.getTime());

			setBidDate(bidDate);
			con = db.getConnection();
			try {
				st1=con.createStatement();
				Statement st5=con.createStatement();
				st5=con.createStatement();
				st5.executeUpdate(query);
				rs = st1.executeQuery("select product_id from musiccd where category_id='"
						+ getCategory_id()
						+ "' and subcategory_id='"
						+ getSubcategory_id()
						+ "' order by product_id desc limit 1");
				if(rs.next())
				product_id = rs.getInt("product_id");
				System.out.println("product id" + product_id);
				setProduct_id(product_id);
				query2 = "insert into  bidding values('" + getCategory_id()+ "','" + getSubcategory_id() + "','" + product_id+ "',' ','" + currDate + "','" + bidDate + "','"+ getPrice() + "','"+getStatus()+"','"+userID+" ','"+getNoofbidders()+"')";
				
				//st = con.createStatement();
				st1.executeUpdate(query2);
				st1.executeUpdate(query2);
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				System.out.println("shekhar error is" + e.getMessage());
				e.printStackTrace();
			}
		}
		else{
			setBiddingstatus(0);
			con1 = db.getConnection();
			try {
				st1 = con1.createStatement();
				st1.executeUpdate(query);
				} catch (SQLException e) {
				// TODO Auto-generated catch block
				System.out.println("sekhu error is" + e.getMessage());
				e.printStackTrace();
			}
		}
		return "success";
	}

	

	public float getShipcost() {
		return shipcost;
	}


	public void setShipcost(float shipcost) {
		this.shipcost = shipcost;
	}


	public float getWeight() {
		return weight;
	}


	public void setWeight(float weight) {
		this.weight = weight;
	}


	public String getUserID() {
		return userID;
	}



	public void setUserID(String userID) {
		this.userID = userID;
	}



	public String getCurrDate() {
		return currDate;
	}

	public void setCurrDate(String currDate) {
		this.currDate = currDate;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getNoofbidders() {
		return noofbidders;
	}

	public void setNoofbidders(int noofbidders) {
		this.noofbidders = noofbidders;
	}

	public String getBidDate() {
		return bidDate;
	}

	public void setBidDate(String bidDate) {
		this.bidDate = bidDate;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public String getPaisaPay() {
		return paisaPay;
	}

	public void setPaisaPay(String paisaPay) {
		this.paisaPay = paisaPay;
	}

	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}

	public void setSubcategory_id(int subcategory_id) {
		this.subcategory_id = subcategory_id;
	}

	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}

	public void setBiddingstatus(int biddingstatus) {
		this.biddingstatus = biddingstatus;
	}

	public void setBid_id(int bid_id) {
		this.bid_id = bid_id;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public void setCond(String cond) {
		this.cond = cond;
	}

	public void setSpecialatt(String specialatt) {
		this.specialatt = specialatt;
	}

	public String getSpecialatt() {
		return specialatt;
	}

	public int getCategory_id() {
		session = ActionContext.getContext().getSession();
		category_id = (Integer) session.get("category_id");
		System.out.println("gettin catid" + category_id);
		return category_id;
	}

	public int getSubcategory_id() {
		session = ActionContext.getContext().getSession();
		subcategory_id = (Integer) session.get("subcategory_id");
		System.out.println("gettin sub catid" + subcategory_id);
		return subcategory_id;
	}

	public int getProduct_id() {
		return product_id;
	}

	public int getBiddingstatus() {
		return biddingstatus;
	}

	public int getBid_id() {
		return bid_id;
	}

	public int getQuantity() {
		return quantity;
	}

	public float getPrice() {
		return price;
	}

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public String getGenre() {
		return genre;
	}

	public String getImage() {
		return image;
	}

	public String getCond() {
		return cond;
	}

    public void setUserImageFileName(String userImageFileName) {
        this.userImageFileName = userImageFileName;
    }
 
    @Override
    public void setServletRequest(HttpServletRequest servletRequest) {
        this.servletRequest = servletRequest;
 
    }
}
