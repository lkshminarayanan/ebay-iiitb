-- Book Database

-- creating database e-bay
Create DataBase ebay;

use ebay;

-- creating category table
create table category (category_id int(11) auto_increment primary key, category_name varchar(20));

-- create subcategory
create table subcategory (subcategory_id int(11) auto_increment primary key, category_id int(11), subcategory_name varchar(20), Foreign key (category_id) references category(category_id));

-- create bidding table
create table bidding (category_id int(11) , subcategory_id int(11) , product_id int(11) ,buyer_id varchar(100),startingtime datetime ,endtime datetime,currprice float,status int,seller_id varchar(100),noofbidders int,flag int );

-- create book details table
CREATE TABLE IF NOT EXISTS `bookdetails` (
  `category_id` int(11) NOT NULL DEFAULT '0',
  `subcategory_id` int(11) NOT NULL DEFAULT '0',
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `seller_id` varchar(50) DEFAULT NULL,
  `title` varchar(400) DEFAULT NULL,
  `description` text,
  `price` float DEFAULT NULL,
  `cond` varchar(100) DEFAULT NULL,
  `author` varchar(400) DEFAULT NULL,
  `pubYear` int(10) DEFAULT NULL,
  `pages` int(11) NOT NULL,
  `publisher` varchar(100) NOT NULL,
  `lang` varchar(20) DEFAULT NULL,
  `format_type` varchar(400) DEFAULT NULL,
  `spclAtt` varchar(20) DEFAULT NULL,
  `image` varchar(400) DEFAULT NULL,
  `biddingstatus` int(11) DEFAULT NULL,
  `bidderNo` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `expdate` datetime NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`category_id`,`subcategory_id`),
  KEY `seller_id` (`seller_id`)
);
-- table for music
create table musiccassette (category_id int(11) , subcategory_id int(11) , product_id int(11) auto_increment ,seller_id varchar(20), title varchar(400), description text, price float, cond varchar(100),genre varchar(20),image varchar(400), biddingstatus int(10) , bidderNo int , quantity int(11),expdate datetime,status int,  primary key(product_id, category_id, subcategory_id));

create table musiccd (category_id int(11) , subcategory_id int(11) , product_id int(11) auto_increment ,seller_id varchar(20), title varchar(400), description text, price float, cond varchar(100),genre varchar(20),image varchar(400), biddingstatus int(10) , bidderNo int , quantity int(11),expdate datetime,status int,  primary key(product_id, category_id, subcategory_id));


-- mobile
create table mobilesmartphone (category_id int(11) , subcategory_id int(11) , product_id int(11) auto_increment ,seller_id varchar(20),  title varchar(400), description text, price float, cond varchar(100),brand varchar(20),mpnno varchar(20),os varchar(20),camera varchar(20),image varchar(400), biddingstatus int(10) ,quantity int(11),expdate datetime,status int,  primary key(product_id, category_id, subcategory_id));

create table mobileaccessories (category_id int(11) , subcategory_id int(11) , product_id int(11) auto_increment ,seller_id varchar(20),  title varchar(400), description text, price float, cond varchar(100),brand varchar(20),mpnno varchar(20),connectivity varchar(20),earpiece varchar(20),image varchar(400), biddingstatus int(10) ,quantity int(11),expdate datetime,status int,  primary key(product_id, category_id, subcategory_id));

create table mobileothers (category_id int(11) , subcategory_id int(11) , product_id int(11) auto_increment ,seller_id varchar(20),  title varchar(400), description text, price float, cond varchar(100),brand varchar(20),mpnno varchar(20),type varchar(20),camera varchar(20),simtype varchar(20),os varchar(20),network varchar(20),image varchar(400), biddingstatus int(10) ,quantity int(11),expdate datetime,status int, primary key(product_id, category_id, subcategory_id));

-- create review
create table review(review_id int(11) not null auto_increment primary key, category_id int(11) , subcategory_id int(11), productid int(11) , reviewmsg text,additional_details text, ratings float);

-- user
create table users(fname varchar(300), lname varchar(300),streetaddress varchar(500),zip int(11),city varchar(300),state varchar(50),country varchar(50),email varchar(100) ,phone_no varchar(10), ebay_id varchar(200) primary key ,password varchar(200),secret_question varchar(300),secret_answer varchar(300),dob varchar(50));

-- brands
create table brands(brand_id varchar(20), brand_name varchar(400));

-- bank
CREATE TABLE  `ebay`.`paisapayreg` (
  `payee_name` varchar(100) DEFAULT NULL,
  `bank_acc_num` varchar(100) DEFAULT NULL,
  `bank_name` varchar(100) DEFAULT NULL,
  `city` varchar(200) DEFAULT NULL,
  `ebay_id` varchar(45) NOT NULL,
  PRIMARY KEY (`ebay_id`));

CREATE TABLE  `ebay`.`bank_db` (
  `bank_db_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `bank_acc_num` varchar(100) NOT NULL,
  `bank_acc_type` varchar(100) NOT NULL,
  `bank_name` varchar(100) NOT NULL,
  `cust_name` varchar(100) NOT NULL,
  `branch_id` int(11) unsigned NOT NULL,
  `balance` float NOT NULL,
  PRIMARY KEY (`bank_db_id`));

CREATE TABLE  `ebay`.`bank_third_party` (
  `category_id` int(11) unsigned NOT NULL,
  `subcategory_id` int(11) unsigned NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  `buyer_id` varchar(45) NOT NULL,
  `seller_id` varchar(45) NOT NULL,
  `amount` float NOT NULL,
  `status` int(1) unsigned NOT NULL,
  `bank_third_party_id` int(10) unsigned NOT NULL AUTO_INCREMENT,`date` datetime,

  PRIMARY KEY (`bank_third_party_id`));

-- 
create table userpurchasesale(user_id int(11) ,category_id int(11) ,subcategory_id int(11) ,product_id int(11) ,title varchar(200) ,image varchar(500) ,price numeric(5,3) ,description text,biddingstatus int ,salestatus int,purchasestatus int,date date,soldstatus int);
create table cart(buyer_id varchar(20),seller_id varchar(20),product_id int,title varchar(20),quantitypurchased varchar(20),status int ,category_id int ,subcategory_id int ,price float, PRIMARY KEY (`product_id`,`category_id`,`subcategory_id`,`buyer_id`));
		

create table userbidding(user_id int(11),category_id int(11),subcategory_id int(11),product_id int(11),title text,image varchar(500),price numeric(5,3),won int);

