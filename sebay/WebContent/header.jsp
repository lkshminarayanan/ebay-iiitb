
    <%@ taglib prefix="s" uri="/struts-tags" %>
    <%@ taglib prefix="sx" uri="/struts-dojo-tags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
	
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<link rel="stylesheet" type="text/css" href="ddlevelsfiles/ddlevelsmenu-base.css" />
<link rel="stylesheet" type="text/css" href="ddlevelsfiles/ddlevelsmenu-topbar.css" />
<link rel="stylesheet" type="text/css" href="ddlevelsfiles/ddlevelsmenu-sidebar.css" />

<script type="text/javascript" src="ddlevelsfiles/ddlevelsmenu.js">
</script>


<style type="text/css">

.glossymenu, .glossymenu li ul{
list-style-type: none;
margin: 0;
padding: 0;
width: 185px; /*WIDTH OF MAIN MENU ITEMS*/
border: 1px solid #4D4D4D;
}

table
{
font-size:12px;
padding: 1px;
}

.glossymenu li{
position: relative;
border:solid;
border-color:#CFCFCF;
border-width: 1px;
}

.glossymenu li a{
background:#EDEDED ;
font: bold 12px Verdana, Helvetica, sans-serif;
color:black;
display: block;
width: auto;
padding: 5px 0;
padding-left: 10px;
text-decoration: none;
}

.glossymenu li ul{ /*SUB MENU STYLE*/
position: absolute;
width: auto; /*WIDTH OF SUB MENU ITEMS*/
left: 0;
top: 0;
display: none;
}

.glossymenu li ul li{
float: left;
}

.glossymenu li ul a{
width: 180px; /*WIDTH OF SUB MENU ITEMS - 10px padding-left for A elements */
}

.glossymenu .arrowdiv{
position: absolute;
right: 6px;
}

.glossymenu li a:visited, .glossymenu li a:active{
color: black;
}

.glossymenu li a:hover{
background: #FFFFFF;
}
.style1 {font-weight: bold;
color: #333333;

}
.recomd{
height:125;
width:15%;
border-width: 1px;
border-style: solid;
border-color: #999;
}
.style3 {color: #0000FF;
margin-top: 10;
}
/* Holly Hack for IE \*/
* html .glossymenu li { float: left; height: 1%; }
* html .glossymenu li a { height: 1%; }
/* End */
#Cat_bar {
	position:absolute;
	width:1000px;
	height:50px;
	z-index:-10;
	top: 134px;
}
.style9 {color: #0000FF; margin-top: 10; font-size: 16; }
</style>

<script type="text/javascript">
var menuids=new Array("verticalmenu") //Enter id(s) of UL menus, separated by commas
var submenuoffset=-2 //Offset of submenus from main menu. Default is -2 pixels.

function createcssmenu(){
for (var i=0; i<menuids.length; i++){
  var ultags=document.getElementById(menuids[i]).getElementsByTagName("ul");
    for (var t=0; t<ultags.length; t++){
    	var spanref=document.createElement("span");
		spanref.className="arrowdiv";
		spanref.innerHTML="&nbsp;&nbsp;";
		ultags[t].parentNode.getElementsByTagName("a")[0].appendChild(spanref);
    ultags[t].parentNode.onmouseover=function(){
    this.getElementsByTagName("ul")[0].style.left=this.parentNode.offsetWidth+submenuoffset+"px";
    this.getElementsByTagName("ul")[0].style.display="block";
    }
    ultags[t].parentNode.onmouseout=function(){
    this.getElementsByTagName("ul")[0].style.display="none";
    }
    }
  



if (window.addEventListener)
window.addEventListener("load", createcssmenu, false)
else if (window.attachEvent)
window.attachEvent("onload", createcssmenu)
</script>
<head>
<sj:head locale="de" jqueryui="true" defaultIndicator="myDefaultIndicator"/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Ebay India Home</title>



<!-- Start css3menu.com HEAD section -->
<link rel="stylesheet" href="Home_files/css3menu1/style.css" type="text/css" />
<!-- End css3menu.com HEAD section -->

<script>var pageHasRtmPlacements = true;</script>
<link rel="stylesheet" type="text/css"
	href="./header_files/wqgvhcoo1qyotigiejpkb5zv2.css">
<link rel="stylesheet" type="text/css"
	href="./header_files/lhniyzzerm4fferebck2ef3qb.css">
<style type="text/css">
#hpFeaturedItems {
	width: 615px
}

#hpFeaturedItems .titleBar {
	height: 30px;
	background-color: #fc0
}

#hpFeaturedItems .titleBarLeft {
	background: transparent
		url(http://q.ebaystatic.com/aw/pics/in/globalAssets/imgPanelULTrans.gif)
		no-repeat scroll top left;
	float: left;
	margin-left: 0;
	padding-left: 15px
}

#hpFeaturedItems .titleBarRight {
	background: transparent
		url(http://p.ebaystatic.com/aw/pics/in/globalAssets/imgPanelURTrans.gif)
		no-repeat scroll top right;
	margin-right: 0;
	padding-right: 15px
}

#hpFeaturedItems .titleText {
	color: #5d5d5d;
	font-size: medium;
	font-family: Arial;
	font-weight: Bold;
	padding-top: 5px
}

#hpFeaturedItems .contentContainerText {
	padding: 13px 15px 0 15px
}

* html #hpFeaturedItems .contentContainerText {
	width: 95%
}

#hpFeaturedItems .contentContainerImg {
	padding: 10px 15px 0 15px
}

* html #hpFeaturedItems .contentContainerImg {
	width: 95%
}

#hpFeaturedItems .textViewLeft {
	padding: 0;
	margin: 0;
	float: left;
	width: 284px;
	overflow: hidden
}

#hpFeaturedItems .textViewRight {
	padding: 0;
	margin-left: 15px;
	margin-top: 0;
	margin-bottom: 0;
	float: left;
	width: 284px;
	overflow: hidden
}

#hpFeaturedItems .textViewClear {
	clear: both;
	visibility: hidden;
	height: 1px
}

#hpFeaturedItems .textViewContent {
	color: #00f;
	font-size: small;
	font-family: Arial
}

#hpFeaturedItems .textViewList {
	margin-left: 15px;
	margin-top: 0;
	margin-bottom: 0;
	padding: 0;
	text-align: left
}

#hpFeaturedItems .bottomBar {
	width: 100%;
	text-align: right;
	height: 15px;
	background: #f2f2f2
		url(http://q.ebaystatic.com/aw/pics/globalAssets/imgPanelBGGreyGrad.gif)
		repeat-x scroll bottom;
	border-top: 1px solid #dedede
}

#hpFeaturedItems .imgViewItem {
	width: 112px;
	float: left;
	font-size: small;
	font-family: Arial;
	color: #00f;
	overflow: hidden
}

#hpFeaturedItems .imgViewItemSpace {
	padding-left: 5px
}

#hpFeaturedItems .imgContainer {
	height: 64px;
	width: 64px;
	border: 1px solid #666;
	margin: 0;
	padding: 0;
	text-align: center;
	vertical-align: middle;
	overflow: hidden
}

#hpFeaturedItems .imgViewItem a {
	text-decoration: none
}

#hpFeaturedItems .imgViewItem a:hover {
	text-decoration: underline
}

#hpFeaturedItems .imgViewItemTitle {
	padding-top: 5px
}
.smallLinks
{
font-size:12px;
padding: 1px;
font-family:Trebuchet,"Trebuchet MS";
}
</style>
<title>eBay India - Online Shopping Mall: Free Auctions,
	Shop/Buy/Sell Mobiles, Cameras, Apparel, Computers, Bollywood Clothes
	&amp; Indian Products</title>
<meta name="keywords"
	content="India online shopping, free online auctions, india apparels, accessories, men&#39;s apparel, women&#39;s apparel, mobiles, computers, cameras, Indian products, books, bollywood movies, music vcd/dvd, clothes, cars, motor bikes, nokia, sony ericsson, laptops, pda, indian jewellery, jewelry, stamps, travel tickets, toys. India online shopping mall.">
<meta name="description"
	content="eBay.in is India&#39;s most popular online shopping mall providing free online auctions for products like mobiles/cell phones, cameras, computers, consumer electronics, bollywood movies, music &amp; clothes, Indian art products, books, cars, motor bikes, Indian apparel/clothes, computers, jewellery, stamps, travel tickets &amp; a variety of goods &amp; products in India">
<meta name="google-site-verification"
	content="FhX5RgxqM_mmLAfBKHOUJAIShteGrrwq52kR5wmiLcg">
<meta name="y_key" content="9d67cbc37e38b26c">
<meta name="msvalidate.01" content="966859516F906915699AD6A9F6DF23B6">
<meta property="fb:app_id" content="102628213125203">
<noscript>&lt;meta http-equiv="refresh"
	content="0;url=http://www.ebay.in/?_js=OFF"&gt;</noscript>
<script> var pageName = 'HomePagePortal';
</script>
<script src="./header_files/rtm.js" type="text/javascript"
	id="xdr_ext_0" defer="defer" async=""></script>
</head>
<body id="body"">
	
	<div align="center">
<div>
<ul id="ddsubmenu3" class="ddsubmenustyle">
<s:iterator value="%{#session.catlist}">
<li><input type="hidden" value="<s:property value="category_id"/>"/>
		<a href="displayItemList?category_id=<s:property value="category_id"/>"><s:property value="category_name"/></a>
		</li>
</s:iterator>
</ul>
</div>

		<div class="ContentContainer" id="TopContainer">

			<div>
				<div class="pcontent">
					<!--cacheStatus: true-->

					<div id="gnheader" class="gh-w">
						<div id="cobrandHeader"></div>
						<a
							href="home"
							rel="nofollow"
							style="display: block; position: absolute; left: -9999px;">Skip
							to main content</a>
						<div>
							<!-- headerType=FULL:CCHP_PAGE-->
							<script type="text/javascript">var RoverDomainBaseUrl = 'http://rover.ebay.in';var svrGMT = 1333890785957;</script>
							<div></div>
							<div class="gh-eb">
								<div class="gh-emn">
									<div class="gh-hid"></div>
									<div class="gh-mn">
										<span class="gh-fst"><a id="MyEbay"
											href="myebayhome">My eBay</a>
										</span>
										<s:if test="%{#session.loggedin!='true'}">
										<a id="Sell" href="jsp_pages/account/SignIn.jsp">
										</s:if>
										<s:else>
										<a id="Sell" href="getsellpage">
										</s:else>
										Sell</a>
										<a class=" " id="Community" href="#">Community</a>
										<a id="Help" href="#">Customer Support</a>
										<span class="gh-nho"><span></span>
										</span>
									</div>
								</div>
							</div>
							<div class="gh-log">
								<span class="gh-lg"><a href="home"
									rel="nofollow"> <img src="./header_files/logoEbay_x45.gif"
										alt="eBay" border="0">
								</a>
								</span><span class="greeting gh-ui">
									<s:if test="%{#session.loggedin!='true'}">
									<!-- BEGIN: GREETING:SIGNEDOUT -->Welcome!&nbsp;
									
									<a href="jsp_pages/account/SignIn.jsp" class="style3">Sign In</a> or 
									<a href="jsp_pages/account/registration.jsp" class="style3"> Register</a>
									
									<!-- END: GREETING:SIGNEDOUT -->
									</s:if><s:else>
<s:property value="%{#session.fname}"/>&nbsp;<a href="logout" class="style3">Logout</a> 
</s:else>
									<span id="bta"></span>
								</span><span class="coupon"></span><span></span>
							</div>
							<div class="gh-rph">
								<span class="addllinks"><img
									src="./header_files/cart.gif" width="15" height="15"> 
									<a href="showCart">
									 My	Shopping Cart</a><span id="glb_cart"></span>
							
									| <a
									href="showpaisapaydetail">My
										PaisaPay</a> | <a href="loadadvertisement">Add Advertisement
										</a>
								</span>
							</div>
							<div class="gh-cl"></div>
							<div id="v4-header17acl"
								class="acrc-roundcont acrc-roundtopside acl">
								<div class="acrc-roundoutside">
									<div style="background-color: #ffffff;">
										<div id="v4-header0acl" class="sug">
											
										</div>
										<div style="word-break: normal" class="prd" id="prdDivWrp">
											<div class="pln">
												<span class="ptl">Popular products</span>
											</div>
											<div class="tbw">
												<table cellspacing="0" width="100%">
													<tbody>
														<tr id="v4-header11acl">
															<td></td>
															<td width="100%"></td>
														</tr>
														<tr id="v4-header12acl">
															<td></td>
															<td width="100%"></td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<div style="word-break: normal" id="tdDivWrp">
											<div class="pln"></div>
											<div id="tdDiv">
												<div id="v4-header13acl"></div>
												<div id="v4-header14acl"></div>
											</div>
										</div>
										<div id="v4-header15acl" class="nosug hide">No
											suggestions.</div>
										<div class="affooter">
											<a href="javascript:;" id="v4-header16acl" class="hlink">Hide
												eBay suggestions</a>
										</div>
									</div>
								</div>
								<div class="acrc-roundbottom">
									<div class="acrc-roundbottomright">
										<div class="acrc-roundbottomMiddle"></div>
									</div>
								</div>
							</div>
							
							<form method="get" action="displayItemList"
								name="headerSearch" id="headerSearch">
								
								<input type="hidden" name="filterItemsToDisplay" id="filterItemsToDisplay" value='<s:property default="0" value="filterItemsToDisplay"/>'>
								
								<div class="gh-sbox">
									<div class="gh-fl">
										<label for="_nkw" class="g-hdn">
										Enter your search keyword</label><input value='<s:property value="searchTerm"/>' autocomplete="OFF" name="searchTerm" 
											maxlength="300" size="60" class="gh-tb" type="text">
											<!--<input title="Show Suggestions" readonly="readonly" id="_nkw_acdiv"
											class="ac-ac_div">-->
									
										<label for="_sacat" class="g-hdn">Select a category	for search</label>
										<s:select name="category_id" headerKey="0" listKey="category_id"
							headerValue="All Categories" listValue="category_name" value="%{category_id}"
							list="%{#session.catlist}" class="gh-sb" />
											
											<a class="gh-ss">
											<input value="Search" id="search" name ="search" class="gh-btn" type="submit">
										</a>
									</div>
									<div class="gh-fl gh-as">
										<a id="AdvSearchId"
											href="advancesearch.jsp" rel="nofollow">Advanced search</a>
									</div>
									<div class="gh-clr"></div>
								</div>
							</form>
							<div class="gh-col">
								<b class="gh-c1"></b><b class="gh-c2"></b><b class="gh-c3"></b><b
									class="gh-c4"></b><b class="gh-c5"></b><b class="gh-c6"></b><b
									class="gh-c7"></b>
								<div class="gh-clr"></div>
							</div>
							<div id="headerWrapper" class="gh-hbw  ">
								<div class="gh-hb">
									<div class="gh-mn"  id="ddtopmenubar">
									
										<a href="#" rel="ddsubmenu3">CATEGORIES</a>
									
								<script type="text/javascript">
								ddlevelsmenu.setup("ddtopmenubar", "topbar") //ddlevelsmenu.setup("mainmenuid", "topbar|sidebar")
								</script>
										
									<a id="EbayStores" title="Find more of what you love."
											href="#">SHOPS</a><a id="v4-gnh_0"
											href="#">MOTORS</a><a id="v4-gnh_1"
											href="#">PHOTO CENTRE</a>
									</div>
								</div>
								<div class="gh-lbh1">
									<div class="gh-rtm">
										<div
											style="width: 160px; height: 22px; overflow: hidden; display: block;"
											id="rtm_html_876">
											<table bgcolor="#F5F5F5" cellpadding="0" cellspacing="0"
												height="22">
												<tbody>
													<tr align="center">
														<td valign="bottom"><a
															href="#"
															target="_blank"
															style="text-decoration: none; color: rgb(102, 102, 102); bottom: 3px; font: bold 0.923em Trebuchet MS; padding: 0px 5px;"
															onmouseover="this.style.textDecoration=&#39;underline&#39;;"
															onmouseout="this.style.textDecoration=&#39;none&#39;;">INSURANCE</a>
														</td>
														<td
															style="border-right: 1px solid rgb(204, 204, 204); border-left: 1px solid rgb(204, 204, 204);"><a
															href="#"
															target="_blank"><img
																src="./header_files/GEBHeader_15thSep.jpg"
																alt="Global EasyBuy" border="0" width="76" height="22">
														</a>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<div class="gh-lbh2">
									<div class="gh-rtm">
										<div
											style="width: 160px; height: 22px; overflow: hidden; display: block;"
											id="rtm_html_912">
											<table bgcolor="#F5F5F5" cellpadding="0" cellspacing="0"
												height="22">
												<tbody>
													<tr align="center">
														<td valign="bottom"><a
															href="#"
															target="_blank"
															style="text-decoration: none; color: rgb(102, 102, 102); position: relative; bottom: 3px; font: bold 0.923em Trebuchet MS; padding: 0px 9px 0px 0px;"
															onmouseover="this.style.textDecoration=&#39;underline&#39;;"
															onmouseout="this.style.textDecoration=&#39;none&#39;;">JOBS</a>
														</td>
														<td
															style="border-right: 1px solid rgb(204, 204, 204); border-left: 1px solid rgb(204, 204, 204);"
															valign="bottom"><a
															href="#"
															target="_blank"
															style="text-decoration: none; color: rgb(102, 102, 102); position: relative; bottom: 3px; font: bold 0.923em Trebuchet MS; padding: 0px 5px;"
															onmouseover="this.style.textDecoration=&#39;underline&#39;;"
															onmouseout="this.style.textDecoration=&#39;none&#39;;">MY
																CITY</a>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
								<div class="gh-lbh3">
									<div class="gh-rtm">
										<div
											style="width: 144px; height: 22px; overflow: hidden; display: block;"
											id="rtm_html_433">
											<img src="./header_files/FinaleBayguarantee.png"
												usemap="#FinaleBayguarantee" alt="" border="0">
											<map name="FinaleBayguarantee">
												<area shape="rect" coords="1,0,144,22" alt="eBay Guarantee*"
													href="#"
													target="_top">
											</map>
											<!-- 1315468617247 -->
										</div>
									</div>
								</div>
								<div class="gh-clr"></div>
							</div>
						</div>
					</div>

</body>
</html>