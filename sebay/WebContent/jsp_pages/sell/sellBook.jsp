<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<style type="text/css">
.l1{
height:100%;width:100%;

border:1px solid #999!important;
border-width: 1px;
border-style: solid;
border-color: #99CC33;
}
</style>
<script type="text/javascript">
function auction(){
	var xmlhttp;
	  xmlhttp=new XMLHttpRequest();
	xmlhttp.onreadystatechange=function()
	  {
	  if (xmlhttp.readyState==4 && xmlhttp.status==200)
	    {
	    document.getElementById("sellType").innerHTML=xmlhttp.responseText;
	    }
	  }
	xmlhttp.open("GET","jsp_pages/sell/auction.jsp",true);
	xmlhttp.send();
}
function fixed(){
	var xmlhttp;
	  xmlhttp=new XMLHttpRequest();
	xmlhttp.onreadystatechange=function()
	  {
	  if (xmlhttp.readyState==4 && xmlhttp.status==200)
	    {
	    document.getElementById("sellType").innerHTML=xmlhttp.responseText;
	    }
	  }
	xmlhttp.open("GET","jsp_pages/sell/fixedPrice.jsp",true);
	xmlhttp.send();
}
</script>
<body marginwidth="150px">
<%@ include file="../../header.jsp" %><br /><br /><br />
<br /><div class="l1">
<div style="background-color:#99CC33;height:50px">
<b>Create Your Listing</b>
</div>
<div align="center"><b>Category Selected</b>
<b><i><s:property value="#session['catnames']" /></i></b></div>
<s:form action="bookDetails" enctype="multipart/form-data">

<img src="images/title1.jpg" align="absmiddle" style="width=98%"/>
<br />
<input type="text" name="title" value="" style="width:80%;margin-left: 40px;height:40px" maxlength="80" required="required" /><br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b> (max length is 80 character)</b>
<br />
<br /><br /><br />
<div>
<img src="images/title2.jpg" align="absmiddle" style="width=98%"/><br /><br />
<input type="file" name="userImage" />
</div>
<br /><br />
<img src="images/title3.jpg" align="absmiddle" style="width=98%"/><br /><br />
<div style="margin-left: 50px">
<b>Add more information to help buyers find your item in search results. Buyers can use popular item specifics to refine their search and locate your item faster.</b>
<br /><br />

<b>Item's Condition</b><br />
<select id="itemCondition" name="cond">
<option value="-1">-</option>
<option value="new">New</option>
<option value="used">Used</option>
</select><br /><br />
<b>Subject</b><br />
<input type="text" name="subject" /><br />
<b>Format</b><br />
<select name="format">
<option value="hardcover">Hardcover</option>
<option value="paperback">PaperBack</option>
<option value="other">Other</option>
</select><br />
<b>Author</b><br />
<input type="text" name="author" /><br />
<b>Special Attribute</b><br />
<select name="spclAtt">
<option value="signed">Signed</option>
<option value="illustrated">Illustrated</option>
<option value="LargePrint">LargePrint</option>
</select><br />
<b>Publication Year</b><br />
<input type="number" name="year" min="1700" max="2020" /><br />
<b>Language</b><br />
<select name="language" >
<option value="english">English</option>
<option value="hindi">hindi</option>
<option value="bengali">Bengali</option>
<option value="Kannada">Kannada</option>
<option value="other">Other</option>
</select><br />

<b>Describe your item here..</b><br />
<input type="text" name="description" style="width:70%;height:100px"/>

</div>

<img src="images/title4.jpg" align="absmiddle" style="width=98%"/><br /><br />
<input type="button" value="AUCTION" onclick="auction()" />&nbsp;&nbsp;<input type="button" value="FIXED PRICE" onclick="fixed()" />
<div id="sellType"></div>
<br /><br />
<img src="images/title5.jpg" align="absmiddle" style="width=98%"/><br /><br />
<div >Accept payment with <b><i>Paisa Pay</i></b> &nbsp;&nbsp;<input type="text" name="paisaPay" value="" style="width:40%;height:30px"  /><br />

<b>Dont Have an account...</b><a href="/jsp_pages/sell/paisaPayReg.jsp">click here.</a>
</div>
<br /><br />
<s:submit value="Save Details" />
</s:form>
</div>


</body>
</html>