<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<style type="text/css">
.l1{
padding-top: 6px;padding-right: 0px;padding-bottom: 6px;padding-left: 4px;
margin-top:15px;
height:150px;
background-color:#FFFAF0;
border: 1px solid #999!important;
border-width: 1px;
border-style: solid;
border-color: #999;
color: #003DAD!important;
font:"Times New Roman", Times, serif;
}
</style>
<script type="text/javascript">
function getForm(){
	
	var xmlhttp;
	  xmlhttp=new XMLHttpRequest();
	xmlhttp.onreadystatechange=function()
	  {
	  if (xmlhttp.readyState==4 && xmlhttp.status==200)
	    {
	    document.getElementById("bankForm").innerHTML=xmlhttp.responseText;
	    }
	  }
	  
	xmlhttp.open("GET","jsp_pages/bank/bankForm.jsp",true);
	xmlhttp.send();
	}
	function credit()
	{
	 var xmlhttp;
	  xmlhttp=new XMLHttpRequest();
	xmlhttp.onreadystatechange=function()
	  {
	  if (xmlhttp.readyState==4 && xmlhttp.status==200)
	    {
	    document.getElementById("bank").innerHTML=xmlhttp.responseText;
	    }
	  }
	  
	xmlhttp.open("GET","getDebitBanks",true);
	xmlhttp.send();
}
function debit()
{
	
 var xmlhttp;
  xmlhttp=new XMLHttpRequest();
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("bank").innerHTML=xmlhttp.responseText;
    }
  }
  
xmlhttp.open("GET","getDebitBanks?accntType="+'debit',true);
xmlhttp.send();
}
function credit()
{
 var xmlhttp;
  xmlhttp=new XMLHttpRequest();
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("bank").innerHTML=xmlhttp.responseText;
    }
  }
  
xmlhttp.open("GET","getDebitBanks",true);
xmlhttp.send();
}
function credit()
{
	
 var xmlhttp;
  xmlhttp=new XMLHttpRequest();
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("bank").innerHTML=xmlhttp.responseText;
    }
  }
  
xmlhttp.open("GET","getDebitBanks?accntType="+'credit',true);
xmlhttp.send();
}

</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body marginwidth="100px">
<%@ include file="../../header.jsp" %><br/>
<br /><br />
<div class="l1" style="float:left;width:60%">
<b>Order Details</b>
<s:property value="%{#session.user.fname}"/>
</div>
<div class="l1" style="float:right;width:35%">
<b>Shipping address</b>
<table>
<tr><td><s:property value="%{#session.user.fname}"/>
<tr><td><s:property value="%{#session.user.streetaddress}"/>
<tr><td><s:property value="%{#session.user.city}"/>
<tr><td><s:property value="%{#session.user.state}"/>,
<td><s:property value="%{#session.user.pin}"/>
<tr><td>Mobile-
<td><s:property value="%{#session.user.phone_no}"/>
</table>
</div><br /><br />
<br />
<div class="l1" style="float:left;width:100%;height:30px">
<b>Select Your Payment Option</b>
</div>

<div class="l1" style="float:left;width:30%">
<b>Payment Mode</b>
<input type="button" value="DebitCard" onclick="debit()" style="width: 100%;height:45px"><br />
<input type="button" value="CreditCard" onclick="credit()" style="width: 100%;height:45px"><br />
<input type="button" value="BankTransfer" onclick="debit()" style="width: 100%;height:45px"><br />
</div>

<div class="l1" style="float:right;width:65%;background-color: #ffffff"">
<b>Select Bank</b>
<s:form action="pay">
<div id="bank" ></div>

</s:form>
</div><br /><br />
</body>
</html>