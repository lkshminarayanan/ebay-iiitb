<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="/struts-tags" prefix="s" %>
<%@ page  import="java.util.*,com.opensymphony.xwork2.*,java.util.Map"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css">

.glossymenu, .glossymenu li ul{
list-style-type: none;
margin: 0;
padding: 0;
width: 185px; /*WIDTH OF MAIN MENU ITEMS*/
border: 1px solid #4D4D4D;
}

table
{
font-size:12px;
padding: 1px;
}

.glossymenu li{
position: relative;
border:solid;
border-color:#CFCFCF;
border-width: 1px;
}

.glossymenu li a{
background:#EDEDED ;
font: bold 12px Verdana, Helvetica, sans-serif;
color:black;
display: block;
width: auto;
padding: 5px 0;
padding-left: 10px;
text-decoration: none;
}

.glossymenu li ul{ /*SUB MENU STYLE*/
position: absolute;
width: auto; /*WIDTH OF SUB MENU ITEMS*/
left: 0;
top: 0;
display: none;
}

.glossymenu li ul li{
float: left;
}

.glossymenu li ul a{
width: 180px; /*WIDTH OF SUB MENU ITEMS - 10px padding-left for A elements */
}

.glossymenu .arrowdiv{
position: absolute;
right: 6px;
}

.glossymenu li a:visited, .glossymenu li a:active{
color: black;
}

.glossymenu li a:hover{
background: #FFFFFF;
}

/* Holly Hack for IE \*/
* html .glossymenu li { float: left; height: 1%; }
* html .glossymenu li a { height: 1%; }
/* End */
</style>

<script type="text/javascript">
var menuids=new Array("verticalmenu") //Enter id(s) of UL menus, separated by commas
var submenuoffset=-2 //Offset of submenus from main menu. Default is -2 pixels.

function createcssmenu(){
for (var i=0; i<menuids.length; i++){
  var ultags=document.getElementById(menuids[i]).getElementsByTagName("ul")
    for (var t=0; t<ultags.length; t++){
    var spanref=document.createElement("span")
		spanref.className="arrowdiv"
		spanref.innerHTML="&nbsp;&nbsp;"
		ultags[t].parentNode.getElementsByTagName("a")[0].appendChild(spanref)
    ultags[t].parentNode.onmouseover=function(){
    this.getElementsByTagName("ul")[0].style.left=this.parentNode.offsetWidth+submenuoffset+"px"
    this.getElementsByTagName("ul")[0].style.display="block"
    }
    ultags[t].parentNode.onmouseout=function(){
    this.getElementsByTagName("ul")[0].style.display="none"
    }
    }
  }
}


if (window.addEventListener)
window.addEventListener("load", createcssmenu, false)
else if (window.attachEvent)
window.attachEvent("onload", createcssmenu)
</script>
<title>Home</title>
</head>
<body>
<%@include file="header.jsp" %>
<ul id="verticalmenu" class="glossymenu">

<s:iterator value="list1">

<input type="hidden" value="<s:property value="category_id"/>"/>
<li><a href="subcategories?category_id=<s:property value="category_id"/>"><s:property value="category_name"/></a>
</s:iterator>
</ul>
</body>
</html>