<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="/struts-tags" prefix="s" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
Book List
<table border="1" cellspacing="5">
<s:iterator value="lstbooks" >
<tr>
<td> <s:property value="author" /></td>
<td> <s:property value="description" /></td>
<td> <s:property value="price" /></td>
<td> <s:property value="publisher" /></td>
<td> <img src="<s:property value="image" />" width="55" height="55"/></td>

</tr>
</s:iterator>
</table>


</body>
</html>