<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="/struts-tags" prefix="s" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Books</title>

<style type="text/css">
#search
{
	background-color: #0431B4; 
	
	color: white; font-family: arial, verdana, ms sans serif;
	 font-weight:bold;
	  font-size: 12pt;
	   text-align: right;
	   -moz-border-radius: 8px;
   	-webkit-border-radius: 8px;
}

#list
{

 border: 1px solid black;
 
}

#list td
{

 border: 1px solid black;

}

#list th
{

 border: 1px solid black;

}

#menu
{ 
margin:0px;
 padding:1px; 
 list-style:none; 
 color:#000000; 
 line-height:45px;
  display:inline-block;
    float:right; 
    z-index:1000; 
    border-width:1px;
    font-size:12px;
 }


#menu a 
{ 
color:#000000;
 text-decoration:none;
}

#menu > li {background:#FFFFFF none repeat scroll 0 0; cursor:pointer; float:left; position:relative;
    padding:0px 10px; border-width:1px;
    }

#menu > li a:hover {color:#000000;}

#menu .logo {background:transparent none repeat scroll 0% 0%; padding:0px;
    background-color:Transparent;}
/* sub-menus*/
#menu ul {
 padding:0px; margin:0px; display:block; display:inline;
 float:right;
 }
#menu li ul { position:absolute; left:-10px; top:0px; margin-top:45px; width:150px; line-height:16px;
    background-color:#FFFFFF; color:#000000; border: 1px solid black;/* for IE */ display:none; }
#menu li:hover ul { display:block;}
#menu li ul li{ display:block; margin:5px 20px; padding: 5px 0px; border-top: dotted 1px #0000FF;
    list-style-type:none; }
#menu li ul li:first-child { border-top: none; }
#menu li ul li a { display:block; color:#000000; }
#menu li ul li a:hover { color:#800080; }
/* main submenu */
#menu #main { left:0px; top:-20px; padding-top:20px; background-color:#FFFFFF; color:#000000;
    z-index:999;}




#footer {
    border-top: 1px solid #000;
    height: 20px;
    top:90%;
    font-size:10px;
}




a { color:#08088A;
text-decoration:none;
 }
 a:visited { color: #08088A; }
 a:hover { color: #6A0888; }
 a:active { color: #08088A; }
 
 
#menu1
{
background-color: #E6E6E6;
width:100%;
}



#tbl1
{
 	padding: 20px;
	background-color:#E6E6E6;
	width:100%;
}



#footer {
    border-top: 1px solid #000;
    height: 20px;
    top:90%;
    font-size:9px;
}


#tblmenu
{
 border: 1px solid #E6E6E6;
 width:40%;
}


#tblbrowse
{
 border: 1px solid #E6E6E6;
	
}


#tbllist
{
 border: 1px solid #E6E6E6;
 width:100%;
}


</style>

</head>
<body>

<div align="right">
<table >
<tr>
<td width="50%"><img src="../../ebayimages/ebay-logo.jpg" width="10%" height="10%"></td>
</tr>
<tr>
<td>
Hi
<s:if test="#session.user!=null && #session.loggedin == 'true'">
<s:property value="#session.user.fname"/> <s:property value="#session.user.lname"/>(<s:property value="#session.user.ebay_id"/>)! <s:a href="SignOut.action">SignOut</s:a>
</s:if>
<s:else>
Guest! (<s:a href="SignIn.action">Sign In</s:a>)
</s:else>
</td>
<td  valign="top">
<div id="menulist">
<ul id="menu">
  <li><a href="">My Ebay</a>&nbsp;|
  <ul>
  <li><a href="">Summary</a>
  <li><a href="">Bids/Offers</a>
  <li><a href="">Watch List</a>
  <li><a href="">Wish List</a>
  <li><a href="">All Lists</a>
  <li><a href="">Purchase History</a>
  
  </ul>
  </li>
  <li>Sell&nbsp;|
  <ul>
  <li><a href="">Sell an item</a>
  <li><a href="">Check for instant offers</a>
  <li><a href="">Sell it for me</a>
  <li><a href="">Seller information center</a>
  </ul>
  
  </li>
  <li>Community&nbsp;|
    <ul><li><a href="">Announcement</a>
    <li><a href="">Answer center</a>
    <li><a href="">Discussion forums</a>
    <li><a href="">Preview new features</a>
    <li><a href="">Green Shopping</a>
    <li><a href="">Groups</a>
    <li><a href="">eBay Top Shared</a>
  	</li>
  	</ul>
  </li>
  
  <li><a href="">Customer Support</a>&nbsp;|
    <ul><li><a href="">Customer Support</a>
    <li><a href="">Learning Center</a>
    <li><a href="">Resolution Center</a>
    <li><a href="">eBay University</a>
    </li>
  	</ul>
  </li>
  
  <li><a href="">Cart</a>
  
  </li>
  
    
</ul>
</div>
</td>
</tr>
</table>
</div>

<input type="text" width="40%" name="txtSearch">
<select>
<option>Books
<option>Antiques
</select>
<input type="submit" id="search" name="search" value="Search">
<br>

<hr color="red">
<table id="menu1">
<tr>
<td>Categories

<td>Electronics</td>
<td>Fashion</td>
<td>Motor</td>
<td>Tickets</td>
<td>Deals</td>
<td>Classfieds</td>
<td colspan="8" align="center"><img src="ebaysecurity1.png" width="4%" height="4%"/>Ebay Buyer Protection &nbsp;&nbsp;&nbsp;<a href="">Learn more</a>
</tr>
</table>
</body>
</html>