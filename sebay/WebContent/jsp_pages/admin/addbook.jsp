<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="/struts-tags" prefix="s" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<%@include file="logoheader.jsp" %>
<s:form  method="post" action="addbook" enctype="multipart/form-data" name= "book">
<table>
<tr>
<td>Category_id
<td><input name="category_id"  type="text"/> 
</td>

<tr>
<td>subcategory_id
<td><input name="subcategory_id"  type="text"/> 
</td>
<tr>
<td>book_id
<td><input name="book_id"  type="text"/> 
</td>
<tr>
<td>title
<td><input name="title"  type="text"/> 
</td>
<tr>
<td>description
<td><input name="description"  type="text"/> 
</td>
<tr>
<td>price
<td><input name="price"  type="text"/> 
</td>

<tr>
<td>cond
<td><select  name="cond">
<option>Good
<option>Bad
<option>Very Good
<option>Second Hand
<option>Brand New
</option>
</select> 
</td>

<tr>
<td>pages
<td><input name="pages"  type="text"/> 
</td>

<tr>
<td>author
<td><input name="author"  type="text"/> 
</td>

<tr>
<td>publisher
<td><input name="publisher"  type="text"/> 
</td>

<tr>
<td>lang
<td><input name="lang"  type="text"/> 
</td>

<tr>
<td>format_type
<td><input name="format_type"  type="text"/> 
</td>

<tr>
<td>biddingstatus
<td><input name="biddingstatus"  type="text"/> 
</td>

<tr>
<td>bid_id
<td><input name="bid_id"  type="text"/> 
</td>


<tr>
<td>quantity
<td><input name="quantity"  type="text"/> 
</td>
<tr><td><s:file name="image" label="Item Image"  /></td></tr>


<tr><td><s:submit value="add item"></s:submit>
</table>


</s:form>
</body>
</html>