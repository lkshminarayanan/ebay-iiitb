<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Registeration Details</title>
</head>
<body>
<fieldset>
<legend><h1>Registration details</h1></legend>
<fieldset>
<img src="correct.gif" alt=" " width="30" height="30">Your registration details have been successfully updated.
</fieldset>
<br></br>
<fieldset>
<legend>Details</legend>
  <p>&nbsp;</p>
  <table width="531" height="163" border="0">
    <tr>
      <td width="203"><b>Payee Name :</b></td>
      <td width="439"><s:property value="payee_name"/> </td>
    </tr>
    <tr>
      <td><b>Bank Account Number :</b></td>
      <td><s:property value="bank_acc_num"/></td>
    </tr>
    <tr>
      <td><b>Bank Name :</b></td>
      <td><s:property value="bank_name"/></td>
    </tr>
    <tr>
      <td><b>Branch :</b></td>
      <td><s:property value="branch"/></td>
    </tr>
    <tr>
      <td><b>City :</b></td>
      <td><s:property value="city"/></td>
    </tr>
    
  </table>
  <p>&nbsp;</p>
</fieldset>

</fieldset>
</body>
</html>