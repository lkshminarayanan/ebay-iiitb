<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="sx" uri="/struts-dojo-tags"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Transaction Details</title>
</head>
<sx:head />
<body>
	<%@ include file="../../header.jsp"%>

	<sx:tabbedpanel id="text">

		<%-- 		<sx:div id="All" label="All" cssClass="d">


			<table width="1091" border="0">
				<tr>
					<th width="233" scope="col">Item Title</th>
					<th width="288" scope="col">Shipping Method</th>
					<th width="129" scope="col">Quantity</th>
					<th width="207" scope="col">Total</th>
					<th width="200" scope="col">Current Status</th>
				</tr>
				<s:iterator value="alllist">
					<tr align="center">
					  <td><s:property value="alltitle"/></td>
						<td><s:property value="allshippingmethod"/></td>
						<td><s:property value="allquantity"/></td>
						<td><s:property value="alltotal"/></td>
						<td><s:property value="allcurrentstatus"/></td>
						
					</tr>
					
				</s:iterator>
				<tr align="center">
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td><b>Amount Paid</b></td>
					<td><s:property value="allamountpaid"/>  </td>
					<td>&nbsp;</td>
				</tr>
		  </table>
</sx:div> --%>


		<sx:div id="registrationdetails" label="Registration Details">

			<table width="866" height="163" border="0">
				<tr>
					<td width="372"><font size="4" face="Comic Sans MS, Calibri"><b>Payee
								Name </b></font></td>
					<td width="484">:<font size="4" face="Comic Sans MS, Calibri">
							<s:property value="payee_name" />
					</font></td>
				</tr>
				<tr>
					<td><font size="4" face="Comic Sans MS, Calibri"><b>Bank
								Account Number </b></font></td>
					<td>:<font size="4" face="Comic Sans MS, Calibri"> <s:property
								value="bank_acc_num" />
					</font></td>
				</tr>
				<tr>
					<td><font size="4" face="Comic Sans MS, Calibri"><b>Bank
								Name </b></font></td>
					<td>:<font size="4" face="Comic Sans MS, Calibri"> <s:property
								value="bank_name" />
					</font></td>
				</tr>
				<tr>
					<td><font size="4" face="Comic Sans MS, Calibri"><b>Branch
						</b></font></td>
					<td>:<font size="4" face="Comic Sans MS, Calibri"> <s:property
								value="branch" />
					</font></td>
				</tr>
				<tr>
					<td><font size="4" face="Comic Sans MS, Calibri"><b>City
						</b></font></td>
					<td>:<font size="4" face="Comic Sans MS, Calibri"> <s:property
								value="city" />
					</font></td>
				</tr>
			</table>
			<p>&nbsp;</p>
		</sx:div>
		<sx:div id="buy" label="Bought With Paisa Pay">
			<s:property value="%{#session.emp_id}" />
				<s:if test="%{#session.valuebuy!=null}">
					<p>&nbsp;</p>
					<table width="900" border="0">
						<tr align="center" valign="middle">

							<th width="235" scope="col"><font size="4"
								face="Comic Sans MS, Calibri"><u>Item Title</u></font></th>
							<th width="287" scope="col"><font size="4"
								face="Comic Sans MS, Calibri"><u><!-- Shipping Method --></u></font></th>
							<th width="130" scope="col"><font size="4"
								face="Comic Sans MS, Calibri"><u>Quantity</u></font></th>
							<th width="207" scope="col"><font size="4"
								face="Comic Sans MS, Calibri"><u>Total</u></font></th>
							<th width="197" scope="col"><font size="4"
								face="Comic Sans MS, Calibri"><u>Current Status</u></font></th>
						</tr>
						<s:iterator value="buylist">
							<tr align="center" valign="middle">
								<td><font size="4" face="Comic Sans MS, Calibri"> <s:property
											value="buytitle" />
								</font></td>
								<td><font size="4" face="Comic Sans MS, Calibri"> <s:property
											value="buyshippingmethod" />
								</font></td>
								<td><font size="4" face="Comic Sans MS, Calibri"> <s:property
											value="buyquantity" />
								</font></td>
								<td><font size="4" face="Comic Sans MS, Calibri"> <s:property
											value="buytotal" />
								</font></td>
								<td><font size="4" face="Comic Sans MS, Calibri"> <s:a href="paisapaystatusupdate?id=%{buystatus}&bank_third_party_id=%{bank_third_party_id}"><s:property
											value="buycurrentstatus" /></s:a>
								</font></td>
							</tr>
						</s:iterator>
						<tr align="center" valign="middle">
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td><font size="4" face="Comic Sans MS, Calibri"><b>Amount
										Paid</b></font></td>
							<td><font size="4" face="Comic Sans MS, Calibri"> <s:property
										value="buyamountpaid" />
							</font></td>

							<td>&nbsp;</td>
						</tr>
					</table>

					<br>
					<br>
				</s:if>
				<s:else>
					<br>
					<font size="5" face="Comic Sans MS, Calibri">No Items bought
						yet.</font>
					<i><br> </i>
				</s:else>
			</sx:div>
			<s:property value="%{#session.emp_id}" />
			<sx:div id="sell" label="Sold With Paisa Pay">
				<s:if test="%{#session.valuesold!=null}">
					<p>&nbsp;</p>
					<table width="900" border="0">
						<tr align="center" valign="middle">

							<th width="235" scope="col"><font size="4"
								face="Comic Sans MS, Calibri"><u>Item Title</u></font></th>
							<th width="287" scope="col"><font size="4"
								face="Comic Sans MS, Calibri"><u><!-- Shipping Method --></u></font></th>
							<th width="130" scope="col"><font size="4"
								face="Comic Sans MS, Calibri"><u>Quantity</u></font></th>
							<th width="207" scope="col"><font size="4"
								face="Comic Sans MS, Calibri"><u>Total</u></font></th>
							<th width="197" scope="col"><font size="4"
								face="Comic Sans MS, Calibri"><u>Current Status</u></font></th>
						</tr>
						<s:iterator value="soldlist">
							<tr align="center" valign="middle">
								<td><font size="4" face="Comic Sans MS, Calibri"> <s:property
											value="soldtitle" />
								</font></td>
								<td><font size="4" face="Comic Sans MS, Calibri"> <s:property
											value="soldshippingmethod" />
								</font></td>
								<td><font size="4" face="Comic Sans MS, Calibri"> <s:property
											value="soldquantity" />
								</font></td>
								<td><font size="4" face="Comic Sans MS, Calibri"> <s:property
											value="soldtotal" />
								</font></td>
								<td><font size="4" face="Comic Sans MS, Calibri"> <s:property
											value="soldcurrentstatus" />
								</font></td>
							</tr>
						</s:iterator>
						<tr align="center" valign="middle">
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td><font size="4" face="Comic Sans MS, Calibri"><b>Amount
										Paid</b></font></td>
							<td><font size="4" face="Comic Sans MS, Calibri"> <s:property
										value="soldamountpaid" />
							</font></td>

							<td>&nbsp;</td>
						</tr>
					</table>

					<br>
					<br>
				</s:if>
				<s:else>
					<br>
					<font size="5" face="Comic Sans MS, Calibri">No Items sold
						yet.</font>
					<i><br> </i>
				</s:else>
			</sx:div>

			<sx:div label="Bank Account Details">
				<table width="899" border="0">
					<tr>
						<td width="186"><font size="4" face="Comic Sans MS, Calibri">Customer
								Name</font></td>
						<td width="703"><font size="4" face="Comic Sans MS, Calibri">
								<s:property value="customername" />
						</font></td>
					</tr>

					<tr>
						<td><font size="4" face="Comic Sans MS, Calibri">Bank
								Name</font></td>
						<td><font size="4" face="Comic Sans MS, Calibri"> <s:property
									value="bankname" />
						</font></td>
					</tr>

					<tr>
						<td><font size="4" face="Comic Sans MS, Calibri">Branch
								Name</font></td>
						<td><font size="4" face="Comic Sans MS, Calibri"> <s:property
									value="branchname" />
						</font></td>
					</tr>

					<tr>
						<td><font size="4" face="Comic Sans MS, Calibri">Account
								Number</font></td>
						<td><font size="4" face="Comic Sans MS, Calibri"> <s:property
									value="bank_acc_num" />
						</font></td>
					</tr>

					<tr>
						<td><font size="4" face="Comic Sans MS, Calibri">Account
								Type</font></td>
						<td><font size="4" face="Comic Sans MS, Calibri"> <s:property
									value="accounttype" />
						</font></td>
					</tr>

					<tr>
						<td><font size="4" face="Comic Sans MS, Calibri">Balance</font></td>
						<td><font size="4" face="Comic Sans MS, Calibri"> <s:property
									value="balance" /> INR
						</font></td>
					</tr>
				</table>

			</sx:div>
	</sx:tabbedpanel>
</body>
</html>