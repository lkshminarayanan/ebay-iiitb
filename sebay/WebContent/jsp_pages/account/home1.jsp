
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBdivC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<style type="text/css">

.glossymenu, .glossymenu li ul{
list-style-type: none;
margin: 0;
padding: 0;
width: 185px; /*WIDTH OF MAIN MENU ITEMS*/
border: 1px solid #4D4D4D;
}

table
{
font-size:12px;
padding: 1px;
}

.glossymenu li{
position: relative;
border:solid;
border-color:#CFCFCF;
border-width: 1px;
}

.glossymenu li a{
background:#EDEDED ;
font: bold 12px Verdana, Helvetica, sans-serif;
color:black;
display: block;
width: auto;
padding: 5px 0;
padding-left: 10px;
text-decoration: none;
}

.glossymenu li ul{ /*SUB MENU STYLE*/
position: absolute;
width: auto; /*WIDTH OF SUB MENU ITEMS*/
left: 0;
top: 0;
display: none;
}

.glossymenu li ul li{
float: left;
}

.glossymenu li ul a{
width: 180px; /*WIDTH OF SUB MENU ITEMS - 10px padding-left for A elements */
}

.glossymenu .arrowdiv{
position: absolute;
right: 6px;
}

.glossymenu li a:visited, .glossymenu li a:active{
color: black;
}

.glossymenu li a:hover{
background: #FFFFFF;
}
.style1 {font-weight: bold;
color: #333333;

}
.style2 {
	font-size: 18px;
	font-weight: bold;
}
.recomd{
height:125;
width:15%;
border-width: 1px;
border-style: solid;
border-color: #999;
}
.style3 {color: #0000FF;
margin-top: 10;
}
/* Holly Hack for IE \*/
* html .glossymenu li { float: left; height: 1%; }
* html .glossymenu li a { height: 1%; }
/* End */
</style>

<script type="text/javascript">
var menuids=new Array("verticalmenu") //Enter id(s) of UL menus, separated by commas
var submenuoffset=-2 //Offset of submenus from main menu. Default is -2 pixels.

function createcssmenu(){
for (var i=0; i<menuids.length; i++){
  var ultags=document.getElementById(menuids[i]).getElementsByTagName("ul")
    for (var t=0; t<ultags.length; t++){
    var spanref=document.createElement("span")
		spanref.className="arrowdiv"
		spanref.innerHTML="&nbsp;&nbsp;"
		ultags[t].parentNode.getElementsByTagName("a")[0].appendChild(spanref)
    ultags[t].parentNode.onmouseover=function(){
    this.getElementsByTagName("ul")[0].style.left=this.parentNode.offsetWidth+submenuoffset+"px"
    this.getElementsByTagName("ul")[0].style.display="block"
    }
    ultags[t].parentNode.onmouseout=function(){
    this.getElementsByTagName("ul")[0].style.display="none"
    }
    }
  }
}


if (window.addEventListener)
window.addEventListener("load", createcssmenu, false)
else if (window.attachEvent)
window.attachEvent("onload", createcssmenu)
</script>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />




<!-- Start css3menu.com HEAD section -->
<link rel="stylesheet" href="Home_files/css3menu1/style.css" type="text/css" />
<!-- End css3menu.com HEAD section -->

</head>

<body marginwidth="100px" bgcolor="#FFFFFF">
<span style="float:right">
<a href="jsp_pages/account/Home.jsp"><strong><span class="style1">My Ebay</span></strong></a>&nbsp;|&nbsp;
<a href="jsp_pages/account/SignIn"><strong><span class="style1">Sell</span></strong></a>&nbsp;|&nbsp;
<a href="showpaisapaydetail"><strong><span class="style1">My PaisaPay</span></strong></a>&nbsp;|&nbsp;
<a href="jsp_pages/account/Home.jsp"><strong><span class="style1">Customer Support</span></strong></a>&nbsp;|&nbsp;
<s:if test="%{#session.loggedin!='true'}">
<a href="jsp_pages/account/SignIn.jsp"><strong><span class="style1"><img src="ebayimages/iconCart000.gif" height="24" width="31" />Cart</span></strong></a>&nbsp;|&nbsp;</span>
</s:if>
<s:else>
<a href="getCart"><strong><span class="style1"><img src="ebayimages/iconCart000.gif" height="24" width="31" />Cart</span></strong></a>&nbsp;|&nbsp;</span>

</s:else>
<div>
<table>
<tr><td>
<img src="ebayimages/logoEbay_x45.gif" style="height:50px; width:150px ; float:left;margin-left: 10px" /></td><td style="margin-top: 5px">
Welcome!&nbsp;
<s:if test="%{#session.loggedin!='true'}">
<a href="jsp_pages/account/SignIn.jsp" class="style3">Sign In</a> or<a href="jsp_pages/account/registration.jsp" class="style3"> Register</a>
</s:if>
<s:else>
<s:property value="%{#session.fname}"/>&nbsp;<a href="logout" class="style3">Logout</a> 
</s:else>

</td></tr>
</table>

<br />
<form action="">
<div adivgn="center" width="100%" height="50" style="background-color:#e3e3e3" >
<div style="border-bottom-color:#996600" adivgn="center" ><div><input type="text" name="_nkw" id="_nkw" value="" maxlength="300"  autocomplete="OFF" style="width:60%; height:inherit">&nbsp;<s:select 
  name="cat_name" 
  headerKey="1"
  headerValue="--Select Category --"
  listValue="category_name"
  
  list="%{#session.catlist}"
  />&nbsp;    <input type="submit" name="search" width="40" height="40"/>&nbsp;<a href="advancesearch.jsp">Advanced</a></div>
</div>
</div>
</form>
</div>
<div>
<img src="ebayimages/hp.jpg" height="297" width="80%" align="right" border="2"  />

<div >

<ul id="verticalmenu" class="glossymenu">
<s:iterator value="list1">
<input type="hidden" value="<s:property value="category_id"/>"/>
<li><a href="subcategories?category_id=<s:property value="category_id"/>"><s:property value="category_name"/></a>
</s:iterator>
</ul>
</div>

</div>

</div>

</div>

</body>
</html>
