<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="/struts-dojo-tags" prefix="sx"%>
<%@taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>My Ebay Home</title>

<style type="text/css">
table {
	padding: 1px;
	border-collapse: collapse;
	border: 1px solid black;
}

tr,td {
	padding: 3px;
}

a {
	text-decoration: none;
	color: blue;
}

.d {
	width: 1000;
	height: 80%
}
.style1 {
	color: #000000;
	font-weight: bold;
}
.style2 {
	font-family:  Calibri;
	color: #FF0000;
	font-style: italic;
	font-size: medium;
}
</style>
<sx:head />

</head>
<body>
	<%@include file="../../header.jsp" %>

	<form>

		<sx:tabbedpanel id="text" cssClass="d">

			<sx:div id="Activity" label="Activity" cssClass="d" onmouseover="changecolor">
				<table width="100%">
					<tr>
						<td width="20%">
							<table width="100%">
								<tr>
									<td bgcolor="#F4F4F4"><font color="blue">Summary</font> <br>
										<hr width="100%" />
								<tr>
									<td bgcolor="#F4F4F4"><b><font color="blue">Buy</font></b> <br>
										<hr width="100%" />
								<tr>
									<td bgcolor="#F4F4F4"><a href="myebayshowbids">Bids</a>
								<tr>
<!-- 									<td bgcolor="#F4F4F4"><a href="myebayloosebids">Didn't win</a> -->
								<tr>
									<td bgcolor="#F4F4F4"><a href="myebaypurchasehistory">Purchase History</a>
								<tr>
									<td bgcolor="#F4F4F4"><b><font color="blue">Sell</font></b> <br>
										<hr width="100%" />
								<tr>
									<td bgcolor="#F4F4F4"><a href="myebayallsales">All Selling</a>
								<tr>
<!-- 									<td bgcolor="#F4F4F4"><a href="myebaysoldsales">Sold</a> -->
								<tr>
<!-- 									<td bgcolor="#F4F4F4"><a href="myebayunsoldsales">UnSold</a> -->
							</table>
						<td valign="top">
						
								<s:iterator value="listbidm">
								<table width = "100%" border="0">
									<tr>
										<td><span class="style2">
										<s:if test="%{won==2}">
									    <s:a href="buyNow?subcategory_id=%{subcategory_id}&category_id=%{category_id}&product_id=%{product_id}&quantitypurchased=1"><s:property value="status"/></s:a></s:if>
									    <s:else><s:property value="status"/></s:else>
									  </span></td>
										<td><s:a href="showItemDetails?category_id=%{category_id}&subcategory_id=%{subcategory_id}&product_id=%{product_id}">
												<div align="center">
													<img src="<s:property value="image"/>" width="100"
														height="120" />
												</div>
											</s:a></td>
										<td><s:a href="showItemDetails?category_id=%{category_id}&subcategory_id=%{subcategory_id}&product_id=%{product_id}">
												<s:property value="name" />
												<div align="center"></div>
											</s:a></td>
										<td><s:property value="no_of_bids" />
											<div align="center"></div></td>
										<td><s:property value="price" />
											<div align="center"></div></td>
										<td><s:property value="timeleft" />
											<div align="center"></div></td>
										<td><s:property value="actions" />
											<div align="center"></div></td>
									</tr>
								  </table>
									<hr>
								</s:iterator>
								
							 
							<s:iterator value="listpurchase">
								<table border="0">
									<tr>
									                                      
										<td width="101"><s:a href="showItemDetails?category_id=%{category_id}&subcategory_id=%{subcategory_id}&product_id=%{product_id}"><img src="<s:property value="image"/>" width="80px"
											height="110px" /></s:a>
										<td width="250" align="center"><s:property value="title" />
									  <td width="450"><s:property value="description" />
									  <td width="252"><s:property value="price" />
								</table>
								<hr>
							</s:iterator> 
							
							<s:iterator value="listlossbid">
								<table width="713" border="1">
									<tr>
										<td width="101"><img src="<s:property value="image"/>" width="800px"
											height="110px" />
										<td width="180"><s:property value="title" />
									  <td width="229"><s:property value="description" />
									  <td width="252"><s:property value="price" />
							  </table>
							</s:iterator> 
<!-- ---------------------------------------------------------------------------------------------------------- -->
							<s:iterator value="listsoldsale">
								<table border="0">
									<tr>
										<td width="178"><img src="<s:property value="image"/>" width="80px"
											height="110px" />
										<td width="89"><s:property value="title" />
									  <td width="9"><s:property value="description" />
									  <td width="408"><s:property value="price" />
								</table>
							</s:iterator> 
<!-- ---------------------------------------------------------------------------------------------------------- -->							
							<s:iterator value="listunsoldsale">
								<table width="713" border="1">
									<tr>
										<td width="181"><s:a href="showItemDetails?category_id=%{category_id}&subcategory_id=%{subcategory_id}&product_id=%{product_id}"><img src="<s:property value="image"/>" width="150px"
											height="100px" /></s:a>
										<td width="463"><s:property value="title" />
									  <td width="20"><s:property value="description" />
									  <td width="21"><s:property value="price" />
							  </table>
							</s:iterator> 
<!-- ---------------------------------------------------------------------------------------------------------- -->							
							<s:iterator value="listsale">
								<table width="100%" border="0">
								
									<tr>
										<td width="12%"><s:a href="showItemDetails?category_id=%{category_id}&subcategory_id=%{subcategory_id}&product_id=%{product_id}"><img src="<s:property value="image"/>" width="80px"
											height="110px" /></s:a>
										<td width="250" align="center"><s:property value="title" />
									  <td width="450"><s:property value="description" />
									  <td width="180"><s:property value="price" />
							  </table>
							  <hr>
							</s:iterator>
				</table>
			</sx:div>


			<sx:div id="Account" label="Account">
				<table width="100%" border="1">
					<tr>
						<td width="20%" bgcolor="#F4F4F4">
							<table width="100%"  height="260">
								<tr>
									<td bgcolor="#F4F4F4"><a href="myebaypersonalinfo">Personal Info</a>
								<tr>
									<td bgcolor="#F4F4F4"><a href="myebayaddress">Address</a>
								<tr>
<!-- 									<td><a href="myebaypaypal">PayPal Account</a> -->
						  </table>
					  <td><s:if test="%{status==1}">
								<table width="100%">


									<tr>
										<td><font color="blue"> Your Primary Address is<br />
										</font> <s:iterator value="listaddress">

												<s:property />
												<br />

											</s:iterator>
										<!-- <td><font color="blue"><a
												href="myebaychangeprimaryaddress">Change Your Primary
													Address</a> </font> <br /> -->
									<tr>
										<td colspan="2"><hr width="100%" color="blue" />
									<tr>
										<td><font color="blue"> Your Shipping Address is:</font><br />
											<s:iterator value="listshippingaddress">
												<s:property />
												<br />

											</s:iterator>
										<!-- <td><a href="myebaychangeshippingaddress">Change You
												Shipping Address</a> -->
								</table>
							</s:if> <s:elseif test="%{status==2}">

								<table width="100%">
									<s:iterator value="listpersonalinfo">
										<tr bgcolor="#F4F4F4">
											<td colspan="2"><div align="center" class="style1">Personal Info:
											        </div>
									  <tr>
											<td width="29%">Name
											<td width="71%"><s:property value="fname" />&nbsp;&nbsp;&nbsp;<s:property
													value="lname" />
									  <tr>
											<td>Date Of Birth
											<td><s:property value="dob" />
										<tr>
											<td>Street Address
											<td><s:property value="streetaddress" />
										<tr>
											<td>City
											<td><s:property value="city" />
										<tr>
											<td>State
											<td><s:property value="state" />
										<tr>
											<td>Country
											<td><s:property value="country" />
										<tr>
											<td>Zip
											<td><s:property value="zip" />
										<tr>
											<td>Email
											<td><s:property value="email" />
										<tr>
											<td>Phone_no
											<td><s:property value="phone_no" /></td>
										</tr>
										<tr>
											<td colspan="2"><center>
													<a href="myebayeditinfo">Edit Info</a>
								    </center>									</s:iterator>
								</table>
							</s:elseif>
			  </table>
			</sx:div>
		</sx:tabbedpanel>
	</form>

</body>
</html>