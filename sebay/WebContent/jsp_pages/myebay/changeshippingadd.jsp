<%@taglib prefix="s" uri="/struts-tags" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title></title>
</head>
<body>
<a href="">Back to Addresses in My eBay</a>
<h1>UPDATE SHIPPING ADDRESSES</h1>
You can delete an existing address, or add a new address below.
<h3>Primary Shipping Address :</h3>
Your main Shipping Address for purchases 
<h3>Add New Address</h3>
If you've moved, you may want to review your other addresses to make sure they are correct(for example: your registration address). Return to Addresses in My eBay.
<br>

<form action="myebayupdateshippingaddress" method="post">
<table>
<tr>
<td><s:textfield name="line1" label="Line1"></s:textfield></td>
</tr>
<tr>
<td><s:textfield name="line2" label="Line2"></s:textfield></td>
</tr>
<tr>
<td><s:textfield name="city" label="City"></s:textfield></td>
</tr>
<tr>
<td>State/Region</td> <td><select name="state">

<option selected>Select your State</option>

<option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>

<option value="Andhra Pradesh">Andhra Pradesh</option>

<option value="Arunachal Pradesh">Arunachal Pradesh</option>

<option value="Assam">Assam</option>

<option value="Bihar">Bihar</option>

<option value="Chandigarh">Chandigarh</option>

<option value="Chhattisgarh">Chhattisgarh</option>

<option value="Dadra and Nagar Haveli">Dadra and Nagar Haveli</option>

<option value="Daman and Diu">Daman and Diu</option>

<option value="Delhi">Delhi</option>

<option value="Goa">Goa</option>

<option value="Gujarat">Gujarat</option>

<option value="Haryana">Haryana</option>

<option value="Himachal Pradesh">Himachal Pradesh</option>

<option value="Jammu and Kashmir">Jammu and Kashmir</option>

<option value="Jharkhand">Jharkhand</option>

<option value="Karnataka">Karnataka</option>

<option value="Kerala">Kerala</option>

<option value="Lakshadweep">Lakshadweep</option>

<option value="Madhya Pradesh">Madhya Pradesh</option>

<option value="Maharashtra">Maharashtra</option>

<option value="Manipur">Manipur</option>

<option value="Meghalaya">Meghalaya</option>

<option value="Mizoram">Mizoram</option>

<option value="Nagaland">Nagaland</option>

<option value="Orissa">Orissa</option>

<option value="Pondicherry">Pondicherry</option>

<option value="Punjab">Punjab</option>

<option value="Rajasthan">Rajasthan</option>

<option value="Sikkim">Sikkim</option>

<option value="Tamil Nadu">Tamil Nadu</option>

<option value="Tripura">Tripura</option>

<option value="Uttaranchal">Uttaranchal</option>

<option value="Uttar Pradesh">Uttar Pradesh</option>

<option value="West Bengal">West Bengal</option></select></td>
</tr>

<tr>
<td><s:textfield name="pincode" label="Pincode"></s:textfield></td>
</tr>

<tr><td>Country</td><td><select name="country" >
<option selected>India</option>
</select>
</td>
</tr>

<tr>
<s:checkbox name="same" label="Make this my primary shipping address"></s:checkbox>
</tr>
<tr>
<td></td><td><s:submit value="Add New Address"></s:submit></td>
</tr>
</table>
</form>
</body>
</html>