<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><s:property value="item.title"/> | Order Details | Ebay</title>
<style type="text/css">
.cellBorder{
border: 1px solid #E2E2E2; 
margin: -2px;
}
.errorsBg {
    background-color: #FFDDDD;
    border: 1px solid;
    color: red;
}
</style>
</head>
<body>

	<table cellspacing="6" cellpadding="0" align="center" width="90%">
	<tr><td width="100%"><a href="home.action"><img border="0" alt="eBay" src="./header_files/logoEbay_x45.gif"></a></td></tr>

	<tr><td><hr color="red"/></td></tr>
	
	<tr><td align="right"><s:a action="showItemDetails?category_id=%{item.category_id}&subcategory_id=%{item.subcategory_id}&product_id=%{item.product_id}">Back to item description</s:a></td></tr>
	<s:if test='errorMessage!=null&&!errorMessage.equals("")'>
	<tr><td align="center" class="errorsBg"><s:property escapeHtml="false" value="errorMessage"/>
	</s:if>
	<tr><td>	<br/>

	<table width="100%" bgcolor="#E6E6E6">
		<tr>
			<td><font face='Trebuchet,"Trebuchet MS"' size="4">Review the items you are buying and your order total</font></td>
	</table>
	<tr><td class="cellBorder"><form action="buyNow">
	<s:hidden name="category_id" value="%{item.category_id}" />
		<s:hidden name="subcategory_id" value="%{item.subcategory_id}"/>
		<s:hidden name="product_id" value="%{item.product_id}"/>

		<table width="100%">
		<tr>
			<th width="15%"/>
			<th width="25%"/>
			<th width="20%">Price</th>
			<th width="20%">Quantity</th>
			<th width="20%">Sub Total</th></tr>
		<tr>
			
				<td><img src="<s:property value="item.image" />" width="80px"
					height="120px" />
				</td>

				<td style='font-face:Trebuchet,"Trebuchet MS"'><s:a action="showItemDetails?category_id=%{item.category_id}&subcategory_id=%{item.subcategory_id}&product_id=%{item.product_id}"><s:property value="item.title" /></s:a><br><font size="2">seller : <s:property value="item.seller_id" /></font></td>
				<td align="center">&#8377;<s:property value="item.priceText" /></td>
				<td align="center"><input <s:if test='errorMessage!=null&&!errorMessage.equals("")'> class="errorsBg"</s:if> align="middle" size="3" type="text" name="quantitypurchased" value="<s:property value="quantitypurchased" />"></td>
				<td align="center">&#8377;<s:property value="subtotalText" /></td>
		</tr>
		<tr>
			<td colspan="5" align="right"><input type="submit" value="Update Order"/>
		</tr>
		</table>
		</form>
	</td></tr>
	<tr><td colspan="1">
		<table cellspacing="6" width="100%">
		<tr><td class="cellBorder" width="50%">
			<table  width="100%">
			<tr>
				<td bgcolor="#E6E6E6"><font face='Trebuchet,"Trebuchet MS"' size="4">Your Shipping Address</font></td></tr>
			
			<tr>
	
				<td width="100%">
					<table width="100%">
						<s:iterator value="shippingAddress">
						<tr>
							<td><s:property/></td>
						</tr>
						</s:iterator>
						<tr>
							<td><a href="">Change Shipping Address</a>
					</table>
					</td>
				</tr>
			</table>
		<td class="cellBorder" valign="top">
			<table width="100%">
				
				<tr bgcolor="#E6E6E6"><td colspan="2" align="right"><font face='Trebuchet,"Trebuchet MS"' size="4">Order Total And Shipping Charges</font></td>
				<tr><td >
				<tr><td valign="top" align="right">Subtotal
						<td align="center">&#8377;<s:property value="subtotalText" />
				<tr><td align="right">Shipping Method
					<td align="center">&#8377;<s:property value="item.shippingChargesText"/>
				<tr><td colspan="2"><hr color="#E6E6E6">
				<tr><td align="right">Total</td><td align="center">&#8377;<s:property value="totalText"/>  </td>
			</table>
			</table>
		</td>
	</tr>
	<tr><td>
		<form action="paynow" method="get">
		<s:hidden name="category_id" value="%{item.category_id}" />
		<s:hidden name="subcategory_id" value="%{item.subcategory_id}"/>
		<s:hidden name="product_id" value="%{item.product_id}"/>
		<s:hidden name="seller_id" value="%{item.seller_id}"/>
		<s:hidden name="quantitypurchased" value="%{quantitypurchased}"/>
		<s:hidden name="price" value="%{item.price}" />
		<s:submit value="PayNow"/>
		</form>
	</table>
</body>
</html>