<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><s:property value="item.title"/> | Bid Details | Ebay</title>
<style type="text/css">
.cellBorder{
border: 1px solid #E2E2E2; 
margin: -2px;
}
.errorsBg {
    background-color: #FFDDDD;
    border: 1px solid;
    color: red;
}
</style>
</head>
<body>

	<table cellspacing="6" cellpadding="0" align="center" width="90%">
	<tr><td width="100%"><a href="home.action"><img border="0" alt="eBay" src="./header_files/logoEbay_x45.gif"></a></td></tr>

	<tr><td><hr color="red"/></td></tr>
	
	<tr><td align="right"><s:a action="showItemDetails?category_id=%{item.category_id}&subcategory_id=%{item.subcategory_id}&product_id=%{item.product_id}">Back to item description</s:a></td></tr>
	<s:if test='!placeBid'>
	<tr><td align="center" class="errorsBg">Enter &#8377; <s:property value="item.bid.nextValidBidText"/> or more
	</s:if>
	<tr><td>	<br/>
	
	<s:if test="item.isExpired()">
	<tr><td><div align="center" class="errorsBg">
	Sorry the item you were interested in expired!
	</div>
	</s:if>
	<s:elseif test="item==null">

	<tr><td><div align="center" class="errorsBg">
	Invalid Request!
	</div>
	</s:elseif>

	<s:else>
	
	<tr><td><font face='Trebuchet,"Trebuchet MS"' color="#333333" size="5">Review and confirm your Bid.</font></td>
	<tr><td><hr color="#E6E6E6"/></td></tr>
	
	<tr><td>
	<s:if test="placeBid">
	<form action="confirmBid">
		<s:hidden name="category_id" value="%{item.category_id}" />
		<s:hidden name="subcategory_id" value="%{item.subcategory_id}"/>
		<s:hidden name="product_id" value="%{item.product_id}"/>
		<s:hidden name="bidAmount" value="%{bidAmount}"/>
		<table width="100%">
		<tr>
			<td width="25%" valign="middle" align="center" id="image" class="cellBorder" >
			<img src="<s:property value="item.image" />" width="200" height="230"/>
			</td>
			<td>
				<table width="100%">
				<tr><td style="font-size: 30px;font-family: Trebuchet,'Trebuchet MS'" colspan="2">
					<s:a action="showItemDetails?category_id=%{item.category_id}&subcategory_id=%{item.subcategory_id}&product_id=%{item.product_id}">
					<s:property value="item.title"/></s:a>
					</td></tr>
				<tr><td align="right" ><s:if test="item.bid.noOfBidders==0">Starting Price : </s:if><s:else>Current Bid : </s:else><td><b>&#8377; <s:property value="item.bid.bidPriceText"/></b></td>
				<tr><td align="right">Your Bid :</td><td style="font-size: 3"><b>&#8377; <s:property value="bidAmountText"/></b></td></tr>
				<tr><td align="right">Shipping :</td><td><s:if test="item.shippingCharges==0">Free Shipping--Flat Rate Courier - Delivery anywhere in India</s:if><s:else>&#8377;<s:property value="item.shippingChargesText"/> </s:else> </td></tr>
				<tr><td align="right">Payment :</td><td ><b>PaisaPay</b> - Credit Card, Debit Card and Online Bank Transfer </td></tr>
				<tr><td></td><td><input type="submit" value="Confirm Bid"> </tr>
				</table>
			</td>
		</table>

	</form>
	</s:if>
	<s:else>
	<form action="reviewBid">
		<s:hidden name="category_id" value="%{item.category_id}" />
		<s:hidden name="subcategory_id" value="%{item.subcategory_id}"/>
		<s:hidden name="product_id" value="%{item.product_id}"/>
		
		<table width="100%">
		<tr><td colspan="2">
		
		</td></tr>
		<tr>
			<td width="25%" valign="middle" align="center" id="image" class="cellBorder" >
			<img src="<s:property value="item.image" />" width="200" height="230"/>
			</td>
			<td>
				<table width="100%">
				<tr><td style="font-size: 30px;font-family: Trebuchet,'Trebuchet MS'" colspan="2">
					<s:a action="showItemDetails?category_id=%{item.category_id}&subcategory_id=%{item.subcategory_id}&product_id=%{item.product_id}">
					<s:property value="item.title"/></s:a>
					</td></tr>
				<tr><td align="right" ><s:if test="item.bid.noOfBidders==0">Starting Bid : </s:if><s:else>Current Bid : </s:else><td><b>&#8377; <s:property value="item.bid.bidPrice"/></b></td>
				<tr><td align="right">Your Bid :</td><td style="font-size: 3"><b>&#8377; <input type="text" name="bidAmount"></b></td></tr>
				<tr><td align="right">Shipping :</td><td><s:if test="item.shippingCharges==0">Free Shipping--Flat Rate Courier - Delivery anywhere in India</s:if><s:else>&#8377;<s:property value="item.shippingChargesText"/> </s:else> </td></tr>
				<tr><td align="right">Payment :</td><td ><b>PaisaPay</b> - Credit Card, Debit Card and Online Bank Transfer </td></tr>
				<tr><td></td><td><input type="submit" value="Place Bid"> </tr>
				</table>
			</td>
		</table>

	</form>
	
	</s:else>
	</td></tr>
	</s:else>
	</table>
</body>
</html>