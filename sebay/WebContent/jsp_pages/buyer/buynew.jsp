<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Buy New</title>

<style type="text/css">
#search
{
	background-color: #0431B4; 
	
	color: white; font-family: arial, verdana, ms sans serif;
	 font-weight:bold;
	  font-size: 12pt;
	   text-align: right;
	   -moz-border-radius: 8px;
   	-webkit-border-radius: 8px;
}

#list
{

 border: 1px solid black;
 
}

#list td
{

 border: 1px solid black;

}

#list th
{

 border: 1px solid black;

}



#footer {
    border-top: 1px solid #000;
    height: 20px;
    top:90%;
    font-size:10px;
}





a { color:#08088A;
text-decoration:none;
 }
 a:visited { color: #08088A; }
 a:hover { color: #6A0888; }
 a:active { color: #08088A; }
 
 
#menu1
{
background-color: #E6E6E6;
width:100%;
}



#tbl1
{
 	padding: 20px;
	background-color:#E6E6E6;
	width:100%;
}



#footer {
    border-top: 1px solid #000;
    height: 20px;
    top:90%;
    font-size:9px;
}


#tblmenu
{
 border: 1px solid #E6E6E6;
 width:40%;
}


#tblbrowse
{
 border: 1px solid #E6E6E6;
	
}


#image
{
 border: 1px solid #E6E6E6;
	
}



#tbllist
{
 border: 1px solid #E6E6E6;
 width:100%;
}


</style>


</head>
<body>

<%@ include file="../../header.jsp" %><br/>
<br><font size="3">
<a href="">Home</a> ><a href=""> Buy</a> ><a href="">Books</a>
</font><br>
<h4>Books</h4>
<hr width="100%">
<form action="buynew">
<table width="100%" >
<s:iterator value="lstbooks">
<input type="hidden" name="book_id" value="<s:property value="book_id" />">

<tr><td width="30%" id="image"><img src="<s:property value="image" />" width="60%" height="300"/>
<td width="70%">
<table width="100%">
<tr><td   colspan="2" align="center" width="20%" valign="top"><s:property value="title"/><td>
<tr><td colspan="2" width="100%"><hr width="100%"/>
<tr><td valign="top">Item Condition:

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
 
<b><s:property value="cond"/></b>
<td></td>
<tr><td>Time Left
</td>

<tr><td width="50%">
Quantity
<input type="text" name="quantitypurchased"/>

<tr><td colspan="2">
<table width="100%" bgcolor="#E6E6E6">
<tr><td><b><s:property value="price"/></b>
<td><input type="submit" value="Buy New"/>
<tr><td><td><a href="addtocart">add to cart</a>
</table>

<tr>
<td colspan="2">Shipping

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Read item description or Contact seller
 

<tr>
<td colspan="2">Delievery
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Varies

<tr>
<td>Payments
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Paypal
 
 <tr>
 <td>Returns
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;14 days money back
</table>
</td>
</tr>
</s:iterator>

</table>

</form>
</form>
</body>
</html>