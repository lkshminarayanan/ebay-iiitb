<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><s:property value="item.title"/> | Bid Details | Ebay</title>
<style type="text/css">
.cellBorder{
border: 1px solid #E2E2E2; 
margin: -2px;
}
.errorsBg {
    background-color: #FFDDDD;
    border: 1px solid;
    color: red;
}
</style>
</head>
<body>
<%@ include file="../../header.jsp" %><br/>
<table width="100%" cellpadding="4" >
<tr bgcolor="#d6dcfe">
<td>
<h1 style="display:inline; font-size:24px; font-weight:normal;">Bid History</h1>
</td>
</tr>
<s:if test="history.size()==0">
<div align="center" class="errorsBg">
	No History Available!
	</div>
</s:if><s:else>
<tr><td class="cellBorder">	To help keep the eBay community safe, enhance bidder privacy, and protect our members from fraudulent emails (such as fake second chance offers), eBay has changed how bid history information is displayed. If you place a bid on a higher-priced item, only you and the seller of the item can view your User ID in the bid history. Other members can no longer view User IDs and will see anonymous names, such as x***y.</td></tr>
<tr style='color:#333333;font-family:Trebuchet,"Trebuchet MS";font-size: 18px'>
<td>Item Title : <s:property value="item.title"/><br/>
Time Left : <s:property value="item.timeRemaining"/>
</td>
</tr>
<tr>
<table width="100%">
<tr bgcolor="E2E2E2" style='font-family:Arial,Helvetica,sans-serif;font-size: 16px'>
<td>Bidder</td>
<td align="center">Bid Amount</td>
<td>Date of Bid</td>
</tr>
<s:if test="#session.user.ebay_id.equals(item.seller_id)">
<s:iterator value="history">
<tr style='font-family:Arial,Helvetica,sans-serif;font-size: 14px'>
<td><s:property value="user_id"/> </td>
<td align="center">&#8377; <s:property value="bidPriceText"/></td>
<td><s:property value="timeStamp"/></td>
</tr>
</s:iterator>
</s:if>
<s:else>
<s:iterator value="history">
<tr style='font-family:Arial,Helvetica,sans-serif;font-size: 14px'>
<td><s:if test="#session.user.ebay_id.equals(user_id)"><s:property value="user_id"/></s:if><s:else><s:property value="userIdEnc"/> </s:else> </td>
<td align="center">&#8377; <s:property value="bidPriceText"/></td>
<td><s:property value="timeStamp"/></td>
</tr>
</s:iterator>
</s:else>
</table>

</tr>
</s:else>
</table>
</body>
</html>