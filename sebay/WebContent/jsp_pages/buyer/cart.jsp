<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="/struts-tags" prefix="s" %>
<html>
<head>
<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>



<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>My Shopping Cart</title>
<style type="text/css">

.tbl1
{
border: 1px solid #FFD732;
}
.cellBorder{
border: 1px solid #E2E2E2; 
margin: -2px;
}
</style>
<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
<script type="text/javascript">
var userid = '<s:property value="#session.user.ebay_id"/>';
</script>
<script type="text/javascript" src="js/cart.js"></script>

</head>

<body>

<%@ include file="../../header.jsp" %>
<br />
<br />
<div style="color:#333333;font-family:Trebuchet,&quot;Trebuchet MS&quot;">
<font size="5" >My Shopping Cart</font>
<ul style="font-size: 17px">
<li>Your shopping cart might contain different items from different seller.
<li>To buy a specific item, click on the <b>Checkout Item</b> below the item.
</ul>
</div>
<s:if test="cartItems.size()==0">
Your shopping cart is empty.Click <s:a action="home">here</s:a> to continue shopping and add items to your cart.
</s:if>
<s:else>
<table class="cellBorder" width="100%">
<tr><td bgcolor="E2E2E2" style="font-size:16px;color:#333333;font-family:Trebuchet,&quot;Trebuchet MS&quot;">Items in My Cart</td></tr>
<tr><td>
	<table id="tbl1" width="100%">
	<tr>
	<td width="20%">
	<td width="35%">
	<td width="15%">
	<td width="15%" align="center">
	<td width="15%" align="right">
	</tr>
	<s:iterator value="cartItems" status="stat">
	<tr>
	<td  colspan="5" width="100%">
	<div id="itemClass<s:property value="#stat.count"/>">
	<form action="checkOutCartItem">
	<s:hidden name="category_id" value="%{category_id}" id="category%{#stat.count}"/>
	<s:hidden name="subcategory_id" value="%{subcategory_id}" id="subcategory%{#stat.count}"/>
	<s:hidden name="product_id" value="%{product_id}" id="product%{#stat.count}"/>
	<table width="100%">
	<tr style="font-size: 15px" bgcolor="#C0C0C0">
	<td colspan="2" width="100%" style="font-size: 15px" bgcolor="#C0C0C0">
	Seller : <font color="blue"><s:property value="seller_id"/></font>
	</td>
	<td width="15%">Price
	<td width="15%" align="center">Quantity
	<td width="15%" align="right">Sub-Total
	</tr>
	<tr>
	<td width="20%"><img src="<s:property value="image"/>" width="80px" height="80px"/>
	<td style="font-size: 15px" width="35%">
	  <s:a action="showItemDetails?category_id=%{category_id}&subcategory_id=%{subcategory_id}&product_id=%{product_id}">
	  <s:property value="title"/></s:a>
	<td width="15%">&#8377; <s:property value="priceText"/>
	<td align="center" width="15%"><input size="4" type="text" name="quantitypurchased" value="<s:property value="quantitypurchased"/>">
	<td width="15%" align="right">&#8377; <s:property value="subTotalText"/>
	</tr>
	<tr><td colspan="3">
	<td id="hello" >
	<td id='item-<s:property value="#stat.count"/>' align="center"> <input type="submit" value="Checkout Item" ><br/><br/><a class="removeItem" href="removeItemfromCart">Remove from cart</a>.
	
	</tr>
	</table>
	</form>
	</div>
	</td>
	</tr>
	</s:iterator>
	</table>
</td>
</tr>
</table>
</s:else>

</body>
</html>