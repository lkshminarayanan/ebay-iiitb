<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="/struts-tags" prefix="s" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Books</title>
<link rel="stylesheet" type="text/css" href="css/glossymenu.css" />
<link rel="stylesheet" type="text/css" href="css/showItemDetails.css" />
<style type="text/css">
.errorsBg {
    background-color: #FFDDDD;
    border: 1px solid;
    color: red;
}
</style>
</head>
<body>
<%@ include file="../../header.jsp" %><br/>
<form action="displayItemList" method="get" name="displayForm">
<s:hidden name="category_id" value="%{category_id}"/>
<s:hidden name="subcategory_id" value="%{subcategory_id}"/>
<s:hidden id="pageNumber" name="pageNumber" value="%{pageNumber}"/>
<s:hidden id="filterReset" name="filterReset" value="%{filterReset}"/>
<s:hidden id="filterSellerId" name="filterSellerId" value="%{filterSellerId}"/>
<s:hidden id="searchTerm" name="searchTerm" value="%{searchTerm}"/>
<table width="100%" align="center">
	<tr>
	<td valign="top" width="20%">
	<%@ include file="/jsp_pages/buyer/subCategoryMenu.jsp" %>
	<table width="100%">
	<tr><td>
	<div >
	<ul id="verticalmenu" class="glossymenu">
	<li><a>Price Range</a>
	<li><a>&#8377;<input type="text" id="filterPriceRangeHigh" name="filterPriceRangeLow" value='<s:property value="filterPriceRangeLow"/>' size="2"> to 
	<input type="text" name="filterPriceRangeHigh" id="filterPriceRangeLow" value='<s:property value="filterPriceRangeHigh"/>' size="2"> 
	<input type="submit" value=">"></a>
	</li>
	</ul>
	</div>
	</tr>
	</table>
	<table width="100%">
	<tr><td>
	<div >
	<ul id="verticalmenu" class="glossymenu">
	<li><a><input type="checkbox" <s:if test="filterBuyNow">checked="checked"</s:if> onclick="document.getElementById('pageNumber').value=0;document.displayForm.submit()" name="filterBuyNow" > &nbsp;&nbsp;Buy Now  </a>
	<li><a> <input type="checkbox" <s:if test="filterAuction">checked="checked"</s:if> onclick="document.getElementById('pageNumber').value=0;document.displayForm.submit()" name="filterAuction">&nbsp;&nbsp;&nbsp;Auction</a>
	</li>
	</ul>
	</div>
	</tr>
	</table>
	<table width="100%">
	<tr><td>
	<div >
	<ul id="verticalmenu" class="glossymenu">
	<li><a><input type="checkbox" <s:if test="filterCondNew">checked="checked"</s:if> onclick="document.getElementById('pageNumber').value=0;document.displayForm.submit()" name="filterCondNew" > &nbsp;&nbsp;New  </a>
	<li><a> <input type="checkbox" <s:if test="filterCondOld">checked="checked"</s:if> onclick="document.getElementById('pageNumber').value=0;document.displayForm.submit()" name="filterCondOld">&nbsp;&nbsp;&nbsp;Used/Old</a>
	</li>
	</ul>
	</div>
	</tr>
	</table>
	</td>
	<td valign="top">
	<hr width="100%">
	<table width="100%">
	<tr>
		<td width="35%">Showing
			<select  id="page" name="filterItemsToDisplay" onchange="document.getElementById('pageNumber').value=0;document.displayForm.submit()">
				<option value="1" <s:if test="filterItemsToDisplay==1">selected="selected" </s:if>>1</option>
				<option value="2" <s:if test="filterItemsToDisplay==2">selected="selected" </s:if>>2</option>
				<option value="3" <s:if test="filterItemsToDisplay==3">selected="selected" </s:if>>25</option>
				<option value="4" <s:if test="filterItemsToDisplay==4">selected="selected" </s:if>>50</option>
			</select> 
			results per page.
		</td>
		<td width="30%">
		<s:if test="isFilterSet()">
		<a href="javascript:document.getElementById('filterReset').value=1;
							document.displayForm.submit()">clear preferences</a>
		</s:if>
		</td>
		<td width="35%" align="right">
		Sort by : <select id="filterSort" name="filterSort" onchange="document.getElementById('pageNumber').value=0;document.displayForm.submit()">
			<option value="0" <s:if test="filterSort==0">selected="selected" </s:if>>---</option>
			<option value="1" <s:if test="filterSort==1">selected="selected" </s:if>>Price : Lowest First</option>
			<option value="2" <s:if test="filterSort==2">selected="selected" </s:if>>Price : Highest First</option>
			<option value="3" <s:if test="filterSort==3">selected="selected" </s:if>>Time : Ending Soon</option>
		</select>
		</td>
	</tr>
	</table>
	<hr width="100%">
	<s:if test="totalPages>0">
	<%@include file="item.jsp" %>
	</s:if>
	<s:else>
	<div align="center" class="errorsBg">
	No results matching your query.
	</div>
	</s:else>
	</td>
	</tr>
</table>
</form>
</body>
</html>