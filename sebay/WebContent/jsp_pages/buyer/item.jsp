<%@taglib uri="/struts-tags" prefix="s" %>

<table width="100%">
<tr>
<td width="15%"></td>
<td width="40%"></td>
<td width="5%"></td>
<td width="10%"></td>
<td width="15%"></td>
<td></td>
</tr>
<s:iterator value="itemList" >
<tr>
<td align="center" valign="middle">
<img src="<s:property default="/image/noimageavailable.jpg" value="value.image" />" width="100%" height="140"/>
</td>
<td align="left" valign="top">
<a href="showItemDetails?category_id=<s:property value="value.category_id"/>&subcategory_id=<s:property value="value.subcategory_id"/>&product_id=<s:property value="value.product_id"/>">
<s:property value="value.title" />
</a>
<br/><s:property value="key"/>
</td>
<td align="left" valign="top">
<img src="ebayimages/paisapay.gif"/>
</td>

<s:if test="value.biddingstatus">
	<td align="left" valign="top">
	<s:if test="value.bid.noOfBidders==1">
		<s:property value="value.bid.noOfBidders"/> bid
	</s:if>
	<s:else>
		<s:property value="value.bid.noOfBidders"/> bids
	</s:else>
	</td>
</s:if>
<s:else>
	<td align="left" valign="top">
	<img src="ebayimages/buynow.gif"/>
	</td>
</s:else>
<td align="left" valign="top">
	&#8377; <s:property value="value.priceText"/><br/>
	<s:if test="value.shippingCharges == 0">
	Free shipping
	</s:if>
	<s:else>
	+ &#8377; <s:property  value="value.shippingChargesText"/>
	</s:else>
</td>
<td align="left" valign="top">
<s:property value="value.timeRemaining"/>
</td>
</tr>
<tr><td colspan="7"><hr width="100%" /></td></tr>
</s:iterator>
<tr>
<td colspan="6" align="right">
<s:if test="pageNumber>0">
<a href="javascript:document.getElementById('pageNumber').value--;document.displayForm.submit()">
&lt;&lt; 
</a>
</s:if>
<s:property value="pageNumber+1"/>/<s:property value="totalPages"/>
<s:if test="pageNumber+1<totalPages">
<a href="javascript:document.getElementById('pageNumber').value++;document.displayForm.submit()">
&gt;&gt; 
</a>

</s:if>
</td>
</tr>
</table>


