<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@taglib uri="/struts-tags" prefix="s" %>
<%@taglib uri="/struts-dojo-tags" prefix="sx" %>    
<!DOCTYPE html>
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><s:property value="item.title" /> | eBay_iiitb </title>
<style type="text/css">
.cellBorder{
border: 1px solid #E2E2E2; 
margin: -2px;
}
.errorsBg {
    background-color: #FFDDDD;
    border: 1px solid;
    color: red;
}
.infoBg {
	
    border: 2px solid;
    color: blue;
}
</style>
<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>

<script type="text/javascript">
var ajaxOptions = 'category_id=<s:property value="item.category_id"/>&subcategory_id=<s:property value="item.subcategory_id"/>&product_id=<s:property value="item.product_id"/>&seller_id=<s:property value="item.seller_id"/>&title=<s:property value="item.title"/>&price=<s:property value="item.price"/>&image=<s:property value="item.image"/>';
$(document).ready(function() {
	$('a.cart').click(function(e) {
		//alert("here");
		e.preventDefault();
		var quan = document.getElementById("quantity").value;
		//alert(""+quan);
		$.ajax({
			type: 'get',
			url: 'addToCart',
			data: 'ajax=1&' + ajaxOptions +'&quantitypurchased='+quan ,
			beforeSend: function() {
				
			},
			cache: false,
			success: function() {
				$('#addToCart').text("Product successfully added to cart!");
				//alert("Product added to cart successfully!");
			}
		});
	});
});

</script>


<sx:head/>

</head>

<body >
<%@ include file="../../header.jsp" %><br/>
<div class="smallLinks">
<a href='<s:property value="#session.prevSearch"/>'>&lt;-Go Back to search results</a> | 
<a href="displayItemList?category_id=<s:property value="item.category_id"/>"><s:property value="categoryName" /></a> &gt;
<a href="displayItemList?category_id=<s:property value="item.category_id"/>&subcategory_id=<s:property value="item.subcategory_id"/>"><s:property value="subCategoryName"/></a>
</div>
<br/>
<table width="100%" cellpadding="4" >
<s:if test='#session.user.ebay_id.equals(item.seller_id)'>
<div align="center" class="errorsBg">
	This item has been listed by you!
	</div>
</s:if>
<s:if test="item.isExpired()">
<div align="center" class="errorsBg">
	Sorry the item you were interested in expired!
	</div>
</s:if>
<s:elseif test="item==null">

<div align="center" class="errorsBg">
	Invalid Request!
	</div>
</s:elseif>

<s:else>
<s:if test="isAlreadyInCart()">
<tr><td align="left" colspan="3">
<div style="font-size: 15px" align="center" class="infoBg">
	This item is already in your cart.<a href="showCart">Checkout Now!</a>
	</div>
</s:if>


<tr>
<td width="25%" valign="middle" align="center" id="image" class="cellBorder" >
<img src="<s:property value="item.image" />" width="200" height="230"/></td>
<td width="45%">
<table style='color:#333333;' width="100%">
	<tr><td colspan="2" align="left" width="100%" valign="top"><font style='color:#333333;font-family:Trebuchet,"Trebuchet MS"' size="5"><s:property value="item.title"/></font></tr>
	<tr><td colspan="2"><s:property value="item.description"/>
	<tr><td colspan="2">
	<table>
	<tr>
		<td width="30%" align="right">Item Condition : </td><td width="70%"><b><s:property value="item.cond"/></b></td>
	</tr>
	<tr>
		<td width="30%" align="right">Time Left : </td><td width="70%"><s:property value="item.timeRemaining"/>(<s:property value="item.endTime"/>)</td>
	</tr>
	</table>
	</td>
	</tr>

	<tr><td width="50%">
	<tr><td colspan="2">
	<s:if test="item.biddingstatus">
		<form action="reviewBid">
		<input type="hidden" name="subcategory_id" value="<s:property value="item.subcategory_id" />">
		<input type="hidden" name="category_id" value="<s:property value="item.category_id" />">
		<input type="hidden" name="product_id" value="<s:property value="item.product_id" />">
		<table width="100%" bgcolor="#E6E6E6">
			<tr><td width="30%"><s:if test="item.bid.noOfBidders==0">Starting Price : </s:if><s:else>Current Bid : </s:else><td><b>&#8377; <s:property value="item.bid.bidPriceText"/></b>
			<tr><td>Bid History : </td>
				<td><s:if test="item.bid.noOfBidders>0"><s:a href="showBiddingHistory?category_id=%{item.category_id}&subcategory_id=%{item.subcategory_id}&product_id=%{item.product_id}"><s:property value="item.bid.noOfBidders"/> bid<s:if test="item.bid.noOfBidders!=1">s</s:if></s:a></s:if> 
				<s:else>0 bids</s:else>
				</td>
			<s:if test='#session.user==null||!#session.user.ebay_id.equals(item.seller_id)'>
			<tr><td>Your Bid : <td colspan="2">&#8377; <input  type="text" size="5" name="bidAmount"/>&nbsp;&nbsp;&nbsp;<input type="submit" value="Place Bid"/>
			<tr><td><td>(Enter &#8377; <s:property value="item.bid.nextValidBidText"/> or more)</tr>
			
			</s:if>
		</table>
		</form>
	</s:if>
		<s:else>
		<form action="buyNow">
		<input type="hidden" name="subcategory_id" value="<s:property value="item.subcategory_id" />">
		<input type="hidden" name="category_id" value="<s:property value="item.category_id" />">
		<input type="hidden" name="product_id" value="<s:property value="product_id" />">
		<table width="100%" bgcolor="#E6E6E6">
			<tr><td>Price : <td><b>&#8377;<s:property value="item.priceText"/></b>
			<s:if test='#session.user==null||!#session.user.ebay_id.equals(item.seller_id)'>
			<tr><td>Quantity : <td>&#8377; <input value="1" type="number" size="3" id="quantity" min="1" max="<s:property value="item.quantity" />" name="quantitypurchased"/>&nbsp;&nbsp;&nbsp;<b><s:property value="item.quantity" /></b> available.
			<tr><td><td><input type="submit" value="Buy Now"/>
			<tr><td> <td id="addToCart">
			<s:if test="!isAlreadyInCart()">or 
			<s:url id="addtocartUrl" action="addToCart"/>
			<s:if test='#session.loggedin.equals("true")'>
			<a href="<s:property value="addtocartUrl" />" class="cart">add to cart </a>and continue shopping!
		
			</s:if>
			<s:else><a href="jsp_pages/account/SignIn.jsp">Sign In</a> to add to your cart</s:else>
			</s:if>
			</td></tr>
				
			</s:if>
		</table>
		</form>
		</s:else>
	</table>
		<br/>
	<table width="100%">
		<tr>
		<td align="right" width="30%">Shipping Charges : 
		<td width="70%">
		<s:if test="item.shippingCharges==0">
		Free Shipping
		</s:if>
		<s:else>
		&#8377;<s:property value="item.shippingChargesText"/>
		</s:else> - Anywhere inside India.
		</td>
		</tr>
		<tr><td></td></tr>
		<tr>
		<td align="right">Payments : 
 		<td><img src="ebayimages/paisapayfull.gif"/>&nbsp;&nbsp;&nbsp;(Credit cards, Debit Cards)</td></tr>
 		<tr><td></td></tr>
 		<tr>
 		<td align="right">Returns : 
		<td> Not Specified
	</table>
</td>
<td valign="top" width="30%">
	<table cellpadding="4" cellspacing="10" width="100%">
	<tr valign="top">
		<td class="cellBorder" width="100%">
		<table width="100%">
		<tr><td width="100%" style='color:#333333;font-family:Trebuchet,"Trebuchet MS";font-size:18px'>Seller Info</td></tr>
		<tr><td ><s:property value="item.seller_id" /> </td></tr>
		<tr><td><hr style="color:#E2E2E2" width="100%"/></td></tr>
		<tr><td class="smallLinks"><s:a action="displayItemList?category_id=%{item.category_id}&subcategory_id=%{item.subcategory_id}&filterSellerId=%{item.seller_id}">view other items from the seller</s:a></td></tr>
		</table>
		</td>
	</tr>
	<tr valign="top">
		<td class="cellBorder" width="100%">
		<table width="100%">
		<tr><td width="100%" style='color:#333333;font-family:Trebuchet,"Trebuchet MS";font-size:18px'>Other Item Info</td></tr>
		<tr><td>Item Location : <s:property default="Not Available" value="locationInfo" /></td></tr>
		</table>
		
		</td>
	</tr>
	</table>
	</td>
	</tr>

</s:else>
</table>


<br/>
<br/>
<br/>
<br/>
<br/>
<form>


<!--<sx:tabbedpanel id="text" cssClass="d">

<sx:div id="bookdescription" label="Description" cssClass="d">
<s:iterator value="lstitem">
Title:<s:property value="title"/><br/>
Description:<s:property value="description"/><br/>
Price:<s:property value="price"/><br/>
Condition:<s:property value="cond"/><br/>
No of pages:<s:property value="pages"/><br/>
Author:<s:property value="author"/><br/>
Publisher:<s:property value="publisher"/><br/>
Language:<s:property value="lang"/><br/>
</s:iterator>
</sx:div>

<sx:div id="bookshipping" label="Shipping and Payment" >

</sx:div>

</sx:tabbedpanel>-->
</form>
</body>

</html> 
