<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<table>
<tr><td>
<div >
<ul id="verticalmenu" class="glossymenu">
<li>
<a style="text-decoration: underline">
<s:if test="category_id==0">Categories</s:if>
<s:else>Sub-Categories</s:else>
</a>
<s:iterator value="subCatList">
<li>
<s:if test='key.equals("")'>
<a id="selected"><s:property value="value"/>&nbsp;&nbsp;&nbsp;&gt;</a>
</s:if>
<s:else>
	<s:if test="searchTerm!=null&&searchTerm.length()>0">
	<s:url id="url_1" action="displayItemList%{key}&searchTerm=%{searchTerm}"/>
	</s:if>
	<s:else>
	<s:url id="url_1" action="displayItemList%{key}"/>
	</s:else>
<s:a href="%{url_1}">
<s:property value="value"/>
</s:a>
</s:else>
</s:iterator>
</ul>
</div>
</tr>
</table>