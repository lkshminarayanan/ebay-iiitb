$(document).ready(function() {
	  $('a.removeItem').click(function(e) {
	    e.preventDefault();
	    var parent = $(this).parent();
	    var parentid = parent.attr('id').replace('item-','');
	    var cat_id = document.getElementById("category"+parentid).value;
	    var subcat_id = document.getElementById("subcategory"+parentid).value;
	    var product_id = document.getElementById("product"+parentid).value;
	    var queryString = "category_id="+cat_id+"&subcategory_id="+subcat_id+"&product_id="+product_id+"&user_id="+userid;
	    //alert(queryString);
	    $.ajax({
		      type: 'get',
		      url: 'removeItemfromCart',
		      data: 'ajax=1&' + queryString,
		      beforeSend: function() {
			        $('#itemClass'+parentid).css({"background-color":"#FAC4C4"});
			      },
		      cache: false,
		      success: function() {
		    	    $('#itemClass'+parentid).slideUp(1000,function() {
		    	    	$('#itemClass'+parentid).remove();
		        });
		      }
		    });
	  });
	});